//
//  E2DKey.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 12/05/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_KEY_H
#define E2D_KEY_H

#include <iostream>
#include "E2D_Obj_.hpp"
#include "E2DSprite.h"
#include "E2DFont.h"

#define E2D_KEY_COUNT   100

#pragma mark -
#pragma mark E2DKeyDrawable
#pragma mark -
class E2DKeyDrawable : public E2D_Obj_ {
    friend class E2DKey;
    E2Dvertex   *v[4];
    E2Duint     tID;
    E2Dpoint    pos;
    E2Dsize     size, ss, ss_h; //screensize, screensize half
    
    E2DKeyDrawable(E2Dvertex[], E2Duint tID);
    ~E2DKeyDrawable();
    
    void setSize(E2Dsize);
    void setPos(E2Dpoint);
    void setTexCoord(E2DtexCoord[]);
    
    inline void print(const char *) const;
    
};

#pragma mark -
#pragma mark E2DKey
#pragma mark -
class E2DKey : public E2D_Obj_ {
    friend class E2DRendererES2;        //e2d_rend
    
    unsigned char character;
    E2DKeyDrawable   *letter, *bg;
        
    class E2DFontManager        *fontManager;
    class E2DTextureManager     *textureManager;
    
    E2Dpoint    pos;
    E2Dsize     size, letter_size, bg_size;
    
public:
    inline void print(const char *) const
    {
        E2D_PRINT("\ncharacter:%c, %d", character, character);
    }
    
    E2DKey(unsigned char char_, const char *bgFile_, const char *fontFile_, E2Dvertex[]); // 8 vertices - 4 for letters, 4 for bg
    ~E2DKey();
    
    E2Duint letterTexID();
    E2Duint bgTexID();
    
    void SetPosition(E2Dpoint pos_);
    E2Dpoint Position();
    
    void SetSize(E2Dsize s_);
    E2Dsize Size();
    
    E2Dsize LetterSize();
    E2Dsize BgSize();
    
};

#endif /* defined(__Echo2D_2__E2DKey__) */
