//
//  E2DEmitter.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_EMITTER_H
#define E2D_EMITTER_H

#include <iostream>
#include "E2DDrawable.h"
#include "E2DEmitterSettings.h"
#include "E2DparticleAttribs.hpp"
#include "E2DparticleMod.hpp"

#include "E2DObjectTypes.h"

class E2DParticle;
class E2DEmitter : public E2DDrawable, public E2DEmitterSettings {
    friend class E2DRendererES2;
    friend class E2DParticle;
    friend class E2DLayer;
    
    E2DpRenderAttribs   *p;
    E2DArray            *particles;
    E2Dradians          angle;          //overriding base E2Ddegrees
    
    unsigned int        pCount;
    unsigned int        activeParticles;
    
    class E2DTextureManager     *texManager;
    
    
    void setSize(float width_, float height_) { __size(width_, height_); }
    void update(E2Delapsed dt_);
    void reset(E2DParticle *);
    void particleDeactivated(void);
    void SetColor(E2Dcolor) { };
    void seekProperty(const char *, std::ifstream&);
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_EMITTER; }
    E2DEmitter(const char *spriteFile_, unsigned int pCount_, E2Ddepth depth_);
    ~E2DEmitter();
    
    void print(const char *) const;
    
    void SetAngle(E2Ddegrees);
    E2Ddegrees Angle(void) const;
    
    const E2Dvertex* Vertices(void) const { return nullptr;};
    
    virtual void render();
    void AddParticle(E2DParticle *);
    void LoadConfig(const char *);
    void PrintConfig(void) const;
    
    
};

#endif /* defined(__Echo2D_2__E2DEmitter__) */
