//
//  E2Dvec2.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 28/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_VECTOR_H
#define E2D_VECTOR_H

#include <iostream>
#include <fstream>
#include "E2DDataTypes.h"

struct E2Dvec2 {
    E2Dfloat    x, y;
    
    static inline E2Dclass_t Class(void) { return E2D_CLASS_VECTOR; }
    
    E2Dvec2(void) : x(0.0f), y(0.0f) { }
    E2Dvec2(E2Dfloat x_, E2Dfloat y_) : x(x_), y(y_) { }
    
    void operator()(E2Dfloat x_, E2Dfloat y_) { x = x_, y = y_; }
    
    friend std::ostream& operator<<(std::ostream& stream_, const E2Dvec2& rhs_)
    {
        stream_ << "(" << rhs_.x << ", " << rhs_.y << ")";
        return stream_;
    }

    
    E2Dvec2 operator/(const E2Dvec2& v_) const
    {
        E2Dvec2 ret(x / v_.x, y / v_.y);
        return ret;
    }
    
    E2Dvec2 operator*(const E2Dvec2& v_) const
    {
        E2Dvec2 ret(x * v_.x, y * v_.y);
        return ret;
    }
    
    E2Dvec2 operator+(const E2Dvec2& v_) const
    {
        E2Dvec2 ret(x + v_.x, y + v_.y);
        return ret;
    }
    
    E2Dvec2 operator-(const E2Dvec2& v_) const
    {
        E2Dvec2 ret(x - v_.x, y - v_.y);
        return ret;
    }
    
    void operator/=(const E2Dvec2& v_)
    {
        x /= v_.x;
        y /= v_.y;
    }
    
    void operator*=(const E2Dvec2& v_)
    {
        x *= v_.x;
        y *= v_.y;
    }
    
    void operator+=(const E2Dvec2& v_)
    {
        x += v_.x;
        y += v_.y;
    }
    
    void operator-=(const E2Dvec2& v_)
    {
        x -= v_.x;
        y -= v_.y;
    }
    
    bool operator==(const E2Dvec2& v_) const
    {
        return (x == v_.x && y == v_.y);
    }
    
    void Print() const
    {
        E2D_PRINT("\nvector:(%f, %f)", x, y);
    }
    
    void Print(const char *msg) const
    {
        E2D_PRINT("\n%s (%f, %f)", msg, x, y);
    }
    
    void Read(std::ifstream& stream_)  /* reads in vector from file. file pointer not reset */
    {
        stream_ >> x;
        stream_ >> y;
    }
};

#endif /* defined(__Echo2D_2__E2Dvec2__) */
