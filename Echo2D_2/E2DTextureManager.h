//
//  E2DTextureManager.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 26/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_TEXTURE_MANAGER_H
#define E2D_TEXTURE_MANAGER_H

#include <iostream>
#include "E2D_Obj_.hpp"
#include "E2DDataStructs.hpp"
#include "E2DObjectTypes.h"

#pragma mark -
#pragma mark E2DTexture
#pragma mark -

class E2DTexture : public E2D_Obj_ {
    friend class E2DTextureManager;
    E2DString       *name;
    E2Dsize         size;
    GLuint          textureID;
    E2DtexCoord     coord[4];
    
public:
     static inline E2Dclass_t Class(void) { return E2D_CLASS_TEXTURE; }
    E2DTexture(E2DString *, E2Dsize, GLuint);
    E2DTexture(E2DString *, E2Dsize, GLuint, E2DtexCoord[]);
    virtual ~E2DTexture();
    
    void print(const char *) const;
    
    E2Dsize Size(void);
    GLuint ID(void);
    E2DtexCoord* Coord(void);
    
};


#pragma mark -
#pragma mark E2DTextureManager
#pragma mark -

#define _E2D_DICT_SIZE    1000

class E2DRenderer;

class E2DTextureManager : public E2D_Obj_ {
    
    E2DRenderer         *__renderer;
    E2DDictionary       *__textures;
    
    void print(const char *) const;
  
public:
     static inline E2Dclass_t Class(void) { return E2D_CLASS_TEXTURE_MANAGER; }
    static E2DTextureManager* SharedTextureManager(void);
    
    E2DTextureManager();
    virtual ~E2DTextureManager();
    
    E2DTexture* TextureForName(const char *);
    
    void AssignTexture(const char*, class E2DDrawable4 *);  /* from cached atlas. caches if not already cached */
    
    E2DTexture* CacheTexture(const char *);
    void CacheTextureAtlas(const char *);
    
};

#endif /* defined(__Echo2D_2__E2DTextureManager__) */
