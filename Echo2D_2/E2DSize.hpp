//
//  E2DSize.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 28/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_SIZE_H
#define E2D_SIZE_H

#include <iostream>
#include "E2DDataTypes.h"
#include "E2DMacros.h"

struct E2Dsize {
    
    static inline E2Dclass_t Class(void) { return E2D_CLASS_SIZE; }
    
    E2Dfloat   width, height;
    
    
    E2Dsize() { width = 0.0f, height = 0.0f; }
    E2Dsize(E2Dfloat width_, E2Dfloat height_) : width(width_), height(height_) {}
    
    void Set(E2Dfloat width_, E2Dfloat height_);
    
    void operator()(E2Dfloat width_, E2Dfloat height_);
    void  operator()(E2Dsize&);
    E2Dsize& operator=(const E2Dsize&);
    E2Dsize operator+(const E2Dsize&);
    E2Dsize operator-(const E2Dsize &);
    
    E2Dsize operator/(const float& f_)
    {
        return E2Dsize(width / f_, height / f_);
    }
    E2Dsize& operator+=(const E2Dsize&);
    E2Dsize& operator-=(const E2Dsize&);
    E2Dsize operator*(const E2Dsize&);
    E2Dsize operator*(const float);
    E2Dsize operator*=(const E2Dsize& size);
    E2Dsize operator*=(const float);
    E2Dsize& operator++();
    E2Dsize& operator++(int);
    E2Dsize operator--();
    E2Dsize& operator--(int);
    bool operator==(const E2Dsize& size);
    //friend inline std::ostream& operator<<(std::ostream &out, const E2Dsize &size);
    
    void Print() const;
    void Print(const char *s) const;
};

#endif /* defined(__Echo2D_2__E2DSize__) */
