//
//  E2DAudioLoader.cpp
//  Echo2D_2_Audio
//
//  Created by Vijay Chandran J on 01/06/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#include "E2DAudioLoader.h"

static E2DAudioLoader *instanceOfAudioLoader = nullptr;

E2DAudioLoader* E2DAudioLoader::SharedAudioLoader()
{
    E2D_ASSERT(instanceOfAudioLoader, "audioLoader not instantiated");
    return instanceOfAudioLoader;
}

E2DAudioLoader::E2DAudioLoader()
{
    if (instanceOfAudioLoader)
        return;
    
    instanceOfAudioLoader = this;
    fileManager = E2DFileManager::SharedFileManager();
}

E2DAudioLoader::~E2DAudioLoader()
{
    
}

ALuint E2DAudioLoader::LoadAudio(const char *name_)
{
    ALuint ret;
    
    char *buf = NULL;
    e2d::extension(name_, &buf);
    
    if (strcmp(buf, "wav") == 0)
    {
        ret = this->loadWAV(name_);
    }
    else if (strcmp(buf, "caf") == 0)
    {
        ret = this->loadCAF(name_);
    }
    else
    {
        printf("\nunsupportzed audio file type (.%s)", buf);
        E2D_FREE(buf);
        exit(1);
    }
    
    E2D_FREE(buf);
    
    
    return ret;
}

ALuint E2DAudioLoader::loadWAV(const char *fileName_)
{
    ALuint ret;
    
    char *path = nullptr;
    E2DFileManager::SharedFileManager()->PathForFile_alc(fileName_, &path);
    E2D_PRINT(">>>%s\n", path);
    
    FILE *f = fopen(path, "r");
    E2D_FREE(path);
    E2D_ASSERT(f, "unable to open file audio file at path '%s'", path);
    
    E2DaudioStructWAV wav;
    wav.type[4] = '\0';
    fread(wav.type, sizeof(char), 4, f);
    E2D_ASSERT((strcmp(wav.type, "RIFF") == 0), "invalid WAV file '%s' (not RIFF)", fileName_);
    fread(&wav.size, sizeof(long), 1, f);
    fread(wav.type, sizeof(char), 4, f);
    E2D_ASSERT((strcmp(wav.type, "WAVE") == 0), "inavlid WAV file '%s' (not WAVE)", fileName_);
    
    fread(wav.type, sizeof(char), 4, f);
    E2D_ASSERT((strcmp(wav.type, "fmt ") == 0), "invalid WAV file '%s' (no fmt)", fileName_);
    fread(&wav.chunkSize, sizeof(long), 1, f);
    fread(&wav.formatType, sizeof(short), 1, f);
    fread(&wav.channels, sizeof(short), 1, f);
    fread(&wav.sampleRate, sizeof(long), 1, f);
    fread(&wav.avgBytesperSec, sizeof(long), 1, f);
    fread(&wav.bytesPerSample, sizeof(short), 1, f);
    fread(&wav.bitsPerSample, sizeof(short), 1, f);
    
    fread(&wav.type, sizeof(char), 4, f);        //data
    E2D_ASSERT((strcmp(wav.type, "data") == 0), "invalid WAV file '%s' (no data)", fileName_);
    fread(&wav.dataSize, sizeof(long), 1, f);
    unsigned char *buf = (unsigned char *)malloc(sizeof(unsigned char) * wav.dataSize);
    fread(buf, sizeof(unsigned char), wav.dataSize, f);
    
    fclose(f);
    
    ALenum format;
    
    if (wav.bitsPerSample == 8)
    {
        if (wav.channels == 1)
            format = AL_FORMAT_MONO8;
        else
            format = AL_FORMAT_STEREO8;
        
    }
    else
    {
        if (wav.channels == 1)
            format = AL_FORMAT_MONO16;
        else
            format = AL_FORMAT_STEREO16;
    }
    
    alGenBuffers(1, &ret);
    alBufferData(ret, format, buf, (ALsizei)wav.dataSize, (ALsizei)wav.sampleRate);
    /*
    ALfloat sourcePos[] = { -10.0f, 0.0f, 0.0f };
    ALfloat sourceVel[] = { 0.0f, 0.0f, 0.0f };
    ALfloat listenerPos[] = { 1.0f, 0.0f, 0.0f };
    ALfloat listenerVel[] = { 0.0f, 0.0f, 0.0f };
    ALfloat listenerOrient[] = { 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f };
    
    ALuint s;
    alGenSources(1, &s);
    alSourcei(s, AL_BUFFER, ret);
    alSourcef(s, AL_PITCH, 1.0f);
    alSourcef(s, AL_GAIN, 1.0f);
    alSourcefv(s, AL_POSITION, sourcePos);
    alSourcefv(s, AL_VELOCITY, sourceVel);
    alSourcei(s, AL_LOOPING, AL_TRUE);
    
    alListenerfv(AL_POSITION, listenerPos);
    alListenerfv(AL_VELOCITY, listenerVel);
    alListenerfv(AL_ORIENTATION, listenerOrient);
    
    alSourcePlay(s);
    //*/
    return ret;
}

ALuint E2DAudioLoader::loadCAF(const char *file_)
{
    ALuint ret;
    
    char *path = nullptr;
    fileManager->PathForFile_alc(file_, &path);
    
    FILE *f = fopen(path, "rb");
    E2D_ASSERT(f, "unable to open file audio file at path '%s'", path);
    E2D_FREE(path);
    
    /* read CAF header */
    struct CAFFileHeader caf_h;
    struct CAFChunkHeader caf_chunk_h;
    struct CAFAudioFormat caf_af;
    //fread(&caf_h, sizeof(CAFFileHeader), 1, f);
    fread(&caf_h.mFileType, sizeof(UInt32), 1, f);
    fread(&caf_h.mFileVersion, sizeof(UInt16), 1, f);
    fread(&caf_h.mFileFlags, sizeof(UInt16), 1, f);
    printCAFHeader(&caf_h);
    
    /* read CAF chunk description header */
    //fread(&caf_chunk_h, sizeof(CAFChunkHeader), 1, f);
    fread(&caf_chunk_h.mChunkType, sizeof(UInt32), 1, f);
    fread(&caf_chunk_h.mChunkSize, sizeof(UInt64), 1, f);
    printCAFChunkHeader(&caf_chunk_h);
    
    /* read CAF chunkdescription  data (caf audio format) */
    //fread(&caf_af, sizeof(CAFAudioFormat), 1, f);
    fread(&caf_af.mSampleRate, sizeof(Float64), 1, f);
    printf("\nsample rate>>:%f", caf_af.mSampleRate);
    //fscanf(f, "%lf", &caf_af.mSampleRate);
    fread(&caf_af.mFormatID, sizeof(UInt32), 1, f);
    //fre
    printCAFAudioFormat(&caf_af);
    printf("\n%u: %s\n", kAudioFormatLinearPCM, caf_af.mFormatID == kAudioFormatMPEG4AAC ? "true" : "false");
    char *buf = (char *)malloc(sizeof(char) * 5);
    e2d::extractString(caf_h.mFileType, buf, 4);
    printf("\n%s\n", buf);
    
    fclose(f);
    return ret;
}
