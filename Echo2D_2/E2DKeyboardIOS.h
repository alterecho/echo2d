//
//  E2DKeyboardIOS.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 12/06/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_KEYBOARD_IOS_H
#define E2D_KEYBOARD_IOS_H

//#import <Foundation/Foundation.h>

#include <iostream>
#include "E2DKeyboard.h"

class E2DKeyboardIOS : public E2DKeyboard {
    
public:
    E2DKeyboardIOS();
    ~E2DKeyboardIOS();
    
    void Activate(void);
    void Dismiss(void);
    /*
    void DeleteText();
    bool HasText();
    void InsertText(const char *t_);
    // */
};

#endif /* defined(__Echo2D_2__E2DKeyboardIOS__) */
