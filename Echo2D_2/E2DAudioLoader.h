//
//  E2DAudioLoader.h
//  Echo2D_2_Audio
//
//  Created by Vijay Chandran J on 01/06/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#ifndef E2D_AUDIO_LOADER_H
#define E2D_AUDIO_LOADER_H

#include <iostream>
#include "E2D_Obj_.hpp"

#include "E2DFileManager.h"

#include "E2DAudioData.h"
#include <OpenAL/al.h>
#include <MacTypes.h>


struct CAFFileHeader {
    
    UInt32  mFileType;
    
    UInt16  mFileVersion;
    
    UInt16  mFileFlags;
    
};

static inline void printCAFHeader(struct CAFFileHeader *h)
{
    e2d::printStringFromInt(h->mFileType, e2d::endian_big, "CAFHeader FileType:");
    printf("\nCAFHeader:--\
           \nmFileVersion:%d \
           \nmFileFlags:%d\n", h->mFileVersion, h->mFileFlags);
}

struct CAFChunkHeader {
    UInt32  mChunkType;
    SInt64  mChunkSize;
};

struct CAFAudioFormat {
    
    Float64 mSampleRate;
    
    UInt32  mFormatID;
    
    UInt32  mFormatFlags;
    
    UInt32  mBytesPerPacket;
    
    UInt32  mFramesPerPacket;
    
    UInt32  mChannelsPerFrame;
    
    UInt32  mBitsPerChannel;
    
};

static inline void printCAFChunkHeader(struct CAFChunkHeader *h)
{
    e2d::printStringFromInt(h->mChunkType, e2d::endian_big, "chunkHeader");
    printf("\nCAFFF_S:%lu", sizeof(CAFAudioFormat));
    printf("\nchunkHeader:--\
           \nmChunkType:%u\
           \nmChunkSize:%lld, %lld kb\n", (unsigned int)h->mChunkType, h->mChunkSize, h->mChunkSize / (1024 * 1024));
}

enum {
    
    kAudioFormatLinearPCM      = 'lpcm',
    
    kAudioFormatAppleIMA4      = 'ima4',
    
    kAudioFormatMPEG4AAC       = 'aac ',
    
    kAudioFormatMACE3          = 'MAC3',
    
    kAudioFormatMACE6          = 'MAC6',
    
    kAudioFormatULaw           = 'ulaw',
    
    kAudioFormatALaw           = 'alaw',
    
    kAudioFormatMPEGLayer1     = '.mp1',
    
    kAudioFormatMPEGLayer2     = '.mp2',
    
    kAudioFormatMPEGLayer3     = '.mp3',
    
    kAudioFormatAppleLossless  = 'alac'
    
};

static inline void printCAFAudioFormat(struct CAFAudioFormat *h)
{
    e2d::printStringFromInt(h->mFormatID, e2d::endian_big, "FORMAT_ID:");
    printf("\naudioFormat:--\
           \nmSampleRate:%f\
           \nmFormatID:%u\
           \nmFormatFlags:%u\
           \nmBytesPerPacket:%u\
           \nmFramesPerPacket:%u\
           \nmChannelsPerFrame:%u\
           \nmBitsPerChannel:%u",
           h->mSampleRate, (unsigned int)h->mFormatID, (unsigned int)h->mFormatFlags, (unsigned int)h->mBytesPerPacket, (unsigned int)h->mFramesPerPacket, (unsigned int)h->mChannelsPerFrame, (unsigned int)h->mBitsPerChannel);
}

class E2DAudioLoader : public E2D_Obj_ {
    
    E2DFileManager      *fileManager;
    
    ALuint loadWAV(const char *fileName_);
    ALuint loadCAF(const char *fileName_);
    
public:
   static  E2DAudioLoader *SharedAudioLoader(void);
    
    E2DAudioLoader();
    ~E2DAudioLoader();
    
    E2D_NO_PRINT;
    
    ALuint LoadAudio(const char *name_);
};

#endif /* defined(__Echo2D_2_Audio__E2DAudioLoader__) */
