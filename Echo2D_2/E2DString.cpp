//
//  E2DString.cpp
//  E2DDerivedStructs
//
//  Created by Vijay Chandran J on 02/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DString.h"

E2DString::E2DString(void) : _string(NULL), _length(0)
{
    this->setClassName("E2DString");
    this->setClassType(E2D_CLASS_STRING);
}

//*
E2DString::E2DString(const char *string_)
{
    this->setClassName("E2DString");
    this->setClassType(E2D_CLASS_STRING);
    this->copyString(string_);
}
//*/

E2DString::~E2DString(void)
{
    E2D_FREE(_string);
    _length = 0;
}

void E2DString::Set(const char *string_)
{
    this->copyString(string_);
}

void E2DString::Set(E2DString *string_)
{
    this->copyString(string_->_string);
}

void E2DString::Append(const char *string_)
{
    this->addString(string_);
}

void E2DString::Append(const E2DString *string_)
{
    this->addString(string_->_string);
}

bool E2DString::IsEqualTo(const char *s_)
{
    return strcmp(_string, s_) == 0;
}

bool E2DString::IsEqualTo(const E2DString *s_)
{
    return strcmp(_string, s_->_string) == 0;
}

const char * E2DString::String(void)
{
    return _string;
}

size_t E2DString::Length(void)
{
    return _length;
}

void E2DString::Empty(void)
{
    E2D_FREE(_string);
    _length = 0;
}

void E2DString::Print() const
{
    E2D_SYS_PRINT("%s", _string);
}

void E2DString::Print(const char *tag_) const
{
    E2D_SYS_PRINT("%s:%s",tag_,  _string);
}

void E2DString::deleteExtension(void)
{
    if (!_string) return;
    
//    E2D_PRINT("\nl:%lu", _length);
    for (int i = ((int)_length - 1); i >= 0; i--)
    {
//        E2D_PRINT("\ni:%d, l:%lu", i, _length);
//        E2D_PRINT("\n%c", _string[i]);
        if (_string[i] == '.')
        {
            _string[i] = '\0';
        }
    }
}

//[e2drem]pre-parse extension
size_t E2DString::Extension(char **out_)
{
    if (!_string) return 0;
    //*out_[0] = '\0';
    E2D_FREE(*out_);
    
    size_t length = 0;
    
    for (size_t i = (_length - 1); i > 0; i--)
    {
        if (_string[i] == '.')
        {
            bool stop = false;
            for (size_t j = (i + 1); j < _length; j++)
            {
                const char c[2] = {_string[j], '\0'}; //or 0
                length++;
                *out_ = (char *)realloc(*out_, length);
                strcat(*out_, c);
                stop = true;
            }
            
            if(stop)
                break;
        }
    }
    return length;
}

