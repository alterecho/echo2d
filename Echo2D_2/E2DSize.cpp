//
//  E2DSize.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 19/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DSize.hpp"



//inline
void E2Dsize::operator()(E2Dfloat width_, E2Dfloat height_)
{
    width = width_;
    height = height_;
}

//inline
void E2Dsize::operator()(E2Dsize& size_)
{
    *this = size_;
}

//inline void operator

void E2Dsize::Set(E2Dfloat width_, E2Dfloat height_)
{
    width = width_;
    height = height_;
}

E2Dsize& E2Dsize::operator=(const E2Dsize& size)
{
    width = size.width;
    height = size.height;
    
    return *this;
}

E2Dsize E2Dsize::operator+(const E2Dsize& size)
{
    return E2Dsize(width + size.width, height + size.height);
}

E2Dsize E2Dsize::operator-(const E2Dsize & size )
{
    return E2Dsize(width - size.width, height - size.height);
}

E2Dsize& E2Dsize::operator+=(const E2Dsize& size)
{
    width += size.width;
    height += size.height;
    
    return *this;
}

E2Dsize& E2Dsize::operator-=(const E2Dsize& size)
{
    width -= size.width;
    height -= size.height;
    
    return *this;
}

E2Dsize E2Dsize::operator*(const E2Dsize& s_)
{
    E2Dsize ret(width * s_.width, height * s_.height);
    return ret;
}

E2Dsize E2Dsize::operator*(const float f_)
{
    E2Dsize ret(width * f_, height * f_);
    return ret;
}

E2Dsize E2Dsize::operator*=(const E2Dsize& size)
{
    width *= size.width;
    height *= size.height;
    
    return *this;
}

E2Dsize E2Dsize::operator*=(const float v)
{
    width *= v;
    height *= v;
    
    return *this;
}

E2Dsize& E2Dsize::operator++()
{
    ++width;
    ++height;
    
    return (*this);
}

E2Dsize& E2Dsize::operator++(int)
{
    width++;
    height++;
    
    return (*this);
}

E2Dsize E2Dsize::operator--()
{
    --width;
    ++height;
    
    return (*this);
}

E2Dsize& E2Dsize::operator--(int)
{
    width--;
    height--;
    
    return (*this);
}

bool E2Dsize::operator==(const E2Dsize& size)
{
    return (width == size.width && height == size.height);
}

/*
 friend std::ostream& E2Dsize::operator<<(std::ostream &out, const E2Dsize &size)
 {
 
 }
 // */

void E2Dsize::Print() const
{
    E2D_PRINT("\nsize (%f, %f)", width, height);
}

void E2Dsize::Print(const char *s) const
{
    E2D_PRINT("\n%s (%f, %f)", s, width, height);
}
