//
//  E2DController.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_CONTROLLER_H
#define E2D_CONTROLLER_H

#include <iostream>

#include "E2D_Obj_.hpp"
#include "E2DDataStructs.hpp"

class E2DController : public E2D_Obj_, public e2d::protocol::TouchEvents {
    
    class E2DSceneManager     *sceneManager;

protected:
    E2Dsize             _screenSize;
    E2Dfloat            _screenScale;
    
    class E2DFileManager      *fileManager;
    class E2DImageLoader      *imageLoader;
    class E2DRenderer         *renderer;
    class E2DTextureManager   *textureManager;
    class E2DFontManager      *fontManager;
    class E2DKeyboard         *keyboard;
    class E2DOverlay          *overlay;
    class E2DAudioLoader      *audioLoader;
    class E2DFXPlayer         *fxPlayer;
    class E2DMusicPlayer      *musicPlayer;
    
    //void initialize();      /* called by subclass, after initializing filemanager and imageloader */
    void initManagers();
    void initModules();
    
    void releaseManagers();
    void releaseModules();
    
    E2D_NO_PRINT
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_CONTROLLER; }
    static E2DController* SharedController();
    
    E2DController(); /*override fileManager, renderer, imageLoader*/
    virtual ~E2DController();
    
    virtual bool touchBegan(E2Dpoint);
    virtual void touchMoved(E2Dpoint);
    virtual void touchEnded(E2Dpoint);
    
    
    E2Dsize ScreenSize(void);
    E2Dfloat ScreenScale(void);
    
    virtual void update(E2Delapsed dt_);
    void render() const {};
    
    void ReplaceScene(class E2DScene *);
    
    struct E2DMusicPlayer* MusicPlayer(void);
    struct E2DFXPlayer* FXPlayer(void);
    
    void ActivateKeyboard(void);
    void DismissKeyboard(void);
    
};

#endif /* defined(__Echo2D_2__E2DController__) */
