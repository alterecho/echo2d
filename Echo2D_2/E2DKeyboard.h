//
//  E2DKeyboard.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_KEYBOARD_H
#define E2D_KEYBOARD_H

#include <iostream>
#include "E2D_Obj_.hpp"
#include "E2DDataStructs.hpp"
#include "E2DObjectTypes.h"
#include "E2DKey.h"
#include "E2DObjectTypes.h"

#define E2D_ASCII_NUM_INDX      48  //numbers
#define E2D_ASCII_CASE_U_INDX   65  //uppercase
#define E2D_ASCII_CASE_L_INDX   97  //lowercase

#define E2D_KEYBOARD_KEY_PADDING_X      4
#define E2D_KEYBOARD_KEY_PADDING_Y      2

typedef enum {
    e2dkb_layout_alpha_l,
    e2dkb_layout_alpha_u,
    e2dkb_layout_num
} E2DKeyboardLayout;

class E2DKeyboard : public E2D_Obj_, public e2d::protocol::KeyboardEvents {
    friend class E2DRendererES2;
    friend class E2DOverlay;
        
    E2Dvertex       *vertices;
    unsigned short  v_index;
    E2Duint         vertices_count;
    E2Dsize         screenSize, letterSize, bgSize;
    E2Dpoint        screenCenter;
    E2DString       *fontFile;
    //E2DSprite       *btn_bg_def;
    E2Duint         tID_text, tID_bg;
    
    E2DDictionary   *layout_a, *layout_A, *layout_$; /* lowercase, uppercase, numbers */
    
    void initLayout_a();
    void initializeVertices();
    
    void setKeyBGColor(E2Dcolor);
    void setKeyTextColor(E2Dcolor);
    
protected:
    bool            nativeKeyboard;
    
public:
    static E2DKeyboard* SharedKeyboard(void);
    E2DKeyboard();
    virtual ~E2DKeyboard();
    
    void DeleteText();
    bool HasText();
    void InsertText(const char *t_);
    
    virtual void Activate(void);
    virtual void Dismiss(void);
    
    E2D_NO_PRINT;
};

#endif /* defined(__Echo2D_2__E2DKeyboard__) */
