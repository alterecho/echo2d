//
//  E2DShaders.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 13/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_SHADERS_H
#define E2D_SHADERS_H

#define _E2D_SHADER_VERT_DEFAULT            "default.vsh"
#define _E2D_SHADER_FRAG_DEFAULT            "default.fsh"

#define _E2D_SHADER_VERT_NO_TEXTURE         "default.vsh"
#define _E2D_SHADER_FRAG_NO_TEXTURE         "colors.fsh"

#define _E2D_SHADER_VERT_PARTICLES          "particles.vsh"
#define _E2D_SHADER_FRAG_PARTICLES          "particles.fsh"


#endif
