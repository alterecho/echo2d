//
//  E2DLayer.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 01/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DLayer.h"

#include "E2DDrawable.h"
#include "E2DSprite.h"
#include "E2DRenderer.h"
#include "E2DController.h"
#include "E2DEmitter.h"

E2DLayer::E2DLayer(E2Ddepth depth_)
{
    this->setClassName("E2DLayer");
    this->setClassType(E2D_CLASS_LAYER);
    __screenSize = E2DController::SharedController()->ScreenSize();
    __size = __screenSize;
    
    __renderer = E2DRenderer::SharedRenderer();
    __depth = depth_;
    visible = true;
    
    
    __drawables = new E2DArray();
    __emitters = new E2DArray(10);
}

E2DLayer::~E2DLayer()
{
    E2D_RELEASE(__emitters);
    E2D_RELEASE(__drawables);
    __renderer = 0;
    __depth = 0;
    __renderer = nullptr;
}

void E2DLayer::print(const char *str_) const
{
    
}

void E2DLayer::sortDrawables()
{
    for (E2Dull_t i = 0; i < __drawables->Size(); i++)
    {
        E2DDrawable *d_i = (E2DDrawable *)__drawables->ObjectAtIndex(i);
        if (!d_i)
            continue;
        
        for (E2Dull_t j = i; j < __drawables->Size(); j++)
        {
            E2DDrawable *d_j = (E2DDrawable *)__drawables->ObjectAtIndex(j);
            if (!d_j)
                continue;
            
            if (d_j->__depth < d_i->__depth)
            {
                d_i = d_j;
                __drawables->SwapObjects(i, j);
            }
        }        
    }
    
    
}

E2Ddepth E2DLayer::Depth() const
{
    return __depth;
}

void E2DLayer::render(E2Delapsed dt_)
{
    if (!visible)
        return;
    
    //E2D_SYS_PRINT("depth:%lu", __depth);
    while (E2DDrawable *temp = (E2DDrawable *)__drawables->NextObject())
    {
        if (!temp)
            continue;
        //temp->Print();
        __renderer->BindTexture(temp->_textureID);
        //__renderer->Render((E2DSprite *)temp);
        temp->render();
    }
    while (E2DEmitter *temp = (E2DEmitter *)__emitters->NextObject()) {
        temp->render();
        temp->update(dt_);
    }
}

void E2DLayer::AddDrawable(E2DDrawable *drawable_)
{
    drawable_->__layer = this;
    
    this->sortDrawables();
    if (drawable_->ClassType() == E2DEmitter::Class())
    {
        E2D_PRINT("\nEMITTER adding............");
        __emitters->AddObject(drawable_);
    }
    else
    {
        __drawables->AddObject(drawable_);
    }
}

void E2DLayer::RemoveDrawable(E2DDrawable *drawable_)
{
    if (!__drawables->ContainsObject(drawable_))
        return;
    drawable_->__layer = nullptr;
    __drawables->RemoveObject(drawable_);
    this->sortDrawables();
}

void E2DLayer::SetScale(E2Dclampf s_)
{
    E2DGeometry::SetScale(s_);
    __size = __screenSize * s_;
}

bool E2DLayer::touchBegan(E2Dpoint p_)
{
    E2D_SYS_PRINT("tbegan:(%f, %f)", p_.x, p_.y);
    return false;
}

void E2DLayer::touchMoved(E2Dpoint p_)
{
    E2D_SYS_PRINT("tMoved:(%f, %f)", p_.x, p_.y);
}

void E2DLayer::touchEnded(E2Dpoint p_)
{
    E2D_SYS_PRINT("tEnded:(%f, %f)", p_.x, p_.y);
}


