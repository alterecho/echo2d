//
//  main.m
//  Echo2D_2
//
//  Created by Vijay Chandran J on 10/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
