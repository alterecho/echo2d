//
//  E2DRenderer.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 10/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_RENDERER_H
#define E2D_RENDERER_H

#include <iostream>
#include <OpenGLES/ES2/gl.h>

#include "E2D_Obj_.hpp"
#include "E2DDataStructs.hpp"

typedef enum {
    E2D_BLEND_FN_ADDITIVE = 1,
    E2D_BLEND_FN_LUMINOUS,
    E2D_BLEND_FN_DEFAULT
} E2DblendFunc;

class E2DRenderer : public E2D_Obj_ {
    
protected:
    GLuint          _frameBuffer, _colorRenderBuffer;
    
    E2Dsize         _screenSize, _screenSize_half;
    E2DblendFunc    _blendFunc;
    
    void setupBuffers();
    void setupViewport(GLfloat width_, GLfloat height_);
    
public:
     static inline E2Dclass_t Class(void) { return E2D_CLASS_RENDERER; }
    static E2DRenderer *SharedRenderer();
    
    E2DRenderer(E2Dsize screenSize_);
    virtual ~E2DRenderer();
    
    E2D_NO_PRINT
    
    E2Dsize CreateTexture(const char *fileName_, GLuint *tex_);
    void BindTexture(GLuint tex_) const;
    void Clear(void) const;
    void SetClearColor(E2Dclampf r_, E2Dclampf g_, E2Dclampf b_, E2Dclampf a_) const;
    void SetClearColor(E2Dcolor) const;
    void SetBlendFunc(E2DblendFunc);
    
    virtual void EnableDefaultVertexAttributes(void) const = 0;
    virtual void DisableDefaultVertexAttributes(void) const = 0;
    
    virtual inline void Render() = 0;
    virtual inline void Render(const class E2DDrawable4 *d_) = 0 ;
    virtual inline void Render(const class E2DLabel *lbl_) = 0;
    virtual inline void Render(const class E2DEmitter *) = 0;
    
    virtual inline void renderKB(const class E2DKeyboard *kb_) = 0;
    
    
    
    virtual void UploadVertexArray(const GLvoid **ptr_, E2Dsizei stride_) const = 0;
    virtual void UploadColorArray(const GLvoid **ptr_, E2Dsizei stride_) const = 0;
    virtual void UploadTexCoordArray(const GLvoid **ptr_, E2Dsizei stride_) const = 0;
    
};

#endif /* defined(__Echo2D_2__E2DRenderer__) */
