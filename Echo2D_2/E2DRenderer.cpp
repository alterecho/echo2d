//
//  E2DRenderer.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 10/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DRenderer.h"
//#include <fstream>
#include "E2DImageLoader.h"
#include "E2DDrawable.h"

using namespace std;

static E2DRenderer*     instanceOfRenderer(nullptr);

E2DRenderer* E2DRenderer::SharedRenderer()
{
    E2D_ASSERT(instanceOfRenderer, "E2DRenderer not initialized");
    return instanceOfRenderer;
}

E2DRenderer::E2DRenderer(E2Dsize screenSize_) : E2D_Obj_()
{
    this->setClassName("E2DRenderer");
    this->setClassType(E2D_CLASS_RENDERER);
    
    instanceOfRenderer = this;
    
    _screenSize = screenSize_;
    _screenSize_half = screenSize_ / 2.0f;
    setupBuffers();
    setupViewport(_screenSize.width, _screenSize.height);
    
    glDisable(GL_DITHER);
    //glDisable(GL_FOG);
    glDisable(GL_STENCIL_TEST);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    //glDisable(GL_DEPTH_TEST);
    
    /*
    E2Dmatrix<4> m1 = E2Dmatrix<4>::Identity();
    //m1.SetX(10); m1.SetY(5);
    m1.m[0][0] =  5, m1.m[0][1] = 10, m1.m[0][2] = 0, m1.m[0][3] = 1;
    m1.m[1][0] =  7, m1.m[1][1] = 2, m1.m[1][2] = 4, m1.m[1][3] = 0;
    m1.m[2][0] =  8, m1.m[2][1] = 2, m1.m[2][2] = 5, m1.m[2][3] = 10;
    m1.m[3][0] =  1, m1.m[3][1] = 8, m1.m[3][2] = 6, m1.m[3][3] = 2;
                          
    E2Dmatrix<4> m2 = E2Dmatrix<4>::Identity();
    m2.m[0][0] =  10, m2.m[0][1] = 5, m2.m[0][2] = 2, m2.m[0][3] = 0;
    m2.m[1][0] =  5, m2.m[1][1] = 4, m2.m[1][2] = 6, m2.m[1][3] = 8;
    m2.m[2][0] =  8, m2.m[2][1] = 2, m2.m[2][2] = 0, m2.m[2][3] = 1;
    m2.m[3][0] =  2, m2.m[3][1] = 4, m2.m[3][2] = 10, m2.m[3][3] = 8;
    
    E2Dmat4 m3 = m1 * m2;
    m3.Print();
     //*/
}

E2DRenderer::~E2DRenderer()
{
    E2D_SYS_PRINT("destroying...");
    instanceOfRenderer = nullptr;
}

void E2DRenderer::setupBuffers()
{
    E2D_SYS_PRINT("setting up buffers...");
    
    glGenFramebuffers(1, &_frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);
}

void E2DRenderer::setupViewport(GLfloat width_, GLfloat height_)
{
    E2D_SYS_PRINT("setting up viewport %f X %f...", width_, height_);
    glViewport(0.0f, 0.0f, _screenSize.width, _screenSize.height);
}

E2Dsize E2DRenderer::CreateTexture(const char *fileName_, GLuint *tex)
{
    E2D_PRINT("\ncreating texture");
    E2Dsize  ret;
    
    GLubyte *imgPntr = NULL;
    E2Dsize size;
    ret = E2DImageLoader::SharedImageLoader()->LoadImage(fileName_, &imgPntr,  size);
    
    glGenTextures(1, tex);
    glBindTexture(GL_TEXTURE_2D, *tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.width, size.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imgPntr); //imgPntr allocated
    glBindTexture(GL_TEXTURE_2D, 0);
    
    E2D_FREE(imgPntr);
    
    return ret;
}

void E2DRenderer::BindTexture(GLuint tex_) const
{
    glBindTexture(GL_TEXTURE_2D, tex_);
}

void E2DRenderer::Clear() const
{
    glClear(GL_COLOR_BUFFER_BIT);
}

void E2DRenderer::SetClearColor(E2Dcolor c_) const
{
    glClearColor(c_.r, c_.g, c_.b, c_.a);
}

void E2DRenderer::SetClearColor(E2Dclampf r_, E2Dclampf g_, E2Dclampf b_, E2Dclampf a_) const
{
    glClearColor(r_, g_, b_, a_);
}

void E2DRenderer::SetBlendFunc(E2DblendFunc fn_)
{
    if (fn_ == _blendFunc) return;
    
    _blendFunc = fn_;
    
    switch (fn_)
    {
        case E2D_BLEND_FN_ADDITIVE:
            glBlendFunc(GL_ONE, GL_ONE);
            break;
        case E2D_BLEND_FN_LUMINOUS:
            glBlendFunc(GL_SRC_ALPHA, GL_ONE);
            break;
        default:
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
            break;
    }
    
}


