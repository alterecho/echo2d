//
//  E2Dvec3.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 03/06/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_VEC_3_H
#define E2D_VEC_3_H

#include <iostream>
#include "E2DDataTypes.h"

struct E2Dvec3 {
    E2Dfloat x, y, z;
    
    E2Dvec3(E2Dfloat x_, E2Dfloat y_, E2Dfloat z_) : x(x_), y(y_), z(z_) { }
    E2Dvec3(void) : x(0.0f), y(0.0f), z(0.0f) { }
    
    E2Dvec3 operator()(E2Dfloat x_, E2Dfloat y_, E2Dfloat z_)
    {
        x = x_, y = y_, z = z_;
        return *this;
    }
    
    E2Dvec3 operator+(E2Dvec3& rhs_)
    {
        return E2Dvec3(x + rhs_.x, y + rhs_.y, z + rhs_.z);
    }
    
    E2Dvec3 operator+=(E2Dvec3& rhs_)
    {
        x += rhs_.x;
        y += rhs_.y;
        z += rhs_.z;
        
        return *this;
    }
    
    E2Dvec3 operator-(E2Dvec3& rhs_)
    {
        return E2Dvec3(x - rhs_.x, y - rhs_.y, z - rhs_.z);
    }
    
    E2Dvec3 operator-=(E2Dvec3& rhs_)
    {
        x -= rhs_.x;
        y -= rhs_.y;
        z -= rhs_.z;
        
        return *this;
    }
    
    E2Dvec3 operator*(const E2Dvec3& rhs_)
    {
        return E2Dvec3(x * rhs_.x, y * rhs_.y, z * rhs_.z);
    }
    
    E2Dvec3 operator*=(const E2Dvec3& rhs_)
    {
        x *= rhs_.x;
        y *= rhs_.y;
        z *= rhs_.z;
        
        return *this;
    }
    
    bool operator==(const E2Dvec3& rhs_)
    {
        return (x == rhs_.x && y == rhs_.y && z == rhs_.z);
    }
    
    void Print(void) const
    {
        E2D_PRINT("\nvec3:(%f, %f, %f)", x, y, z);
    }
    
    void Print(const char *msg_) const
    {
        E2D_PRINT("\n%s (%f, %f, %f)", msg_, x, y, z);
    }
    
    friend std::ostream& operator<<(std::ostream& stream_, const E2Dvec3& rhs_)
    {
        return stream_ << "(" << rhs_.x << ", " << rhs_.y << ", " << rhs_.z << ")";
    }
    
};

#endif /* defined(__Echo2D_2__E2Dvec3__) */
