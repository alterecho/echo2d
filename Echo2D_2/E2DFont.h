//
//  E2DFont.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 07/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_FONT_H
#define E2D_FONT_H

#include <iostream>
#include "E2DDrawable4Z.h"

class E2DFont : public E2DDrawable4Z {
    char            __char;
    
protected:
    virtual void print(const char *) const;
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_FONT; }
    E2DFont(char, E2Dsize, const E2DtexCoord *, E2Duint);
    virtual ~E2DFont();
    
    E2DFont* Copy(void) const;
    
    //void SetPosition(E2Dpoint p_);
    
    const char Char(void) const;
};

#endif /* defined(__Echo2D_2__E2DFont__) */
