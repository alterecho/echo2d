//
//  E2DFontManager.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 07/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_FONT_MANAGER_H
#define E2D_FONT_MANAGER_H

#include <iostream>
#include "E2D_Obj_.hpp"
#include "E2DObjectTypes.h"

class E2DFont;
class E2DFontManager : public E2D_Obj_ {
    
    class E2DFileManager        * __fileManager;
    class E2DRenderer           *__renderer;
    E2DDictionary               *__fontFiles;
    
protected:
    void print(const char *) const;
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_FONT_MANAGER;}
    static E2DFontManager* SharedFontManager(void);
    E2DFontManager(void);
    virtual ~E2DFontManager();
    
    
    
    void CacheFontFile(const char *file_);
    E2Duint FontFileTextureID(const char *file_);
    const E2DFont* FontForCharacter(const char char_, const char *fontFile_);
};

#endif /* defined(__Echo2D_2__E2DFontManager__) */
