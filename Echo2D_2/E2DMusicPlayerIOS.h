//
//  E2DMusicPlayer.h
//  Echo2D_2_Audio
//
//  Created by Vijay Chandran J on 13/06/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#ifndef E2D_MUSIC_PLAYER_IOS_H
#define E2D_MUSIC_PLAYER_IOS_H

#include <iostream>
#include "E2DMusicPlayer.h"
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAudioSession.h>
#include "E2DProtocols.h"

#pragma mark -
#pragma mark E2Dplayer
#pragma mark -
class E2DPlayerIOS : public E2D_Obj_ {
    friend class E2DMusicPlayerIOS;
    
    E2DString           *fileName;
    AVAudioPlayer       *player;
    E2Dseconds          time;
    
    void print(const char *msg_) const
    {
        E2D_PRINT("%s", fileName->String());
    }
    
    E2DPlayerIOS(const char *);
    ~E2DPlayerIOS();
    
};

#pragma mark -
#pragma mark E2DAudioPlayerIOSDelegate
#pragma mark -
@interface E2DMusicPlayerIOSDelegate : NSObject<AVAudioPlayerDelegate, AVAudioSessionDelegate> {
    e2d::protocol::MusicPlayerDelegate       *player;
    
}

- (id)initWithPlayer:(struct E2Dprt::MusicPlayerDelegate *)player_;

@end

#pragma mark -
#pragma mark E2DMusicPlayerIOS
#pragma mark -
class E2DMusicPlayerIOS : public E2DMusicPlayer {
    E2DDictionary       *players;
    E2DPlayerIOS        *currentPlayer;
    
    E2DMusicPlayerIOSDelegate       *delegate;
    
public:
    E2DMusicPlayerIOS();
    ~E2DMusicPlayerIOS();
    
    void musicPlaybackFinished(void);
        
    void PreloadMusic(const char *fileName_);
    void Play(const char *fileName_, bool loop_);
    void Play(bool loop_);
    void Pause(void) const;
    void Stop(void) const;
    const char* CurrentMusic(void) const;
    bool IsPlaying(void) const;
    bool IsPlayingMusic(const char *) const;
    
    
};

#endif /* defined(__Echo2D_2_Audio__E2DMusicPlayer__) */
