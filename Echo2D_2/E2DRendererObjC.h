//
//  E2DRenderer.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 10/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

#include "E2DDataTypes.h"
#include "E2DDataStructs.hpp"

@interface E2DRendererObjC : NSObject {
    //EAGLContext         *context;
    //CAEAGLLayer         *layer;
    E2Dsize             screenSize;
    
    GLuint              frameBuffer, colorRenderBuffer;
    GLuint              vertexShader, fragmentShader, program;
}

- (id)initWithScreenSize:(E2Dsize)screenSize_;
- (void)render;
- (GLuint)createTexture:(NSString *)file_;

@end
