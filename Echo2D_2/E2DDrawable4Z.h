//
//  E2DDrawable4Z.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_DRAWABLE_4Z_H
#define E2D_DRAWABLE_4Z_H

#include <iostream>
#include "E2DDrawable4.h"

class E2DDrawable4Z : public E2DDrawable4 {
protected:
    E2DDrawable4Z(E2Ddepth depth_);
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_DRAWABLE4Z; }
    virtual ~E2DDrawable4Z();
    
    void setSize(float width, float height);
};


#endif /* defined(__Echo2D_2__E2DDrawable4Z__) */
