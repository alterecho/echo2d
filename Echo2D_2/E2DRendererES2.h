//
//  E2DRendererES2.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_RENDERER_ES_2
#define E2D_RENDERER_ES_2

#include <iostream>
#include "E2DRenderer.h"
#include "E2DFileManager.h"

typedef struct {
    GLint pos, col, texc, tex, psize;
} E2DvtxAttribs;

class E2DRendererES2 : public E2DRenderer {
    
    GLuint              program, program_prev, program_def, program_colors, program_particles;
    E2DvtxAttribs       attribs;
    E2Dmat4             modelMatrix;
    GLint               modelView;
    
    
    GLuint newShader(const char *source_, GLenum type_);
    GLuint newProgram(GLuint vShader_, GLuint fShader_);
    void useProgram(GLuint);
    void setupProgram(GLuint);
    void setupProjection(GLfloat left_, GLfloat right_, GLfloat bottom_, GLfloat top_,  GLfloat near_, GLfloat far_, GLint program_);
    void scale(GLfloat mat[], GLfloat x, GLfloat y, GLfloat z);
    
    GLuint          testTex = 0;
    
    //GLfloat         matrix[16] = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    inline void printUniform(const GLchar *name_, GLuint program_, GLuint count_, GLuint break_, const char *msg_);
    
public:
    E2DRendererES2(E2Dsize screenSize_);
    virtual ~E2DRendererES2();
    
    void EnableDefaultVertexAttributes(void) const;
    void DisableDefaultVertexAttributes(void) const;
    
    inline void Render();
    inline void Render(const class E2DDrawable4 *);
    inline void Render(const class E2DLabel *);
    inline void Render(const class E2DEmitter *);
    
    inline void renderKB(const class E2DKeyboard *kb_);
        
    E2DvtxAttribs VertexAttributes(void);
    
    void UploadVertexArray(const GLvoid **ptr_, E2Dsizei stride_) const;
    void UploadColorArray(const GLvoid **ptr_, E2Dsizei stride_) const;
    void UploadTexCoordArray(const GLvoid **ptr_, E2Dsizei stride_) const;
    
};

#endif /* defined(__Echo2D_2__E2DRendererES2__) */
