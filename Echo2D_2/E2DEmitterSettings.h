//
//  E2DEmitterSettings.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 15/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_EMITTER_SETTINGS_H
#define E2D_EMITTER_SETTINGS_H

#include <iostream>
#include "E2DDataStructs.hpp"
#include "E2DparticleAttribs.hpp"
#include "E2DparticleLimits.hpp"
#include "E2DparticleMod.hpp"

class E2DEmitterSettings {
    
protected:
    unsigned int            activeParticles_MAX;
    
    //E2DparticleAttribs      att;
    //E2DparticleMod          mod;
    E2DpLimits              lim;
    
    bool                scale, morphColor, oov, fade;
    bool                transMotion, rotMotion, radMotion; /* translational, rotational, radial */
    struct {
        bool bottom, right, top, left;
    } rebound;
    
    E2Dvec2           gravity;
    
    void print() const;
    
    
public:
    E2DEmitterSettings() { };//mod.lim = &lim; };
    
    void SetActiveParticles(unsigned int);
    unsigned int ActiveParticles(void) const;
    
#pragma mark - setters/getters -
    
    
    void SetMaxLifeTime(E2Dseconds);
    E2Dseconds MaxLifeTime(void) const;
    
    void SetMinLifeTime(E2Dseconds);
    E2Dseconds MinLifeTime(void) const;
    
    
#pragma mark - color -
    void SetMinColorStart(E2Dcolor);
    E2Dcolor MinColorStart(void) const;
    
    void SetMaxColorStart(E2Dcolor);
    E2Dcolor MaxColorStart(void) const;
    
    void SetMorphColor(bool);
    bool MorphColor(void) const;
    
    void SetMinColorEnd(E2Dcolor);
    E2Dcolor MinColorEnd(void) const;
    
    void SetMaxColorEnd(E2Dcolor);
    E2Dcolor MaxColorEnd(void) const;
    
    
    
    void SetMinOpacityStart(E2Dclampf);
    E2Dclampf MinOpacityStart(void) const;
    
    void SetMaxOpacityStart(E2Dclampf);
    E2Dclampf MaxOpacityStart(void) const;
    
    void SetFade(bool);
    bool Fade(void) const;
    
    void SetMinOpacityEnd(E2Dclampf);
    E2Dclampf MinOpacityEnd() const;
    
    void SetMaxOpacityEnd(E2Dclampf);
    E2Dclampf MaxOpacityEnd() const;
    
    
#pragma mark - size -
    void SetMinScaleStart(E2Dfloat);
    E2Dfloat MinScaleStart(void) const;
    
    void SetMaxScaleStart(E2Dfloat);
    E2Dfloat MaxScaleStart(void) const;
    
    void SetScale(bool);
    bool Scale(void) const;
    
    void SetMinScaleEnd(E2Dfloat);
    E2Dfloat MinScaleEnd() const;
    
    void SetMaxScaleEnd(E2Dfloat);
    E2Dfloat MaxScaleEnd() const;
    
    
#pragma mark - transtaltional motion -
    void SetTranslationalMotion(bool);
    bool TranslationalMotion(void) const;
    
    void SetMinVelocity(E2Dvec2);
    E2Dvec2 MinVelocity(void) const;
    
    void SetMaxVelocity(E2Dvec2);
    E2Dvec2 MaxVelocity(void) const;
    
    void SetMinVelocityDamping(E2Dvec2);
    E2Dvec2 MinVelocityDamping(void) const;
    
    void SetMaxVelocityDamping(E2Dvec2);
    E2Dvec2 MaxVelocityDamping(void) const;
    
    void SetMaxSpreadVelocity(E2Dvec2);
    E2Dvec2 MaxSpreadVelocity(void) const;
    
    void SetMinSpreadVelocity(E2Dvec2);
    E2Dvec2 MinSpreadVelocity(void) const;
    
    void SetGravity(E2Dvec2);
    E2Dvec2 Gravity(void) const;
    
    
#pragma mark - rotational motion -
    void SetSpin(bool);
    bool Spin(void) const;
    
    void SetMinAngle(E2Ddegrees);
    E2Ddegrees MinAngle(void) const;
    
    void SetMaxAngle(E2Ddegrees);
    E2Ddegrees MaxAngle(void) const;
    
    
#pragma mark - radial motion -
    void SetRadialMotion(bool);
    bool RadialMotion(void) const;
    
    void SetMinRadialDistance(E2Dfloat);
    E2Dfloat MinRadialDistance(void) const;
    void SetMaxRadialDistance(E2Dfloat);
    E2Dfloat MaxRadialDistance(void) const;
    
    void SetMinRadialDistanceDelta(E2Dfloat);
    E2Dfloat MinRadialDistanceDelta(void) const;
    void SetMaxRadialDistanceDelta(E2Dfloat);
    E2Dfloat MaxRadialDistanceDelta(void) const;
    
    void SetMinRadialDistanceLimit(E2Dfloat);
    E2Dfloat MinRadialDistanceLimit(void) const;
    void SetMaxRadialDistanceLimit(E2Dfloat);
    E2Dfloat MaxRadialDistanceLimit(void) const;
    
    void SetMinRadialAngle(E2Ddegrees);
    E2Ddegrees MinRadialAngle(void) const;
    void SetMaxRadialAngle(E2Ddegrees);
    E2Ddegrees MaxRadialAngle(void) const;
    
    void SetMinRadialAngleDelta(E2Ddegrees);
    E2Ddegrees MinRadialAngleDelta(void) const;
    void SetMaxRadialAngleDelta(E2Ddegrees);
    E2Ddegrees MaxRadialAngleDelta(void) const;
    
    
#pragma mark - bounds -
    void SetOutOfViewAllowed(bool);
    bool OutOfViewAllowed(void) const;
};

#endif /* defined(__Echo2D_2__E2DEmitterSettings__) */
