//
//  E2DGLView.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#include "E2DSize.hpp"

@interface E2DGLView : UIView <UIKeyInput> {
@public
    E2Dsize screenSize;
    struct E2DController *controller;
}

+ (E2DGLView *)sharedGLView;

@end
