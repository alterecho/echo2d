//
//  E2DRendererES2.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DRendererES2.h"
#include "E2DShaders.h"
#include "E2DImageLoader.h"
#import <CoreGraphics/CoreGraphics.h>

#define ATTRIB_POSITION     "position"
#define ATTRIB_COLOR        "color"
#define ATTRIB_TEX_COORD    "texCoord"
#define ATTRIB_POINT_SIZE   "pointSize"

#define UNIFORM_PROJECTION  "projection"
#define UNIFORM_MODEL_VIEW  "modelView"
#define UNIFORM_TEXTURE     "texture"

#include "E2DLayer.h"
#include "E2DDrawable4.h"
#include "E2DLabel.h"

#include "E2DEmitter.h"

#include "E2DKeyboard.h"


E2DRendererES2::E2DRendererES2(E2Dsize screenSize_) : E2DRenderer(screenSize_)
{
    this->setClassName("E2DRendererES2");
    this->setClassType(E2D_CLASS_RENDERER);
    
    modelMatrix = E2Dmat4::Identity();
    attribs.pos = -2;
    attribs.col = -2;
    attribs.tex = -2;
    attribs.texc = -2;
    attribs.psize = -2;
    program = 0;
    
    GLuint vShader_def = newShader(_E2D_SHADER_VERT_DEFAULT, GL_VERTEX_SHADER);
    GLuint fShader_def = newShader(_E2D_SHADER_FRAG_DEFAULT, GL_FRAGMENT_SHADER);
    
    GLuint vShader_colors = newShader(_E2D_SHADER_VERT_NO_TEXTURE, GL_VERTEX_SHADER);
    GLuint fShader_colors = newShader(_E2D_SHADER_FRAG_NO_TEXTURE, GL_FRAGMENT_SHADER);
    
    GLuint vShader_particles = newShader(_E2D_SHADER_VERT_PARTICLES, GL_VERTEX_SHADER);
    GLuint fShader_particles = newShader(_E2D_SHADER_FRAG_PARTICLES, GL_FRAGMENT_SHADER);
    
    program_def = newProgram(vShader_def, fShader_def);
    this->setupProgram(program_def);
    glDeleteShader(fShader_def);
    glDeleteShader(fShader_def);
    
    //*
    program_colors = newProgram(vShader_colors, fShader_colors);
    this->setupProgram(program_colors);
    glDeleteShader(vShader_colors);
    glDeleteShader(fShader_colors);
    //*/
    
    //*
    program_particles = newProgram(vShader_particles, fShader_particles);
    this->setupProgram(program_particles);
    glDeleteShader(vShader_particles);
    glDeleteShader(fShader_particles);
    //*/
    
    this->useProgram(program_def);
    
    this->EnableDefaultVertexAttributes();
    
    //s.Print();
    
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
}

E2DRendererES2::~E2DRendererES2()
{
    
}

E2DvtxAttribs E2DRendererES2::VertexAttributes()
{
    return attribs;
}

GLuint E2DRendererES2::newShader(const char *source_, GLenum type_)
{
    GLuint ret;
    
    ret = glCreateShader(type_);
    
    char *buffer = (char *)malloc(sizeof(char) * 2048);
    memset(buffer, 0, 2048);
    E2DFileManager::SharedFileManager()->StringFromContentsOfFile(source_, &buffer);
    
    glShaderSource(ret, 1, &buffer, NULL);
    glCompileShader(ret);
    
    //E2D_PRINT("\nshader %d:\n%s", type_, buffer);
    
    E2D_FREE(buffer);
    
    GLint status;
    glGetShaderiv(ret, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE)
    {
        //memset(buffer, '\0', 256);
        char buf[256] = "";
        glGetShaderInfoLog(ret, sizeof(buf), NULL, buf);
        E2D_EXIT("Error compiling shader type:%X\nmsg:%s",  type_, buf);
    }
    
    return ret;
}

GLuint E2DRendererES2::newProgram(GLuint vShader_, GLuint fShader_)
{
    GLuint ret;
    
    ret = glCreateProgram();
    glAttachShader(ret, vShader_);
    glAttachShader(ret, fShader_);
    glLinkProgram(ret);
    
    GLint status;
    glGetProgramiv(ret, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        char buf[256];
        glGetProgramInfoLog(ret, sizeof(buf), NULL, buf);
        E2D_EXIT("Error linking program\nmsg:%s", buf);
    }
    
    return ret;
}

inline
void E2DRendererES2::setupProgram(GLuint p_)
{
    GLint prog;
    glGetIntegerv(GL_CURRENT_PROGRAM, &prog);

    glUseProgram(p_);
    setupProjection(-_screenSize.width * 0.5f, _screenSize.width * 0.5f,
                    -_screenSize.height * 0.5f, _screenSize.height * 0.5f,
                    -100.0f, 100.0f,
                    p_);
    
    glUseProgram(prog);
}

inline
void E2DRendererES2::useProgram(GLuint p_)
{
    if (p_ == program_prev)
    {
        return;
    }
    
    glUseProgram(p_);
    program_prev = p_;
    
    modelView = glGetUniformLocation(p_, UNIFORM_MODEL_VIEW);
    attribs.pos = glGetAttribLocation(p_, ATTRIB_POSITION);
    attribs.col = glGetAttribLocation(p_, ATTRIB_COLOR);
    attribs.texc = glGetAttribLocation(p_, ATTRIB_TEX_COORD);
    //atribs.tex = glGetAttribLocation(program, UNIFORM_TEXTURE);
    attribs.psize = glGetAttribLocation(p_, ATTRIB_POINT_SIZE);
    
    modelView = glGetUniformLocation(p_, UNIFORM_MODEL_VIEW);
    
    program = p_;
    
}

void E2DRendererES2::setupProjection(GLfloat left_, GLfloat right_, GLfloat bottom_, GLfloat top_,  GLfloat near_, GLfloat far_, GLint program_)
{
    E2D_PRINT("\nsetting up orhogonal projection:\n left:%f, right:%f, bottom:%f, top:%f, near:%f, far:%f", left_, right_, bottom_, top_, near_, far_);
    E2Dmat4 ortho;
    ortho.SetOrtho(left_, right_, bottom_, top_, near_, far_);

    GLint projection = glGetUniformLocation(program_, UNIFORM_PROJECTION);
    glUniformMatrix4fv(projection, 1, 0, ortho.Pointer());
}


inline
void E2DRendererES2::EnableDefaultVertexAttributes() const
{
    glEnableVertexAttribArray(attribs.pos);
    glEnableVertexAttribArray(attribs.col);
    glEnableVertexAttribArray(attribs.texc);
}

inline
void E2DRendererES2::DisableDefaultVertexAttributes() const
{
    glDisableVertexAttribArray(attribs.texc);
    glDisableVertexAttribArray(attribs.col);
    glDisableVertexAttribArray(attribs.pos);
}

inline
void  E2DRendererES2::printUniform(const GLchar *name_, GLuint program_, GLuint count_, GLuint break_, const char *msg_)
{
    float buf[count_];
    GLint loc = glGetUniformLocation(program_, name_);
    glGetUniformfv(program_, loc, buf);
    e2dPrintArray(buf, count_, break_, msg_);
}

inline
void E2DRendererES2::UploadVertexArray(const GLvoid **ptr_, E2Dsizei stride_) const
{
    //glVertexAttribPointer(__attribs.pos, 2, GL_FLOAT, GL_FALSE, sizeof(E2Dvertex), &d_->__v[0].position.x);
    glVertexAttribPointer(attribs.pos, 2, GL_FLOAT, GL_FALSE, sizeof(E2Dvertex), *ptr_);
}

inline
void E2DRendererES2::UploadColorArray(const GLvoid **ptr_, E2Dsizei stride_) const
{
    // glVertexAttribPointer(__attribs.col, 4, GL_FLOAT, GL_FALSE, sizeof(E2Dvertex), &d_->__v[0].color.r);
    glVertexAttribPointer(attribs.col, 4, GL_TRUE, GL_TRUE, sizeof(E2Dvertex), *ptr_);
}

inline
void E2DRendererES2::UploadTexCoordArray(const GLvoid **ptr_, E2Dsizei stride_) const
{
    //glVertexAttribPointer(__attribs.texc, 2, GL_FLOAT, GL_FALSE, , &d_->__v[0].texCoord.s);
    glVertexAttribPointer(attribs.texc, 2, GL_FLOAT, GL_FALSE, sizeof(E2Dvertex), *ptr_);
}

#pragma mark - render -
inline
void E2DRendererES2::Render()
{
    //E2D_PRINT("\nrender test");
    
    GLint pos = -2, col = -2, texc = -2;
    pos = glGetAttribLocation(program, ATTRIB_POSITION);
    col = glGetAttribLocation(program, ATTRIB_COLOR);
    texc = glGetAttribLocation(program, ATTRIB_TEX_COORD);
    
    GLint mv = -2, p = -2;
    mv = glGetUniformLocation(program, UNIFORM_MODEL_VIEW);
    p = glGetUniformLocation(program, UNIFORM_PROJECTION);

    E2D_PRINT("\nmodel view:%d, proj:%d, pos:%d, col:%d, texc:%d", mv, p, pos, col, texc);
    
    GLfloat buf[16];
    glGetUniformfv(program, p, buf);
    E2D_PRINT("\nproj:\n");
    for (char i = 0; i < 16; i++)
    {
        if (i % 4 == 0)
        {
            E2D_PRINT("\n");
        }
        E2D_PRINT("%f, ", buf[i]);
    }
    
    GLfloat mv_mat[] = {
        1.0f, 0.0, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f,
    };
    
    glUniformMatrix4fv(mv, 1, GL_FALSE, mv_mat);
    glGetUniformfv(program, mv, buf);
    E2D_PRINT("\nmodel view:\n");
    for (char i = 0; i < 16; i++)
    {
        if (i % 4 == 0)
        {
            E2D_PRINT("\n");
        }
        E2D_PRINT("%f, ", buf[i]);
    }
    
    GLfloat  vertices[] = {
        -100.0f, 100.0f,
        100.0f, 100.0f,
        -100.0f, -100.0f,
        100.0f, -100.0f,
    };
    
    GLfloat color[] = {
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
    };
    
    GLfloat texCoord[] = {
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f
    };
    
    glEnableVertexAttribArray(pos);
    glEnableVertexAttribArray(col);
    glEnableVertexAttribArray(texc);
    
    glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 2, vertices);
    glVertexAttribPointer(col, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 4, color);
    glVertexAttribPointer(texc, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 2, texCoord);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glDisableVertexAttribArray(texc);
    glDisableVertexAttribArray(col);
    glDisableVertexAttribArray(pos);
    
}

inline
void E2DRendererES2::Render(const E2DDrawable4 *d_)
{
    //GLint modelView = glGetUniformLocation(__program, UNIFORM_MODEL_VIEW);
    const E2DLayer *l = d_->__layer;
    if (!l)
    {
        d_->PrintClassName();
        E2D_EXIT("no layer");
    }

    modelMatrix.SetTransform(-l->__size.width * 0.5f + l->__pos.x + d_->__pos.x * l->__scale,
                             -l->__size.height * 0.5f + l->__pos.y + d_->__pos.y * l->__scale,
                             0.0f,
                             d_->__scale * d_->__layer->__scale,
                             d_->__scale * d_->__layer->__scale,
                             1.0f,
                             E2D_DEGREES_TO_RADIANS(d_->__angle));
    
    glUniformMatrix4fv(modelView, 1, GL_FALSE, modelMatrix.Pointer());
    //d_->Print("rend");
    glVertexAttribPointer(attribs.pos, 2, GL_FLOAT, GL_FALSE, sizeof(E2Dvertex), &d_->__v[0].position.x);
    glVertexAttribPointer(attribs.col, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(E2Dvertex), &d_->__v[0].color.r);
    glVertexAttribPointer(attribs.texc, 2, GL_FLOAT, GL_FALSE, sizeof(E2Dvertex), &d_->__v[0].texCoord.s);
    //glDisableVertexAttribArray(attribs.texc);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    /*
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, testTex);
    glUniform1i(tex, 0);
    // */
}

inline void E2DRendererES2::renderKB(const E2DKeyboard *kb)
{
//    E2D_PRINT("\nol rend");
    
    this->SetBlendFunc(E2D_BLEND_FN_DEFAULT);
    this->useProgram(program_def);
    //*
    modelMatrix.SetTransform(  0.0f,
                               0.0f,
                               0.0f,
                               1.0f,
                               1.0f,
                               1.0f,
                               0.0f);
    
    glUniformMatrix4fv(modelView, 1, GL_FALSE, modelMatrix.Pointer());
    // */
    
    /*
    for (char i = 4; i < 8; i++)
    {
        //E2D_PRINT("\n(%f, %f)", kb->vertices[i].texCoord.s, kb->vertices[i].texCoord.t);
        e2dVertexPrint(kb->vertices[i]);
    }
    E2D_BL;
    //*/
    
    this->BindTexture(kb->tID_bg);
    //glVertexAttribPointer(attribs.pos, 2, GL_FLOAT, GL_FALSE, 0, f);
    glVertexAttribPointer(attribs.pos, 2, GL_FLOAT, GL_FALSE, sizeof(E2Dvertex), &kb->vertices[0].position.x);
    glVertexAttribPointer(attribs.col, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(E2Dvertex), &kb->vertices[0].color.r);
    glVertexAttribPointer(attribs.texc, 2, GL_FLOAT, GL_FALSE, sizeof(E2Dvertex), &kb->vertices[0].texCoord.s);
    //glVertexAttribPointer(attribs.texc, 2, GL_FLOAT, GL_FALSE, 0, tc);
    char count = 26;
    for (short i = 0; i < count; i++)
    {
        glDrawArrays(GL_TRIANGLE_STRIP, 8 * i, 4);
    }
    
    this->BindTexture(kb->tID_text);
    //glVertexAttribPointer(attribs.pos, 2, GL_FLOAT, GL_FALSE, sizeof(E2Dvertex), &kb->vertices[0].position.x);
    //glVertexAttribPointer(attribs.col, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(E2Dvertex), &kb->vertices[0].color.r);
    //glVertexAttribPointer(attribs.texc, 2, GL_FLOAT, GL_FALSE, sizeof(E2Dvertex), &kb->vertices[0].texCoord.s);
    for (short i = 0; i < count; i++)
    {
        glDrawArrays(GL_TRIANGLE_STRIP, 8 * i + 4, 4);
    }
    
}

inline
void E2DRendererES2::Render(const E2DLabel *lbl_)
{
    //this->useProgram(__program_noTex);
    glBindTexture(GL_TEXTURE_2D, lbl_->TextureID());
    
    //GLint modelView = glGetUniformLocation(__program, UNIFORM_MODEL_VIEW);
    
    const E2DLayer *l = lbl_->__layer;
    //lbl_->__pos.Print();
    //lbl_->Print();
    /*
    E2D_BL;
    E2D_BL;
    lbl_->__pos.Print("lbl pos");
    l->__pos.Print("l pos");
    l->__size.Print("layer size");
    E2D_PRINT("\nlscale:%f, lblscale:%f", l->__scale, lbl_->__scale);
    // */
    modelMatrix.SetTransform(-l->__size.width * 0.5f + l->__pos.x + lbl_->__pos.x * l->__scale,
                             -l->__size.height * 0.5f + l->__pos.y + lbl_->__pos.y * l->__scale,
                             0.0f,
                             lbl_->__scale * l->__scale,
                             lbl_->__scale * l->__scale,
                             1.0f,
                             E2D_DEGREES_TO_RADIANS(lbl_->__angle));
    
    glUniformMatrix4fv(modelView, 1, GL_FALSE, modelMatrix.Pointer());
    
    
    //lbl_->__v->color.Print();
    
    //glVertexAttribPointer(__attribs.pos, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) *2, v);
    glVertexAttribPointer(attribs.pos, 2, GL_FLOAT, GL_FALSE, sizeof(E2Dvertex), &lbl_->v->position.x);
    glVertexAttribPointer(attribs.col, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(E2Dvertex), &lbl_->v->color.r);
    glVertexAttribPointer(attribs.texc, 2, GL_FLOAT, GL_FALSE, sizeof(E2Dvertex), &lbl_->v->texCoord.s);
    
    //glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    size_t i = 0, j = 0;
    while (i < lbl_->f_count)
    {
       // E2D_PRINT("\n[%zu]:%d", j, &lbl_->v_elem_indx[j]);
        glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, &lbl_->v_elem_indx[j]);
        j += 4;
        i++;
    }
   // E2D_BL;
    
}

void E2DRendererES2::Render(const E2DEmitter *e_)
{
    //e_->__particles->ObjectAtIndex(0);
    //E2D_PRINT("\nrend");
    //glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    this->SetBlendFunc(E2D_BLEND_FN_LUMINOUS);
    this->BindTexture(e_->_textureID);
    this->useProgram(program_particles);
    
    GLfloat mat[] = {
        1.0f, 0.0f, 0.0f, 0.f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    };
    
    glUniformMatrix4fv(modelView, 1, GL_FALSE, mat);
    
    //glEnableVertexAttribArray(attribs.texc);
    //glDisableVertexAttribArray(attribs.texc);
    GLfloat tc[] = {
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f
    };
    
    GLsizei stride = sizeof(E2DpRenderAttribs);
    //printf("\npSize:%f", e_->p[0].size = 10.0f);
    //e_->p[0].size = 320.0f;
    printf("\npSize:%f", e_->p[0].size);
    glVertexAttribPointer(attribs.pos, 2, GL_FLOAT, GL_FALSE, stride, &e_->p[0].pos.x);
    glVertexAttribPointer(attribs.col, 4, GL_UNSIGNED_BYTE, GL_TRUE, stride, &e_->p[0].color.r);
    glVertexAttribPointer(attribs.psize, 1, GL_FLOAT, GL_FALSE, stride, &e_->p[0].size);
    glVertexAttribPointer(attribs.texc, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 2, tc);
    
    
    
    E2D_PRINT("\npCount:%d", e_->pCount);
    glDrawArrays(GL_POINTS, 0, e_->pCount);
}