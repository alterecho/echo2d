//
//  E2Dnumber.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 01/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_NUMBER_H
#define E2D_NUMBER_H

#include <iostream>
#include "E2D_Obj_.hpp"

template <typename T>
class E2DNumber : public E2D_Obj_ {
    T           _value;
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_NUMBER; }
    E2DNumber(T value_);
   virtual ~E2DNumber();
    
    void Print(void) const
    {
        E2D_SYS_PRINT("");
        std::cout << _value;
    }
    
    void Print(const char *tag_) const
    {
        E2D_SYS_PRINT("%s", tag_);
        std::cout << _value;
    }
    
    void print(const char *) const { };
    
    T Value() const;
};

template <typename T>
E2DNumber<T>::E2DNumber(T value_) : _value(value_)
{
    this->setClassName("E2DNumber");
    this->setClassType(E2D_CLASS_NUMBER);
}

template <typename T>
E2DNumber<T>::~E2DNumber()
{
    _value = 0;
}

template <typename T>
T E2DNumber<T>::Value() const
{
    return _value;
}



#endif /* defined(__Echo2D_2__E2Dnumber__) */
