//
//  TestReplace2.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 15/06/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "TestReplace2.h"
#include "E2DLayer.h"
#include "E2DSprite.h"
#include "TestReplace1.h"
#include "E2Dcontroller.h"

TestReplace2::TestReplace2()
{
    this->setClassName("TestReplace2");
    E2D_SYS_PRINT("constructing class...");
    
    E2DLayer *l = new E2DLayer(-1);
    this->AddLayer(l);
    E2DSprite *spr1 = new E2DSprite("960.png", 1);
    l->AddDrawable(spr1);
    
    spr1->SetPosition(__screenCenter);
    
}

TestReplace2::~TestReplace2()
{
    E2D_SYS_PRINT("destroying class...");
    
}

void TestReplace2::update(E2Delapsed dt_)
{
    static E2Delapsed t = 0;
    t += dt_;
    E2D_SYS_PRINT("updating:%f", t);
    
    if (t > 2.0)
    {
        TestReplace1 *tr1 = new TestReplace1();
        E2DController::SharedController()->ReplaceScene(tr1);
        tr1->Release();
    }
    
}