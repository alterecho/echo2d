//
//  TestReplace1.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 15/06/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "TestReplace1.h"
#include "TestReplace2.h"
#include "E2DController.h"
#include "E2DLayer.h"
#include "E2DSprite.h"
#include "E2DMusicPlayer.h"
#include "E2DFXPlayer.h"

TestReplace1::TestReplace1()
{
    this->setClassName("TestScene1");
    E2D_SYS_PRINT("constructing class...");
    
    //*
    E2DLayer *l = new E2DLayer(10);
    this->AddLayer(l);
    E2DSprite *spr1 = new E2DSprite("480.png", 10);
    l->AddDrawable(spr1);
    spr1->SetPosition(__screenCenter);
    //*/
    
    E2DController::SharedController()->MusicPlayer()->PreloadMusic("music1.mp3");
    E2DController::SharedController()->MusicPlayer()->Play("music1.mp3", false);
    E2DController::SharedController()->FXPlayer()->LoadSound("l1.wav");
    E2DController::SharedController()->FXPlayer()->PlaySound("l1.wav");
    
}

TestReplace1::~TestReplace1()
{
    E2D_SYS_PRINT("destroying scene...");
    
}

void TestReplace1::update(E2Delapsed dt_)
{
    static E2Dseconds s = 0.0;
    s += dt_;
    E2D_SYS_PRINT("updating:%f, dt:%f", s, dt_);
    if (s > 2.0)
    {
        /*
        TestReplace2 *tr2 = new TestReplace2();
        E2DController::SharedController()->ReplaceScene(tr2);
        tr2->Release();
        //*/
    }
}