//
//  E2DLabel.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 06/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_LABEL_H
#define E2D_LABEL_H

#include <iostream>
#include "E2DDrawable.h"

#define _E2D_LABEL_DEF_FNT_COUNT      100

class E2DFont;
class E2DLabel : public E2DDrawable {
    friend class E2DRendererES2;
    
    char            *string;
    E2Dvertex       *v;
    size_t          v_count, f_count, v_indx, labelLength; /* vertex count, font count, label vertex index */
    unsigned char   *v_elem_indx;     /* vertex indices array (for rendering element array) */
    
    class E2DFontManager        *fontManager;
    
    void addFontVertices(const E2DFont *);
    void resetFontpositions();
    
protected:
    virtual void render();
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_LABEL; }
    E2DLabel(const char *string_, E2Ddepth depth_);
    virtual ~E2DLabel();
    
    
    void Print() const;
    virtual void Print(const char *) const;
    
    virtual void setSize(float width_, float height_) { __size(width_, height_); };
    
    void SetString(const char *);
    const char* String(void);
    
    const E2Dvertex* Vertices(void) const;
    void SetColor(E2Dcolor color_);
};


#endif /* defined(__Echo2D_2__E2DLabel__) */
