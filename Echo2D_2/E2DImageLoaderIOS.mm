//
//  E2DImageLoaderIOS.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 25/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DImageLoaderIOS.h"
#include "E2DFileManagerIOS.h"

E2Dsize E2DImageLoaderIOS::LoadImage(const char *imageName_, GLubyte **out_, E2Dsize& aSize_)
{
    E2Dsize ret;
    char *path = NULL;
    fileManager->PathForFile_alc(imageName_, &path);
    NSString *imagePath = [NSString stringWithCString:path encoding:NSUTF8StringEncoding];
    E2D_FREE(path);
    //NSLog(@"\nimagePath:%@", imagePath);
    NSData *imageFileData = [[NSData  alloc] initWithContentsOfFile:imagePath]; //[UIImage imageWithContentsOfFile:[NSString stringWithCString:fileManager->PathForFile("bullet.png") encoding:NSUTF8StringEncoding]];
    UIImage *image = nil;
    image = [[UIImage alloc] initWithData:imageFileData];
    
    E2D_ASSERT(image, "unable to load image '%s'", imageName_)
    
    ret.width = CGImageGetWidth(image.CGImage);
    ret.height = CGImageGetHeight(image.CGImage);
    
    //E2Dsize pow2;
    aSize_.width = e2d::nearestPow2(ret.width);
    aSize_.height = e2d::nearestPow2(ret.height);
    
    *out_ = (GLubyte *)malloc(aSize_.width * aSize_.height * 4);
    //ret(ret.width, ret.height);
    
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(*out_, aSize_.width, aSize_.height, 8, 4 * aSize_.width, colorSpaceRef, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpaceRef);
    CGContextClearRect(context, CGRectMake( 0, 0, aSize_.width, aSize_.height ));
    CGContextTranslateCTM(context, 0, aSize_.height);
    CGContextScaleCTM(context, 1.0f, -1.0f);
    CGContextDrawImage( context, CGRectMake( 0, 0, aSize_.width, aSize_.height ), image.CGImage );
    
    CGContextRelease(context);
    [image release];
    [imageFileData release];
    
    return ret;
}