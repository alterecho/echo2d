//
//  E2DGLView.m
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#import "E2DGLView.h"
//#include "E2DKeyboardIOS.h"
#include "E2DKeyboard.h"
#include "E2DController.h"

@implementation E2DGLView

static E2DGLView *instanceOfGLView = NULL;
static BOOL toggle = NO;

+ (E2DGLView *)sharedGLView
{
    if (!instanceOfGLView)
    {
        printf("\nGLView not instantiated");
        exit(1);
    }
    
    return instanceOfGLView;
}

- (id)initWithFrame:(CGRect)frame_
{
    if ((self = [super initWithFrame:frame_]))
    {
        if (instanceOfGLView)
        {
            printf("GLView already instantiated");
            exit(1);
        }
        instanceOfGLView = self;
        
        //[rem]
        CAEAGLLayer *glLayer = (CAEAGLLayer *)self.layer;
        glLayer.opaque = YES;
        //[self becomeFirstResponder];
    }
    return self;
}

- (void)dealloc
{
    instanceOfGLView = NULL;
    [super dealloc];
}

+ (Class)layerClass
{
   return  [CAEAGLLayer class];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    printf("\ntouchesBegan");
    //E2DController *controller = E2DController::SharedController();
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:[touch view]];
    controller->touchBegan(E2Dpoint(p.x, screenSize.height - p.y));
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    printf("\ntouchesMoved");
    //E2DController *controller = E2DController::SharedController();
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:[touch view]];
    controller->touchMoved(E2Dpoint(p.x, screenSize.height - p.y));
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    printf("\ntouchesEnded");
    //E2DController *controller = E2DController::SharedController();
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:[touch view]];
    controller->touchEnded(E2Dpoint(p.x, screenSize.height - p.y));
    
    /*
    if (toggle)
        E2DKeyboard::SharedKeyboard()->Activate();
    else
        E2DKeyboard::SharedKeyboard()->Dismiss();
     //*/
    
    toggle = !toggle;
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    printf("\ntouchesCancelled");
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)insertText:(NSString *)text_
{
    printf("\ninsert text");
    E2DKeyboard::SharedKeyboard()->InsertText([text_ cStringUsingEncoding:NSUTF8StringEncoding]);
}

- (void)deleteBackward
{
    printf("\ndelete text");
    E2DKeyboard::SharedKeyboard()->DeleteText();
}

- (BOOL)hasText
{
    printf("\nhas text");
    return E2DKeyboard::SharedKeyboard()->HasText();
}
@end
