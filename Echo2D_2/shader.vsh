attribute vec4 position;

attribute vec4 color;
varying vec4 f_color;

attribute vec2 texCoord;
varying vec2 f_texCoord;

uniform mat4 projection;
uniform mat4 modelView;

void main(void) {
    gl_Position = projection * modelView * position;
    f_color = color;
    f_texCoord = texCoord;
}