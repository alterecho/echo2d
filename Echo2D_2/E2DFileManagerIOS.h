//
//  E2DFileManagerIOS.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_FILE_MANAGER_IOS_H
#define E2D_FILE_MANAGER_IOS_H

#include <iostream>
#include "E2DFileManager.h"

class E2DFileManagerIOS : public E2DFileManager {
    
public:
    //E2DFileManagerIOS();
    void PathForFile_alc(const char *file_, char **pathBuf_) const;
    void StringFromContentsOfFile(const char *file_, char **buf_) const;
    
};

#endif /* defined(__Echo2D_2__E2DFileManagerIOS__) */
