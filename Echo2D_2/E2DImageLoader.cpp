//
//  E2DImageLoader.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 25/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DImageLoader.h"
#include "E2DFileManager.h"

static E2DImageLoader *instanceOfImageLoader(nullptr);

E2DImageLoader* E2DImageLoader::SharedImageLoader()
{
    E2D_ASSERT(instanceOfImageLoader, "ImageLoader not initialized");
    return instanceOfImageLoader;
}

E2DImageLoader::E2DImageLoader()
{
    this->setClassName("E2DImageLoader");
    this->setClassType(E2D_CLASS_IMAGE_LOADER);
    instanceOfImageLoader = this;
    
    fileManager = E2DFileManager::SharedFileManager();
}

E2DImageLoader::~E2DImageLoader()
{
    instanceOfImageLoader = NULL;
}