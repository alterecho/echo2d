//
//  E2DConfig.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 10/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_CONFIG_H
#define E2D_CONFIG_H

#define _E2D_DEBUG                  1
#define _E2D_LOG                    1
#define _E2D_FRAME_RATE             60.0f
#define _E2D_KEYBOARD_FONT          "ocr_20.e2dfnt"
#define _E2D_KEYBOARD_BG            "480.png"

#define _E2D_FX_N_SOURCE            32

//#ifdef __cplusplus
#include <iostream>
//#endif

constexpr static inline float E2D_FRAME_TIME(void)
{
    return (1.0f / _E2D_FRAME_RATE);
}

#endif
