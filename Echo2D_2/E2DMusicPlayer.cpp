//
//  E2DMusicPlayer.cpp
//  Echo2D_2_Audio
//
//  Created by Vijay Chandran J on 13/06/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#include "E2DMusicPlayer.h"

E2DMusicPlayer::E2DMusicPlayer()
{
    this->setClassName("E2DMusicPlayer");
    this->setClassType(E2D_CLASS_MUSIC_PLAYER);
}

E2DMusicPlayer::~E2DMusicPlayer()
{
    
}