//
//  E2DDictionry.h
//  E2DDerivedStructs
//
//  Created by Vijay Chandran J on 04/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_DICTIONARY_H
#define E2D_DICTIONARY_H

#include <iostream>
#include "E2D_Obj_.hpp"
//#include <map>
//#include "E2DString.hpp"
#include "E2DEnumerable.h"

//***retain
class E2DString;
template <typename TKey, typename TVal>
struct E2DKeyValPair : public E2D_Obj_ {
    TKey       key;
    TVal       value;
};

template <typename TKey, typename TVal>
class E2DKeyedArray : public E2D_Obj_ {
    E2DKeyValPair<TKey, TVal>   p;
    
public:
    E2DKeyedArray ();
};

#define _E2D_DEF_DC_SZ      100

class E2DDictionary : public E2DEnumerable {
    
    /* group the key (string/char) with an object (E2D_Obj_) */
    struct _KVPair {
        E2D_Obj_    *object = nullptr;
        char        *key = nullptr;
    } *__pair;
    
    void initializeKVPair(_KVPair *pair_, E2Dull_t a_, E2Dull_t sz_);
    void resize(char op_);
    bool stringEqualsString(const char *s1_, const char *s2_);
    
    
public:
    
    E2DDictionary(void);
    E2DDictionary(unsigned long);
    virtual ~E2DDictionary();
    
    void print(const char *) const;
    
    void SetObject(E2D_Obj_ *, const char *);
    void SetObject(E2D_Obj_ *, long);
    E2D_Obj_* ObjectForKey(const char *);
    E2D_Obj_* ObjectForKey(long);
    const char* KeyForObject(E2D_Obj_ *obj_);
    
    void RemoveObjectForKey(const char *);
    void RemoveObject(E2D_Obj_ *);
    
    void Consolidate(void);
    virtual E2D_Obj_* NextObject(void);
    virtual bool NextObject(E2D_Obj_ **stor_);
    
};







#pragma mark - private -
/* set all the keys and objects to null */
inline
void E2DDictionary::initializeKVPair(E2DDictionary::_KVPair *pair_, E2Dull_t a_,  E2Dull_t sz_)
{
    for (E2Dull_t i = a_; i < sz_; i++)
    {
        pair_[i].key = nullptr;
        pair_[i].object = nullptr;
    }
}

inline
void E2DDictionary::resize(char op_)
{
    _size *= 2;
    _KVPair *temp = (_KVPair *)realloc(__pair, (size_t)_size * sizeof(_KVPair));
    if (!temp)
    {
        E2D_EXIT("error resizing array");
        E2D_FREE(__pair);
        exit(1);
    }
    this->initializeKVPair(temp, _size / 2, _size);
    __pair = temp;
}


inline
bool E2DDictionary::stringEqualsString(const char *s1_, const char *s2_)
{
    if (!s1_ || !s2_) return false;
    
    size_t
    l1 = strlen(s1_),
    l2 = strlen(s2_);
    
    if (l1 != l2) return false;
    
    l1++;
    for (size_t i = 0; i < l1; i++)
    {
        if (s1_[i] != s2_[i])
            return false;
    }
    
    return true;
}

#endif /* defined(__E2DDerivedStructs__E2DDictionry__) */
