//
//  TestReplace1.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 15/06/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef __Echo2D_2__TestReplace1__
#define __Echo2D_2__TestReplace1__

#include <iostream>
#include "E2DScene.h"
#include "E2DLayer.h"
#include "E2DSprite.h"

class TestReplace1 : public E2DScene {
    
public:
    TestReplace1();
    ~TestReplace1();
    
    void update(E2Delapsed dt_);
};

#endif /* defined(__Echo2D_2__TestReplace1__) */
