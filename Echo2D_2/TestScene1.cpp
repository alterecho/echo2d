//
//  TestScene1.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 02/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "TestScene1.h"
#include "E2DTextureManager.h"


TestScene1::TestScene1()
{
    spr1 = new E2DSprite("box2_20.png", 19);
    //spr1->SetPosition(_screenSize.width * 0.5f, _screenSize.height * 0.5f);
    spr1->SetVisible(true);
    spr1->SetScale(1.0f);
    spr1->SetPosition(__screenCenter);
    
    spr2 = new E2DSprite("boxpow2.png", 20);
    
    spr2->SetScale(1.0f);
    //spr2->SetPosition(_screenCenter.x, _screenCenter.y);
    //spr2->SetPosition(_screenCenter);
    //spr2->SetPosition(_screenCenter);
    
    
    //*
    spr3 = new E2DSprite("img32c1.png", 10);
    spr3->SetPosition(__screenCenter.x + 30.0f, __screenCenter.y + 20.0f);
    
    spr4 = new E2DSprite("bullet.png", 11);
    spr4->SetPosition(__screenCenter.x - 20.0f, __screenCenter.y + 40.0f);
    //spr4->SetScale(5.0f);
    //*/
    
    layer1 = new E2DLayer(10);
    layer1->SetVisible(true);
    //layer1->SetPosition(10.0f, 10.0f);
    this->AddLayer(layer1);
    //layer1->AddDrawable(spr1);
    //layer1->AddDrawable(spr2);
    
    
    //*
    layer2 = new E2DLayer(11);
    //layer2->AddDrawable(spr3);
    //layer2->AddDrawable(spr4);
    this->AddLayer(layer2);
    // */
    
    layer1->SetScale(0.5f);
    spr1->SetAngle(45.0f);
    
    E2DTextureManager::SharedTextureManager()->CacheTextureAtlas("IH_textureatlas-ipad.e2dtc");
    E2DSprite *spr = new E2DSprite("title.png", 10.0f);
    spr->SetPosition(__screenCenter);
    layer1->AddDrawable(spr);
}

TestScene1::~TestScene1()
{
    
}

void TestScene1::Update(E2Delapsed dt_)
{
   // return;
    static char sign = 1;
    //spr1->Position();
    //spr1->Move(1.0f, 1.0f);
    //layer1->Move(0.5f, 0.5f);
    //layer2->Move(0.5f, 0.5f);
    //*
    GLclampf scale = layer2->Scale();
    if (scale > 1.0f || scale < 0.0f)
    {
        sign *= -1;
    }
    layer2->SetScale(layer2->Scale() + 0.05f * sign);
    // */
}

void test2()
{
    
}