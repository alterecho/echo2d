
//
//  E2DParticleAttribs.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 14/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_PARTICLE_MOD_H
#define E2D_PARTICLE_MOD_H

#include "E2DparticleLimits.hpp"
#include "E2DMacros.h"
#include "E2DFunctions.h"

struct E2DparticleMod {
   
    E2Dfloat    scale, scale_end;
    E2Dcolor    color, color_end;
    //E2Dfloat    alpha_s, alpha_d; // start, difference
    E2Dvec2   vel, velDamping, velSpread; /* velocity, damping */
    E2Dvec2   acc;                        /* acceleration */
    E2Dvec2   gravity;
    
    E2Dradians    angle;
    E2Dradians    aVel;       /* angular velocity */
    E2Dfloat      aAcc;       /* angular acceleration */
    
    E2Dfloat      radDist;
    E2Dradians    radAngle;
    E2Dseconds    lifeTime;
    //E2DpLimits    *lim;
    
    
    
    static inline E2Dclass_t Class(void) { return E2D_CLASS_P_MOD; }
    
    void Initialize(void)
    {
        vel = E2Dvec2(0, 0);
        color = E2Dcolor(0, 0, 0, 0);
        velDamping(1.0f, 1.0f);
    }
    
    E2DparticleMod(void) :
    scale(0.0f), scale_end(0.0f) { };
    //lim(nullptr) { };
    
    //*
    E2DparticleMod(const E2DparticleMod& mod_)
    {
        color = mod_.color;
        scale = mod_.scale;
        vel = mod_.vel;
        acc = mod_.acc;
        aVel = mod_.aVel;
        aAcc = mod_.aAcc;
    }
     //*/
    
    inline E2DparticleMod random(void)
    {
        E2DparticleMod ret;
        
        ret.scale = e2d::random_0_1() * scale;
        ret.color(e2d::random_255() * color.r, e2d::random_255() * color.g, e2d::random_255() * color.b, e2d::random_255() * color.a);
        ret.vel(e2d::random_0_1() * vel.x, e2d::random_0_1() * vel.y);
        ret.acc(e2d::random_0_1() * acc.x, e2d::random_0_1() * acc.y);
        ret.aVel = e2d::random_0_1() * aVel;
        ret.aAcc = e2d::random_0_1() * aAcc;
        
        return ret;
    }
    /*
    inline void randomize(void)
    {
        this->randomize(*lim);
    }
    // */
    
    inline void randomize(const E2DpLimits& lim_)
    {
        lifeTime = lim_.min.lifeTime + (lim_.max.lifeTime - lim_.min.lifeTime) * e2d::random_0_1();
        
        scale_end = lim_.min.scale_end + (lim_.max.scale_end - lim_.min.scale_end) * e2d::random_0_1();
        //scale_end -= 4;
        
        color(0, 0, 0, 0);//lim_.min.color.a + (lim_.max.color.a - lim_.min.color.a) * e2d::random_0_1());
        //lim_.min.color_end.Print("min");
        //lim_.max.color_end.Print("max");
        color_end = lim_.min.color_end + (lim_.max.color_end - lim_.min.color_end) * e2d::random_255();
        color.a = 0;
        //alpha_s = lim_.min.color.a;
        //alpha_d = (E2D_FRAME_TIME() / lifeTime) * (lim_.max.color.a - alpha_s);
        
        
        vel(
            lim_.min.vel.x + (lim_.max.vel.x - lim_.min.vel.x) * e2d::random_0_1()
            , lim_.min.vel.y + (lim_.max.vel.y - lim_.min.vel.y) * e2d::random_0_1()
            );
        
        velDamping(
                   lim_.min.vel_damping.x + (lim_.max.vel_damping.x - lim_.min.vel_damping.x) * e2d::random_0_1(),
                   lim_.min.vel_damping.y + (lim_.max.vel_damping.y - lim_.min.vel_damping.y) * e2d::random_0_1()
                   );
        
        velSpread(
                  (lim_.min.vel_spread.x + (lim_.max.vel_spread.x - lim_.min.vel_spread.x) * e2d::random_0_1()) * e2d::random_MINUS_1_1_ABS(),
                  (lim_.min.vel_spread.y + (lim_.max.vel_spread.y - lim_.min.vel_spread.y) * e2d::random_0_1()) * e2d::random_MINUS_1_1_ABS()
                  );
        
        //if (radMotion_)
        {
            radAngle = lim_.min.radAngle_d + (lim_.max.radAngle_d - lim_.min.radAngle_d) * e2d::random_0_1();
            radDist = lim_.min.radDist_d + (lim_.max.radDist_d - lim_.min.radDist_d) * e2d::random_0_1();
        }
    }
    
    void calculatescaleMod(E2Dfloat sz_)
    {
        //E2D_PRINT("\ncalc sz mod: sz_:%f, s_end:%f", sz_, scale_end);
        scale = (E2D_FRAME_TIME() / lifeTime) * (scale_end - sz_);
        //E2D_PRINT("\n-> %f", scale);
    }
    
    void calculateColorMod(E2Dcolor c_)
    {
        //c_.Print("c_");
        //color_end.Print("color end");
        color = (E2D_FRAME_TIME() / lifeTime) * (color_end - c_);
        color.a = 0; //alphaMod is called after this method
        //color.Print("color");
    }
    
    void calculateAlphaMod(E2Dfloat o_)
    {
        //color.a = alpha_d;
        color.a = (E2D_FRAME_TIME() / lifeTime) * (color_end.a - o_);
    }
    
    void addGravity(E2Dvec2 g_)
    {
        vel += g_;
    }
    
    
};

#endif
