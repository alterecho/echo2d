//
//  E2DMemoryManager.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 29/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DMemoryManager.h"

static E2DMemoryManager *instanceOfMemoryManager(nullptr);

E2DMemoryManager* E2DMemoryManager::SharedMemoryManager()
{
    E2D_ASSERT(instanceOfMemoryManager, "MemoryManager not initialized");
    return instanceOfMemoryManager;
}

E2DMemoryManager::E2DMemoryManager()
{
    instanceOfMemoryManager = this;
}

E2DMemoryManager::~E2DMemoryManager()
{
    instanceOfMemoryManager = nullptr;
}