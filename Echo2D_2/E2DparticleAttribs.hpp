//
//  E2DparticleAttribs.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 15/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_PARTICLE_ATTRIBS_H
#define E2D_PARTICLE_ATTRIBS_H

#include <iostream>
#include "E2DparticleLimits.hpp"

#include "E2DDataTypes.h"
#include "E2DparticleMod.hpp"

struct E2DpRenderAttribs {
    E2Dpoint pos;
    E2Dfloat size;
    E2Dcolor color;
    E2Dradians angle;
};

struct E2DparticleAttribs {
    
    E2DpRenderAttribs   *pra;
    E2Dpoint            pos;
    E2Dfloat            radDist;
    E2Dradians          radAngle;
    
    E2Dpoint            r_pos;
    E2Dfloat            r_size;
    E2Dradians          r_angle;
    E2Dcolor            r_color;
    
    E2DparticleAttribs(void) :
   
    pra(nullptr),
    pos(E2Dpoint(0.0f, 0.0f)),
    radDist(0.0f),
    radAngle(0.0f),
    
    r_pos(E2Dpoint()),
    r_size(0.0f),
    r_angle(0.0f),
    r_color(E2Dcolor())
    { };
    
    inline void print()
    {
        if (pra)
        {
            std::cout << "size:" << pra->size << "\ncolor:" << pra->color << "\npos:" << pos << "\nangle:" << pra->angle << "\n" << "lifetime:" << std::endl;
        }
        else
        {
            E2D_PRINT("\nno render attributes set");
        }
    }
    
    inline void add(E2DparticleMod& m_, bool scale_, bool fade_, bool morph_, bool radMotion_)
    {
        //if (!pra) return;
        if (scale_)
        {
            pra->size += m_.scale;
            if (pra->size < 0.0f) pra->size = 0.0f; //[e2d]rem
        }
        //E2D_PRINT("\nsz:%f", m_.size);
        if (morph_)
        {
            pra->color.r += m_.color.r;
            pra->color.g += m_.color.g;
            pra->color.b += m_.color.b;
        }
        if (fade_)
        {
            pra->color.a += m_.color.a;
        }
        //m_.color.Print("morph");
        pra->angle += m_.aVel;
        
        pos.x += (m_.vel.x + m_.velSpread.y) * E2D_FRAME_TIME();
        pos.y += (m_.vel.y + m_.velSpread.x) * E2D_FRAME_TIME();
        //m_.vel *= m_.velDamping;  //object update
        m_.vel.x *= (1.0f - m_.velDamping.x);
        m_.vel.y *= (1.0f - m_.velDamping.y);
        
        if (radMotion_)
        {
            pra->pos.x = pos.x + radDist * cosf(radAngle);
            pra->pos.y = pos.y + radDist * sinf(radAngle);
            radDist += m_.radDist;
            radAngle += m_.radAngle;
        }
        else
        {
            pra->pos = pos;
        }
        
    }
    
    inline void randomize(const E2DpLimits& lim_)
    {
        
        r_size = lim_.min.scale_start + (lim_.max.scale_start - lim_.min.scale_start) * e2d::random_0_1();
        r_angle = lim_.min.angle + (lim_.max.angle - lim_.min.angle) * e2d::random_0_1();
        
        r_color(
                   lim_.min.color_start.r + (lim_.max.color_start.r - lim_.min.color_start.r) * arc4random() % 255,
                   lim_.min.color_start.g + (lim_.max.color_start.r - lim_.min.color_start.g) * arc4random() % 255,
                   lim_.min.color_start.b + (lim_.max.color_start.r - lim_.min.color_start.b) * arc4random() % 255,
                   lim_.min.color_start.a + (lim_.max.color_start.r - lim_.min.color_start.a) * arc4random() % 255
                   );
        
        radDist = lim_.min.radDist + (lim_.max.radDist - lim_.min.radDist) * e2d::random_0_1();
        radAngle = lim_.min.radAngle + (lim_.max.radAngle - lim_.min.radAngle) * e2d::random_0_1();
        pos.x = radDist * cosf(radAngle);
        pos.y = radDist * sinf(radAngle);
        r_pos = pos;
        
        /* pra not set directly in case one does not exist (in case of pre-generated attribute) */
        if (pra)
        {
            pra->pos = pos;
            pra->size = r_size;
            pra->angle = r_angle;
            pra->color = r_color;
        }
        
    }
    
    inline void reset(void)
    {
        /* no need to set pra->pos. dependent on pos */
        pra->size = r_size;
        pra->angle = r_angle;
        pra->color = r_color;
    }
    
    inline void setPrAttribs(E2DpRenderAttribs* pra_)
    {
        pra = pra_;
        this->reset();
    }
    
    
};

#endif /* defined(__Echo2D_2__E2DparticleAttribs__) */
