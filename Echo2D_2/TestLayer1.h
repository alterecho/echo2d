//
//  TestLayer1.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 13/06/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef __Echo2D_2__TestLayer1__
#define __Echo2D_2__TestLayer1__

#include <iostream>
#include "E2DLayer.h"

class TestLayer1 : public E2DLayer {
    
public:
    TestLayer1(E2Ddepth);
    bool touchBegan(E2Dpoint);
    void touchMoved(E2Dpoint);
    void touchEnded(E2Dpoint);
};

#endif /* defined(__Echo2D_2__TestLayer1__) */
