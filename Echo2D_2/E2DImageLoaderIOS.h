//
//  E2DImageLoaderIOS.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 25/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_IMAGE_LOADER_IOS_H
#define E2D_IMAGE_LOADER_IOS_H

#include <iostream>

#include "E2DImageLoader.h"

class E2DImageLoaderIOS : public E2DImageLoader {
public:
    E2Dsize LoadImage(const char *imageName_, GLubyte **out_, E2Dsize& aSize_);
};

#endif /* defined(__Echo2D_2__E2DImageLoaderIOS__) */
