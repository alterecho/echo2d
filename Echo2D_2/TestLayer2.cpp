//
//  TestLayer2.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 13/06/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "TestLayer2.h"


TestLayer2::TestLayer2(E2Ddepth d_) : E2DLayer(d_)
{
    this->setClassName("TestLayer2");
}

bool TestLayer2::touchBegan(E2Dpoint p_)
{
    E2D_SYS_PRINT("t began");
    return true;
}

void TestLayer2::touchMoved(E2Dpoint p_)
{
    E2D_SYS_PRINT("t moved");
}

void TestLayer2::touchEnded(E2Dpoint p_)
{
    E2D_SYS_PRINT("t ended");
}