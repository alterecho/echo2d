//
//  TestSprite.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef __Echo2D_2__TestSprite__
#define __Echo2D_2__TestSprite__

#include <iostream>
#include "E2DScene.h"
#include "E2DSprite.h"
#include "E2DLayer.h"
#include "TestLayer1.h"
#include "TestLayer2.h"

class TestSprite : public E2DScene {
    E2DLayer    *l1;
    TestLayer1  *tl1;
    TestLayer2  *tl2;
    E2DSprite   *spr1;
    
public:
    TestSprite();
};


#endif /* defined(__Echo2D_2__TestSprite__) */
