//
//  E2DDataTypes.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 10/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_DATA_TYPES_H
#define E2D_DATA_TYPES_H

#ifdef __cplusplus
#include <iostream>
#endif
#import <OpenGLES/ES2/gl.h>
//#include <OpenGL/gl.h>
#include "E2Dclass_t.h"

typedef GLenum          E2Denum;
typedef GLboolean       E2Dbool;
typedef GLbitfield      E2Dbitfield;
typedef GLbyte          E2Dbyte;
typedef GLshort         E2Dshort;
typedef GLint           E2Dint;
typedef GLsizei         E2Dsizei;
typedef GLubyte         E2Dubyte;
typedef GLushort        E2Dushort;
typedef GLuint          E2Duint;
typedef GLfloat         E2Dfloat;
typedef GLclampf        E2Dclampf;
typedef GLvoid          E2Dvoid;
typedef GLfixed         E2Dfixed;

typedef GLintptr        E2Dintptr;
typedef GLsizeiptr      E2Dsizeiptr;
typedef long            E2Dlong;

typedef double          E2Dseconds;
typedef E2Dseconds      E2Delapsed;

typedef E2Dfloat        E2Ddegrees;
typedef E2Dfloat        E2Dradians;

typedef unsigned long       E2Dul_t;
typedef unsigned long long  E2Dull_t;

typedef int             E2Ddepth;

#endif
