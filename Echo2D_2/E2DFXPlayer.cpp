//
//  E2DFXPlayer.cpp
//  Echo2D_2_Audio
//
//  Created by Vijay Chandran J on 31/05/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#include "E2DFXPlayer.h"
#include "E2DAudioLoader.h"


#pragma mark -
#pragma mark E2DFX
#pragma mark -
E2DFX::E2DFX(const char *fileName_) : _id(0), name(nullptr)
{
    _id = E2DAudioLoader::SharedAudioLoader()->LoadAudio(fileName_);
    
    name = (char *)malloc(sizeof(char) * strlen(fileName_) + 1);
    strcpy(name, fileName_);
}

E2DFX::~E2DFX()
{
    E2D_PRINT("\ndeleting FX '%s'", name);
    alDeleteBuffers(1, &_id);
    E2D_FREE(name);
}

#pragma mark -
#pragma mark E2DFXSource
#pragma mark -
E2DFXSource::E2DFXSource(E2DFXPlayer *player_)
{
    player = player_;
    alGenSources(1, &_id);
    
    fade = false;
    fadeTarget = 0.0f;
    fadeRate = 0.0f;
}

E2DFXSource::~E2DFXSource()
{
    alDeleteSources(1, &_id);
    player = nullptr;
}

inline
bool E2DFXSource::isPlaying() const
{
    ALint state;
    alGetSourcei(_id, AL_SOURCE_STATE, &state);
    if(state == AL_PLAYING)
    {
        return true;
    }
    
    return false;
}

inline
void E2DFXSource::fadeTo(float v_, E2Dseconds t_)
{
    fadeTarget = v_;
    fadeTime = t_;
    ALfloat v;
    alGetSourcef(_id, AL_GAIN, &v);
    fadeRate = E2D_FRAME_TIME() * (v_ - v) / t_;
    printf("\nframe time:%f, fade rate:%f", E2D_FRAME_TIME(), fadeRate);
    
    fade = true;
    player->incrementFadeCount();
}

void E2DFXSource::update(E2Delapsed dt_)
{
    
    static E2Dseconds s = 0.0;
    s += dt_;
//    printf("\ntme:%f", s);
    ALfloat vol;
    alGetSourcef(_id, AL_GAIN, &vol);
    
    ALint state;
    alGetSourcei(_id, AL_SOURCE_STATE, &state);
    
    vol += fadeRate;
    alSourcef(_id, AL_GAIN, vol);
    
    if (vol <= 0.0f || state != AL_PLAYING)
    {
        player->decrementFadeCount();
        fade = false;
        fadeRate = 0.0f;
        fadeTarget = 0.0f;
        alSourceStop(_id);
    }
    
    
}

#pragma mark -
#pragma mark E2DFXPlayer
#pragma mark -
E2DFXPlayer::E2DFXPlayer()
{
    this->setClassName("E2DFXPlayer");
    this->setClassType(E2D_CLASS_FX_PLAYER);
    
    E2D_ASSERT((device = alcOpenDevice(NULL)), "unable to open audio device");
    E2D_ASSERT((context = alcCreateContext(device, NULL)), "unable to create OpenAL context");
    E2D_ASSERT((alcMakeContextCurrent(context)), "audio:unable to set OpenAL context");
    
    sounds = new E2DDictionary();
    sources = new E2DArray(_E2D_FX_N_SOURCE);
    
    for (char i = 0; i < _E2D_FX_N_SOURCE; i++)
    {
        E2DFXSource *s = new E2DFXSource(this);
        sources->AddObject(s);
        s->Release();
    }
    
    //this->LoadSound("12 Bar Blues Bass.caf");
    //this->LoadFX("l1.wav");
    //this->UnloadSound("l1.wav");
    //this->PlaySound("l1.wav");
    //this->FadeSound(this->PlaySound("l1.wav"), 0.0f, 2.0);
    
}

E2DFXPlayer::~E2DFXPlayer()
{
    E2D_RELEASE(sources);
    E2D_RELEASE(sounds);
    alcDestroyContext(context);
    alcCloseDevice(device);
}

ALuint E2DFXPlayer::LoadSound(const char *fileName_)
{
    E2DFX *fx = this->sound(fileName_);
    if (fx)
    {
        return fx->_id;
    }
    
    fx = new E2DFX(fileName_);
    sounds->SetObject(fx, fileName_);
    fx->Release();
    
    return fx->_id;
}

void E2DFXPlayer::UnloadSound(const char *fileName_)
{
    E2DFX *fx = this->sound(fileName_);
    if (!fx)
        return;
    sounds->RemoveObject(fx);
}

ALuint E2DFXPlayer::PlaySound(const char *name_)
{
    /*
     ALfloat sourcePos[] = { -10.0f, 0.0f, 0.0f };
     ALfloat sourceVel[] = { 0.0f, 0.0f, 0.0f };
     ALfloat listenerPos[] = { 1.0f, 0.0f, 0.0f };
     ALfloat listenerVel[] = { 0.0f, 0.0f, 0.0f };
     ALfloat listenerOrient[] = { 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f };
     //*/
    
    //*
    
    //ALuint s;
    //alGenSources(1, &s);
    E2DFX *fx = this->sound(name_);
    E2D_ASSERT(fx, "sound '%s' not loaded", name_);
    
    ALuint s = this->nextSource();
    alSourcei(s, AL_BUFFER, fx->_id);
    //alSourcef(s, AL_PITCH, 1.0f);
    alSourcef(s, AL_GAIN, 1.0f);
    //alSourcefv(s, AL_POSITION, sourcePos);
    //alSourcefv(s, AL_VELOCITY, sourceVel);
    alSourcei(s, AL_LOOPING, AL_TRUE);
    
    //alListenerfv(AL_POSITION, listenerPos);
    //alListenerfv(AL_VELOCITY, listenerVel);
    //alListenerfv(AL_ORIENTATION, listenerOrient);
    
    alSourcePlay(s);
    //*/
    
    return s;
}

inline
ALuint E2DFXPlayer::nextSource() const
{
    sources->ResetEnumerator();
    while (E2DFXSource *s = (E2DFXSource *)sources->NextObject())
    {
        if (!s->isPlaying())
            return s->_id;
    }
    return 0;
}

inline
ALuint E2DFXPlayer::soundID(const char *name_) const
{
    if (E2DFX *fx = (E2DFX *)sounds->ObjectForKey(name_))
    {
        return fx->_id;
    }
    return 0;
}

inline
E2DFX* E2DFXPlayer::sound(const char *fileName_) const
{
    return (E2DFX *)sounds->ObjectForKey(fileName_);
}

void E2DFXPlayer::update(E2Delapsed dt_)
{
    if (!fadeCount)
        return;
        
    for (E2Dull_t i = 0; i < sources->Count(); i++)
    {
        E2DFXSource *s = (E2DFXSource *)sources->ObjectAtIndex(i);
        if (s && s->fade)
        {
            s->update(dt_);
        }
    }
}

void E2DFXPlayer::FadeSound(ALuint s_, float target_, E2Dseconds time_)
{
    sources->ResetEnumerator();
    while (E2DFXSource *fx = (E2DFXSource *)sources->NextObject())
    {
        if (fx->_id == s_)
        {
            fx->fadeTo(target_, time_);
            return;
        }
    }
    
}

inline
void E2DFXPlayer::incrementFadeCount()
{
    fadeCount++;
    //printf("\ninc fade count:%d", fadeCount);
}

inline
void E2DFXPlayer::decrementFadeCount()
{
    fadeCount--;
    printf("\ndec fade count:%d", fadeCount);
}

