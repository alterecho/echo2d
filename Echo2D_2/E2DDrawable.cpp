//
//  E2DDrawable.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 25/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DDrawable.h"

#include "E2DController.h"
#include "E2DRenderer.h"
#include "E2DTextureManager.h"
#include "E2DLayer.h"

E2DDrawable::E2DDrawable(E2Ddepth depth_)
{
    this->setClassName("E2DDrawable");
    this->setClassType(E2D_CLASS_DRAWABLE);
    
    //this->setSize(s.width, s.height);
    
    __controller = E2DController::SharedController();
    __screenSize = __controller->ScreenSize();
    __layer = nullptr;
    _renderer = E2DRenderer::SharedRenderer();
    
    __pos.x = 0.0f, __pos.y = 0.0f;
    __ap.x = 0.0f, __ap.y = 0.0f;
    __ap_adj.x = 0.0f, __ap_adj.y = 0.0f;
    __depth = depth_;
    visible = true;
    __scale = 1.0f;
    
    _textureID = 0;
}

E2DDrawable::~E2DDrawable()
{
    _renderer = nullptr;
    __controller = nullptr;
    __layer = nullptr;
}

/*
void E2DDrawable::Print() const
{
    this->Print("printing...");
}

void E2DDrawable::Print(const char *tag_) const
{
    E2D_BL;
    E2D_OBJ_PRINT_START("%s", tag_);
    e2dVertexPrint(__v[0], "v[0]");
    e2dVertexPrint(__v[1], "v[1]");
    e2dVertexPrint(__v[2], "v[2]");
    e2dVertexPrint(__v[3], "v[3]");
    E2D_PRINT("\ntextureID:%d", _textureID);
    E2D_OBJ_PRINT_END;
}
*/

E2Dsize E2DDrawable::ScreenSize()
{
    return __screenSize;
}

E2Dcolor E2DDrawable::Color()
{
    return __color;
}

GLuint E2DDrawable::TextureID() const
{
    return _textureID;
}



