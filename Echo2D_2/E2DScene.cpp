//
//  E2DScene.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 01/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DScene.h"
#include "E2DLayer.h"
#include "E2DController.h"

#include "E2DRenderer.h"
E2DScene::E2DScene()
{
    this->setClassName("E2DScene");
    this->setClassType(E2D_CLASS_SCENE);
    _controller = E2DController::SharedController();
    renderer = E2DRenderer::SharedRenderer();
    
    __screenSize = _controller->ScreenSize();
    __screenCenter(__screenSize.width * 0.5f, __screenSize.height * 0.5f);
    
    layers = new E2DArray();
    
    touchClaimed = false;
    touchIndex = 0;
}

E2DScene::~E2DScene()
{
    E2D_RELEASE(layers);
}

//*
inline
void E2DScene::print(const char *msg_) const
{
    E2D_PRINT("\nlayer count:%llu, %llu", layers->Size(), layers->Count());
    while (E2DLayer *l = (E2DLayer *)layers->NextObject()) {
        l->Print();
    }
}
// */

inline
void E2DScene::sortLayers()
{
    for (E2Dull_t i = 0; i < layers->Size(); i++)
    {
        E2DLayer *l1 = (E2DLayer *)layers->ObjectAtIndex(i);
        if (!l1)
            continue;
        
        for (E2Dull_t j = i; j < layers->Size(); j++)
        {
            E2DLayer *l2 = (E2DLayer *)layers->ObjectAtIndex(j);
            if (!l2)
                continue;
            
            if (l2->__depth < l1->__depth)
            {
                l1 = l2;
                layers->SwapObjects(i, j);
            }
        }
    }
    
    
}

void E2DScene::refresh(E2Delapsed dt_)
{
    //E2D_SYS_PRINT("refresh");
    //this->Print("layers:");
    //this->print("prnt");
    //E2D_BL;
    E2D_Obj_ *temp = nullptr;
    layers->ResetEnumerator();
    //while ((temp = (E2DLayer *)_layers->NextObject()))
    while (layers->NextObject(&temp))
    {
        if (temp)
        {
            //E2D_SYS_PRINT("layer update");
            //((E2DLayer *)temp)->Update(dt_);
            ((E2DLayer *)temp)->render(dt_);
        }
    }
    
    /*
    for (unsigned int i = 0; i < __layers->Size(); i++)
    {
        //E2D_SYS_PRINT("update layer[%d]", i);        
    }
    //*/
}

void E2DScene::AddLayer(E2DLayer *layer_)
{
    layers->AddObject(layer_);
    this->sortLayers();
}

void E2DScene::RemoveLayer(E2DLayer *layer_)
{
    layers->RemoveObject(layer_);
}

bool E2DScene::touchBegan(E2Dpoint p_)
{
    E2D_SYS_PRINT("t began:(%f, %f)", p_.x, p_.y);
    
    for (E2Dull_t i = 0; i < layers->Count(); i++)
    {
        E2DLayer *l = (E2DLayer *)layers->ObjectAtIndex(i);
        if ((touchClaimed = l->touchBegan(p_)))
        {
            touchIndex = i;
            return true;
        }
    }
    
    return false;
}

void E2DScene::touchMoved(E2Dpoint p_)
{
    E2D_SYS_PRINT("t moved:(%f, %f)", p_.x, p_.y);
    
    if (touchClaimed)
    {
        E2DLayer *l = (E2DLayer *)layers->ObjectAtIndex(touchIndex);
        l->touchMoved(p_);
        return;
    }
    
    for (E2Dull_t i = 0; i < layers->Count(); i++)
    {
        E2DLayer *l = (E2DLayer *)layers->ObjectAtIndex(i);
        l->touchMoved(p_);
    }
}

void E2DScene::touchEnded(E2Dpoint p_)
{
    E2D_SYS_PRINT("t ended:(%f, %f)", p_.x, p_.y);
    
    if (touchClaimed)
    {
        E2DLayer *l = (E2DLayer *)layers->ObjectAtIndex(touchIndex);
        l->touchEnded(p_);
        return;
    }
    
    for (E2Dull_t i = 0; i < layers->Count(); i++)
    {
        E2DLayer *l = (E2DLayer *)layers->ObjectAtIndex(i);
        l->touchEnded(p_);
    }
    
    touchClaimed = false;
}

