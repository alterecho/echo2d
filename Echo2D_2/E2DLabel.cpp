//
//  E2DLabel.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 06/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DLabel.h"
#include "E2DRenderer.h"
#include "E2DFont.h"
#include "E2DFontManager.h"

E2DLabel::E2DLabel(const char *string_, E2Ddepth depth_) : E2DDrawable(depth_)
{
    this->setClassName("E2DLabel");
    this->setClassType(E2D_CLASS_LABEL);
    setSize(20.0f, 20.0f);
    
    
    v_indx = 0;
    v_count = 0;
    f_count = 0;
    string = nullptr;
    v = nullptr;
    v_elem_indx = nullptr;
    fontManager = E2DFontManager::SharedFontManager();
    
    this->SetString(string_);
    
    this->SetColor(E2Dcolor(255, 255, 255, 255));
    
    
}

E2DLabel::~E2DLabel()
{
    E2D_FREE(v);
}

void E2DLabel::Print() const
{
    this->Print("printing...");
}

void E2DLabel::Print(const char *msg_) const
{
    E2D_BL;
    E2D_SYS_PRINT("%s", msg_)
    E2D_PRINT("\nstring:%s fonts:%lu, vertices:%lu", string, f_count, v_count);
    E2D_PRINT("\nvertices:");
    for (size_t i = 0; i < v_count; i++)
    {
        e2dVertexPrint(v[i]);
    }
    E2D_PRINT("\nelement array:\n");
    for (size_t i = 0; i < v_count; i++)
    {
        E2D_PRINT("%d, ", v_elem_indx[i]);
    }
}

inline
void E2DLabel::addFontVertices(const E2DFont *f_)
{
    //E2D_PRINT("\nadding font:%c", f_->Char());
    f_->Print("adding...");
    //f_->Print();
    _textureID = f_->TextureID();
    for (char i = 0; i < 4; i++)
    {
        const E2Dvertex *vtcs = f_->Vertices();
        //E2D_PRINT("\nacces:%lu", (__v_indx + 1));
        
        v_elem_indx[v_indx] = v_indx;
        //e2dVertexPrint(v[i]);
        v[v_indx++] = vtcs[i];
    }
    labelLength += f_->Size().width;

}

//*
inline
void E2DLabel::resetFontpositions()
{
    E2D_PRINT("\nlabel l:%lu", labelLength);
    float pos = -(float)labelLength * 0.5f;
    E2D_PRINT("\npos:%f, label l:%zu", pos, labelLength);
    size_t i = 0;
    //for (E2Dul_t i = 0; i < __v_count; i)
    while (i < v_count)
    {
        float w = v[i + 1].position.x - v[i].position.x;
        printf("\nw:%f", w);
        //for (char j = i; j < (i + 4); j++)
        {
            printf("\n[%lu]pos:%f", i, pos);
            v[i++].position.x = pos;
            //__v[i].color(e2d::random_0_1(), e2d::random_0_1(), e2d::random_0_1(), 1.0f);
            v[i++].position.x = pos + w;
            v[i++].position.x = pos;
            v[i++].position.x = pos + w;
            pos += w;
        }
    }
    
}
// */

void E2DLabel::SetString(const char *string_)
{
    
    size_t _strlen = strlen(string_) * 4;
    bool reset = false;
    if (_strlen > v_count)
    {
        E2D_FREE(string);
        E2D_FREE(v);
        v_count = _strlen;
        f_count = v_count * 0.25f;
        string = (char *)malloc(sizeof(char) * (f_count + 1));
        v = (E2Dvertex *)malloc(sizeof(E2Dvertex) * v_count);
        v_elem_indx = (unsigned char *)malloc(sizeof(unsigned char *) * v_count);
        reset = true;
    }
    
    strcpy(string, string_);

    v_indx = 0;
    labelLength = 0;
    for (char i = 0; i < f_count; i++)
    {
        this->addFontVertices(fontManager->FontForCharacter(string[i], "ocr_20.e2dfnt"));
    }
    
    if (reset)
        this->resetFontpositions();
    
}

const char* E2DLabel::String()
{
    
    if (!this)
    {
        E2D_PRINT("\nE2DLabel:object invalid");
        exit(1);
    }
    return string;
}

void E2DLabel::render()
{
    _renderer->Render(this);
    //_renderer->Render();
    
    /*
    _renderer->UploadColorArray(&__v[0].position.x, sizeof(E2Dvertex));
    _renderer->UploadColorArray(&__v[0].color.r, sizeof(E2Dvertex));
    _renderer->UploadTexCoordArray(&__v[0].texCoord.s, sizeof(E2Dvertex));
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 1);
     //*/
}

const E2Dvertex* E2DLabel::Vertices() const
{
    return v;
}

void E2DLabel::SetColor(E2Dcolor color_)
{
    for (size_t i = 0; i < v_count; i++)
    {
        v[i].color = color_;
    }
}