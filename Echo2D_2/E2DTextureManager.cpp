//
//  E2DTextureManager.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 26/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DTextureManager.h"
#include "E2DFileManager.h"

#include "E2DRenderer.h"
#include "E2DDrawable4.h"

#pragma mark -
#pragma mark E2DTexture
#pragma mark -

E2DTexture::E2DTexture(E2DString *name_, E2Dsize size_, GLuint tID_)
{
    this->setClassName("E2DTexture");
    this->setClassType(E2D_CLASS_TEXTURE);
    name = (E2DString *)name_->Retain();
    size = size_;
    textureID = tID_;
    
    coord[0].s = 0.0f, coord[0].t = 1.0f;
    coord[1].s = 1.0f, coord[1].t = 1.0f;
    coord[2].s = 0.0f, coord[2].t = 0.0f;
    coord[3].s = 1.0f, coord[3].t = 0.0f;
}

E2DTexture::E2DTexture(E2DString *name_, E2Dsize size_, GLuint tID_, E2DtexCoord coord_[])
{
    this->setClassName("E2DTexture");
    this->setClassType(E2D_CLASS_TEXTURE);
    
    name = (E2DString *)name_->Retain();
    size = size_;
    textureID = tID_;
    for (char i = 0; i < 4; i++)
    {
        coord[i] = coord_[i];
    }
}

E2DTexture::~E2DTexture()
{
    name->Release();
}

void E2DTexture::print(const char *tag_) const
{
    E2D_PRINT("\nname:%s", name->String());
    size.Print();
    E2D_PRINT("\ncoordinates:\n(%f, %f) (%f, %f)\n(%f, %f) (%f, %f)",
              coord[0].s, coord[0].t,
              coord[1].s, coord[1].t,
              coord[2].s, coord[2].t,
              coord[3].s, coord[3].t);
}

E2Dsize E2DTexture::Size()
{
    return size;
};

GLuint E2DTexture::ID()
{
    return textureID;
}

E2DtexCoord* E2DTexture::Coord()
{
    return coord;
}

#pragma mark -
#pragma mark E2DTextureManager
#pragma mark -
static E2DTextureManager* instanceOfTextureManager(nullptr);

E2DTextureManager* E2DTextureManager::SharedTextureManager()
{
    E2D_ASSERT(instanceOfTextureManager, "TextureManager not initialized");
    return instanceOfTextureManager;
}

E2DTextureManager::E2DTextureManager()
{
    this->setClassName("E2DTextureManager");
    this->setClassType(E2D_CLASS_TEXTURE_MANAGER);
    
    instanceOfTextureManager = this;
    __renderer = E2DRenderer::SharedRenderer();
    __textures = new E2DDictionary(_E2D_DICT_SIZE);
}

E2DTextureManager::~E2DTextureManager()
{
    
}

void E2DTextureManager::print(const char *str_) const
{
    __textures->print(str_);
}

E2DTexture* E2DTextureManager::TextureForName(const char *name_)
{
    return (E2DTexture *)__textures->ObjectForKey(name_);
}

void E2DTextureManager::AssignTexture(const char *textureName_, E2DDrawable4 *d_)
{
    
    E2DTexture *texture = nullptr;
    texture = this->TextureForName(textureName_);
    //*
    if (texture)
    {
        E2Dsize tSize = texture->Size();
        d_->setSize(tSize.width, tSize.height);
        d_->_textureID = texture->ID();
        for (char i = 0; i < 4; i++)
        {
            d_->__v_orig[i].texCoord = d_->__v[i].texCoord = texture->coord[i];
        }
        //E2D_PRINT("\nfound");
        return;
    }
    // */
    
    //E2D_PRINT("not found");
    
    E2DString *name = new E2DString(textureName_);
    GLuint tID;
    E2Dsize tSize = __renderer->CreateTexture(textureName_, &tID);
    texture = new E2DTexture(name, tSize, tID);
    name->Release();
    d_->setSize(tSize.width, tSize.height);
    d_->_textureID = texture->ID();
    __textures->SetObject(texture, textureName_);
    texture->Release();
}

E2DTexture* E2DTextureManager::CacheTexture(const char *file_)
{
    E2DTexture *ret = this->TextureForName(file_);
    if (ret)
    {
        return ret;
    }
    
    GLuint tID = 0;
    E2Dsize tSize = __renderer->CreateTexture(file_, &tID);
    E2DString *tName = new E2DString(file_);
    ret = new E2DTexture(tName, tSize, tID);
    tName->Release();
    __textures->SetObject(ret, file_);
    ret->Release();
    
    return ret;
}

void E2DTextureManager::CacheTextureAtlas(const char * fileName_)
{
    if (__textures->ObjectForKey(fileName_))
        return;
    
    E2DString *imageName = new E2DString(fileName_);
    imageName->deleteExtension();
    imageName->Append(".png");
    imageName->Print();
    E2D_PRINT("\n%s", imageName->String());
    
    char *path = NULL;
    E2DFileManager::SharedFileManager()->PathForFile_alc(fileName_, &path);
    std::ifstream file(path);
    E2D_FREE(path);
    
    E2D_ASSERT(file.is_open(), "unable to open file '%s'", fileName_);
    GLuint tID = 0;
    __renderer->CreateTexture(imageName->String(), &tID);
    imageName->Release();
    char str_buf[256];
    while (e2d::readStringFromFile(file, str_buf))
    {
        if (strcmp(str_buf, ">") == 0)
        {
            e2d::readStringFromFile(file, str_buf);
            E2DString *name = new E2DString(str_buf);
            
            E2Dsize size;
            size.width = e2d::readElementFromFile<float>(file);
            size.height = e2d::readElementFromFile<float>(file);
            
            E2DtexCoord coord[4];
            coord[0].s = e2d::readElementFromFile<float>(file);
            coord[0].t = e2d::readElementFromFile<float>(file);
            
            coord[1].s = e2d::readElementFromFile<float>(file);
            coord[1].t = e2d::readElementFromFile<float>(file);
            
            coord[2].s = e2d::readElementFromFile<float>(file);
            coord[2].t = e2d::readElementFromFile<float>(file);
            
            coord[3].s = e2d::readElementFromFile<float>(file);
            coord[3].t = e2d::readElementFromFile<float>(file);
            
            E2DTexture *t = new E2DTexture(name, size, tID, coord);
            __textures->SetObject(t, name->String());
            
            name->Release();
        }
    }
    
    file.close();
    
    __textures->Print();
    
    
}