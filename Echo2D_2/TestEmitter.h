//
//  E2DTestEmitter.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef __Echo2D_2__E2DTestEmitter__
#define __Echo2D_2__E2DTestEmitter__

#include <iostream>
#include "E2DScene.h"
#include "E2DLayer.h"
#include "E2DEmitter.h"

class TestEmitter : public E2DScene {
    E2DLayer        *l1;
    E2DEmitter      *emitter1;
    
public:
    TestEmitter();
    ~TestEmitter();
    
    void Update(E2Delapsed dt_);
};

#endif /* defined(__Echo2D_2__E2DTestEmitter__) */
