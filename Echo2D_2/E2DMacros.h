//
//  E2DMacros.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 10/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_MACROS_H
#define E2D_MACROS_H

#include "E2DConfig.h"
#include <math.h>

#define E2D_DEGREES_TO_RADIANS(deg)     deg * 0.01745329252
#define E2D_RADIANS_TO_DEGREES(rad)     rad * 57.2957795100

#define E2D_CONVERT_255_1(val)  val / 255.0f
#define E2D_CONVERT_1_255(val)  255.0f / val


#define E2D_PRINT(log, ...)  if (_E2D_DEBUG) { \
    printf(log, ## __VA_ARGS__); \
}

#define E2D_BOOL_STRING(v) (v ? "true" : "false")

#define E2D_CLASS_PRINT(log, ...)  if (_E2D_DEBUG) { \
printf("\n%s(%p)(%lu):- " log, E2D_CLASSNAME, this, RetainCount(), ## __VA_ARGS__); \
}

#define E2D_OBJ_PRINT_START(log, ...)  if (_E2D_DEBUG) { \
        printf("\n[%s] (%p) (%lu):- #### " log " ####", E2D_CLASSNAME, this, RetainCount(), ## __VA_ARGS__); \
}

#define E2D_OBJ_PRINT_END  if (_E2D_DEBUG) { \
        printf("\n[%s] (%p) (%lu):- #### print end ####", E2D_CLASSNAME, this, RetainCount() ); \
}

#define E2D_SYS_PRINT(log, ...)  if (_E2D_DEBUG) { \
    printf("\n$ECHO2D: %s(%p)(%lu):- " log, E2D_CLASSNAME, this, RetainCount(), ## __VA_ARGS__); \
}

#define E2D_BL      if(_E2D_DEBUG) printf("\n")
#define E2D_BL_2    if(_E2D_DEBUG) printf("\n\n");

#define E2D_EXIT(log, ...)  if (1) { \
    printf("\n%s:- [ERROR] - " log, E2D_CLASSNAME, ## __VA_ARGS__); \
    exit(1); \
}

#define E2D_ASSERT(test, log, ...)  if (1) { \
    if (!test) \
    { \
        printf("\n[ASSERTION ERROR] - " log, ## __VA_ARGS__); \
        exit(1); \
    } \
}

#define E2D_SYS_ASSERT(test, log, ...)  if (1) { \
    if (!test) \
    { \
        printf("\n%s:[ASSERTION ERROR] - " log, E2D_CLASSNAME, ## __VA_ARGS__); \
        exit(1); \
    } \
}

#define E2D_FREE(var) \
        free(var); \
        var = NULL; \

#define E2D_RELEASE(var) \
    if (var) \
    { \
        var->Release(); \
        var = nullptr; \
    }

#define _E2D_DISABLE_COPY(CLASS) \
    CLASS(const CLASS& obj_) {} \

#define _E2D_DISABLE_ASSIGNMENT(CLASS) \
    void operator=(const CLASS& obj_) {} \


#endif
