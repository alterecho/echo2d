//
//  E2DMusicPlayer.h
//  Echo2D_2_Audio
//
//  Created by Vijay Chandran J on 13/06/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#ifndef E2D_MUSIC_PLAYER_H
#define E2D_MUSIC_PLAYER_H

#include <iostream>
#include "E2D_Obj_.hpp"
#include "E2DObjectTypes.h"
#include "E2DProtocols.h"

class E2DMusicPlayer : public E2D_Obj_, protected e2d::protocol::MusicPlayerDelegate {
    
    E2D_NO_PRINT;
    
public:
    E2DMusicPlayer();
    ~E2DMusicPlayer();
    
    virtual void musicPlaybackFinished(void) { };
    
    virtual void PreloadMusic(const char *) { };
    virtual void Play(const char *, bool loop_) { };
    virtual void Play(bool loop_) { };
    virtual void Pause(void) const { };
    virtual void Stop(void) const { };
    virtual const char* CurrentMusic(void) const { return nullptr; };
    virtual bool IsPlaying(void) const { return false; };
    virtual bool IsPlayingMusic(const char *) const { return true; };
};

#endif /* defined(__Echo2D_2_Audio__E2DMusicPlayer__) */
