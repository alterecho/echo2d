//
//  E2DTestEmitter.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "TestEmitter.h"

TestEmitter::TestEmitter()
{
    l1 = new E2DLayer(10);
    emitter1 = new E2DEmitter("particle0.png", 1, 0);
    emitter1->LoadConfig("projectile.e2decfg");
    emitter1->SetActiveParticles(1);
    l1->AddDrawable(emitter1);
    this->AddLayer(l1);
    emitter1->Print();
    /*
    emitter1->SetMinLifeTime(2.0f);
    emitter1->SetMaxLifeTime(3.0f);
    emitter1->SetTranslationalMotion(true);
    emitter1->SetMinVelocity(E2Dvec2(50.0f, 20.0f));
    emitter1->SetMaxVelocity(E2Dvec2(75.0f, -20.0f));
    
    emitter1->SetMinScaleStart(40.0f);
    emitter1->SetMaxScaleStart(50.0f);
    emitter1->SetMinScaleEnd(10.0f);
    emitter1->SetMaxScaleEnd(10.0f);
    emitter1->E2DEmitterSettings::SetScale(false);
    // *
    emitter1->SetMinColorStart(E2Dcolor(255, 255, 255, 255));
    emitter1->SetMaxColorStart(E2Dcolor(255, 255, 255, 255));
    
    emitter1->SetMinColorEnd(E2Dcolor(0, 0, 0, 255));
    emitter1->SetMaxColorEnd(E2Dcolor(0, 0, 0, 255));
    emitter1->SetMorphColor(true);
    
    emitter1->SetMinOpacityStart(255);
    emitter1->SetMaxOpacityStart(255);
    emitter1->SetMinOpacityEnd(0);
    emitter1->SetMaxOpacityEnd(0);
    emitter1->SetFade(true);
    
    emitter1->SetRadialMotion(false);
    emitter1->SetMorphColor(false);
    emitter1->SetTranslationalMotion(true);
    emitter1->E2DEmitterSettings::SetScale(false);
    //*/
}

TestEmitter::~TestEmitter()
{
    
}

void TestEmitter::Update(E2Delapsed dt_)
{
    
}

