//
//  E2DFXPlayer.h
//  Echo2D_2_Audio
//
//  Created by Vijay Chandran J on 31/05/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#ifndef E2D_FX_PLAYER_H
#define E2D_FX_PLAYER_H

#include <iostream>
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#include "E2D_Obj_.hpp"
#include "E2DDataStructs.hpp"
#include "E2DObjectTypes.h"

class E2DFX : public E2D_Obj_ {
    friend class E2DFXPlayer;
    
    ALuint          _id;
    char            *name;
    
    void print(const char *msg_) const
    {
        E2D_PRINT("\nid:%u, name:%s", _id, name);
    }
    
    E2DFX(const char *fileName_);
    ~E2DFX();
    
};

class E2DFXSource : public E2D_Obj_ {
    friend class E2DFXPlayer;
    
    ALuint      _id;
    bool        fade;
    float       fadeRate, fadeTarget;
    E2Dseconds  fadeTime;
    
    class E2DFXPlayer *player;
    
    E2DFXSource(class E2DFXPlayer *);
    ~E2DFXSource();
    
    void print(const char *msg) const
    {
        E2D_PRINT("\nsourceID:%ud", _id);
    }
    
    bool isPlaying() const;
    void fadeTo(float v_, E2Dseconds t_);
    void update(E2Delapsed);
    
};

class E2DFXPlayer : public E2D_Obj_ {
    friend          E2DFXSource;
    ALCdevice       *device;
    ALCcontext      *context;
    E2DDictionary   *sounds;
    E2DArray        *sources;
    
    unsigned char   fadeCount;
        
    ALuint nextSource() const;
    ALuint soundID(const char *) const;
    E2DFX* sound(const char *) const;    /* gets fx for given name, if loaded (through LoadSound()) */
    //E2DFXSource* getSource(ALuint s) const;
    
    /* increment/decrement number of sound sources, that needs to be updated (through update() method)*/
    void incrementFadeCount();
    void decrementFadeCount();
    
public:
    E2DFXPlayer();
    ~E2DFXPlayer();
    
    E2D_NO_PRINT;
    void update(E2Delapsed dt_);
    
    ALuint LoadSound(const char *);
    void UnloadSound(const char *);
    void SetVolume(float);
    float Volume(float);
    ALuint PlaySound(const char *);
    
    void FadeSound(ALuint s_, float target_, E2Dseconds time_);
};

#endif /* defined(__Echo2D_2_Audio__E2DFXPlayer__) */
