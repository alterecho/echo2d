//
//  E2DSprite.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 25/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_SPRITE_H
#define E2D_SPRITE_H

#include <iostream>
#include "E2DDrawable4Z.h"

class E2DSprite : public E2DDrawable4Z {
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_SPRITE; }
    E2DSprite(const char *fileName_, E2Ddepth depth_);
    
};


#endif /* defined(__Echo2D_2__E2DSprite__) */
