//
//  E2DSceneManager.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 02/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DSceneManager.h"
#include "E2DScene.h"

static E2DSceneManager *instanceOfSceneManager(nullptr);

E2DSceneManager* E2DSceneManager::SharedSceneManager()
{
    E2D_ASSERT(instanceOfSceneManager, "SceneManager not initialized");
    return instanceOfSceneManager;
}

E2DSceneManager::E2DSceneManager()
{
    this->setClassName("E2DSceneManager");
    this->setClassType(E2D_CLASS_SCENE_MANAGER);
    instanceOfSceneManager = this;
    
    currentScene = nullptr;
}

E2DSceneManager::~E2DSceneManager()
{
    instanceOfSceneManager = nullptr;
}

void E2DSceneManager::ReplaceScene(E2DScene *scene_)
{
    //E2D_RELEASE(currentScene);
    deprecatedScene = currentScene;
    currentScene = (E2DScene *)scene_->Retain();
}

E2DScene* E2DSceneManager::CurrentScene()
{
    return currentScene;
}

void E2DSceneManager::removeDeprecated()
{
    E2D_RELEASE(deprecatedScene);
}