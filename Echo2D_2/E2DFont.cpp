//
//  E2DFont.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 07/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DFont.h"

E2DFont::E2DFont(char c_, E2Dsize s_, const E2DtexCoord *coord_, E2Duint t_) : E2DDrawable4Z(0)
{
    this->setClassName("E2DFont");
    this->setClassType(E2D_CLASS_FONT);
    
    this->setSize(s_.width, s_.height);
    __char = c_;
    E2D_ASSERT(coord_, "E2DFont:null coord ptr in constructor");
    this->setTexCoord(coord_);
    _textureID = t_;
}

E2DFont* E2DFont::Copy() const
{
    E2DFont *ret = new E2DFont(__char, __size, &__v[0].texCoord, _textureID);
    
    return ret;
}



E2DFont::~E2DFont()
{
    
}

void E2DFont::print(const char *tag_) const
{
    E2D_PRINT("\nchar:%c", __char);
    e2dVertexPrint(__v[0], "v[0]");
    e2dVertexPrint(__v[1], "v[1]");
    e2dVertexPrint(__v[2], "v[2]");
    e2dVertexPrint(__v[3], "v[3]");
    E2D_PRINT("\ntextureID:%d", _textureID);
}

/*
void E2DFont::SetPosition(E2Dpoint p_)
{
    __v[0].position.x = p_.x - __size.width * 0.5f;
    __v[0].position.y = p_.y + __size.height * 0.5f;
    
    __v[1].position.x = p_.x + __size.width * 0.5f;
    __v[1].position.y = p_.y + __size.height * 0.5f;
    
    __v[2].position.x = p_.x - __size.width * 0.5f;
    __v[2].position.y = p_.y - __size.height * 0.5f;
    
    __v[3].position.x = p_.x + __size.width * 0.5f;
    __v[3].position.y = p_.y - __size.height * 0.5f;
}
*/
const char E2DFont::Char() const
{
    return __char;
}