//
//  E2DAudioData.h
//  Echo2D_2_Audio
//
//  Created by Vijay Chandran J on 02/06/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#ifndef E2D_AUDIO_DATA_H
#define E2D_AUDIO_DATA_H

#include <iostream>
#include "E2D_Obj_.hpp"
#include <OpenAL/al.h>

typedef struct {
    char type[5];
    long size, chunkSize;
    short formatType, channels;
    long sampleRate, avgBytesperSec;
    short bytesPerSample, bitsPerSample;
    long dataSize;
} E2DaudioStructWAV;

class E2DAudioData : public E2D_Obj_{
    friend class E2DAudioLoader;
    friend class E2DFXPlayer;
    
    long sampleRate, dataSize;
    ALenum format;
    short bitsPerSample, channels;
    unsigned char *buffer;
    
public:
    E2DAudioData(long sampleRate_, long dataSize_, short bps_, short channels_, unsigned char *buffer_);
    ~E2DAudioData();
    
    
    E2D_NO_PRINT;
};

#endif /* defined(__Echo2D_2_Audio__E2DAudioData__) */
