//
//  E2DEmitter.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DEmitter.h"
#include "E2DTextureManager.h"
#include "E2DParticle.h"
#include "E2DRenderer.h"

E2DEmitter::E2DEmitter(const char *spriteFile_, unsigned int pCount_, E2Ddepth depth_) :
E2DDrawable(depth_), angle(0.0f), pCount(pCount_)
{
    this->setClassName("E2DEmitter");
    this->setClassType(E2D_CLASS_EMITTER);
    
    //pCount_ = 4; //remove
    //pCount = pCount_;
    
    texManager = E2DTextureManager::SharedTextureManager();
    activeParticles = 0;
    
    p = (E2DpRenderAttribs *)malloc(sizeof(E2DpRenderAttribs) * pCount_);
    
    
    particles = new E2DArray(pCount_);
    
    for (unsigned int i = 0; i < pCount_; i++)
    {
        E2DParticle *temp = new E2DParticle(spriteFile_, &p[i]);
        temp->emitter = this;
        _textureID = temp->_textureID;
        
        particles->AddObject(temp);
        temp->Release();
    }

    /*
    
    this->SetMinLifeTime(0.25f);
    this->SetMaxLifeTime(2.0f);
    
    this->SetMinScaleStart(30.0f);
    this->SetMaxScaleStart(30.0f);
    this->SetMinScaleEnd(10.0f);
    this->SetMaxScaleEnd(10.0f);
    this->E2DEmitterSettings::SetScale(false);
    // *
    this->SetMinColorStart(E2Dcolor(0, 0, 0, 255));
    this->SetMaxColorStart(E2Dcolor(255, 255, 255, 255));
    
    this->SetMinColorEnd(E2Dcolor(255, 0, 0, 255));
    this->SetMaxColorEnd(E2Dcolor(255, 0, 0, 255));
    this->SetMorphColor(false);
    
    this->SetMinOpacityStart(255);
    this->SetMaxOpacityStart(255);
    this->SetMinOpacityEnd(0);
    this->SetMaxOpacityEnd(0);
    this->SetFade(false);
    // */
    
    
    this->SetMinVelocity(E2Dvec2(50.0f, 0.0f));
    this->SetMaxVelocity(E2Dvec2(75.0f, 0.0f));
    
    
    //this->SetMinVelocityDamping(E2Dvec2(0.89f, 0.0f));
    //this->SetMaxVelocityDamping(E2Dvec2(0.98f, 0.0f));
    
    //this->SetMinSpreadVelocity(E2Dvec2(10.0f, 0.0f));
    //this->SetMaxSpreadVelocity(E2Dvec2(50.0f, 0.0f));
    
    //this->SetGravity(E2Dvec2(0.0f, -9.8f));
    /*
     this->SetMinRadialDistance(20.0f);
     this->SetMaxRadialDistance(20.0f);
     this->SetMaxRadialAngle(270.0f);
    
    //this->SetMinRadialDistanceDelta(100.0f);
    //this->SetMaxRadialDistanceDelta(100.0f);
    this->SetMinRadialAngleDelta(10.0f);
    this->SetMaxRadialAngleDelta(10.0f);
    this->SetRadialMotion(true);
     //*/
    
    /*
    this->LoadConfig("projectile_.e2decfg");
    //this->SetAngle(90.0f);
    
    this->SetRadialMotion(false);
    this->SetMorphColor(false);
    this->SetTranslationalMotion(true);
    this->E2DEmitterSettings::SetScale(false);
    this->SetMinScaleStart(30.0f);
    this->SetMaxScaleStart(30.0f);
    //*/
    //this->SetActiveParticles(2);
    
}

E2DEmitter::~E2DEmitter()
{
    E2D_RELEASE(particles);
    E2D_FREE(p);
}

void E2DEmitter::print(const char *msg_) const
{
    this->E2DEmitterSettings::print();
}

void E2DEmitter::SetAngle(E2Ddegrees d_)
{
    angle = E2D_DEGREES_TO_RADIANS(d_);
}

E2Ddegrees E2DEmitter::Angle() const
{
    return E2D_RADIANS_TO_DEGREES(angle);
}

void E2DEmitter::render()
{
//    this->update(0);
    //_renderer->Render();
    _renderer->Render(this);
}

void E2DEmitter::update(E2Delapsed dt_)
{
    //E2D_CLASS_PRINT("\n%f", 1.0f / dt_);
    //E2D_PRINT("\ncount:%d", __particles->c)
    while (E2DParticle *p = (E2DParticle *)particles->NextObject())
    {
        if (p->active)
        {
            //E2D_PRINT("\nupd:%p", p);
            p->update(dt_);
        }
        else if (activeParticles < activeParticles_MAX)
        {
            this->reset(p);
            activeParticles++;
        }
    }
    //E2D_BL;
    //__v[0].position.Print();
}

void E2DEmitter::reset(E2DParticle *p_)
{
    p_->randomize(lim);
    p_->reflectEmitterStates();
    p_->active = true;
    
    /*
    std::cout << "\np:" << p_ << " lt:" << p_->att->lifeTime;
    std::cout << " color:" << p_->att->color
    << " size:" << p_->att->size
    << " pos:" << p_->att->pos << "vel:" << p_->mod.vel;
    // */
    //std::cout << " rDist:" << mod.radDist << " rAng:" << E2D_RADIANS_TO_DEGREES(mod.radAngle)  << "\n";
}

void E2DEmitter::particleDeactivated()
{
    activeParticles--;
}

