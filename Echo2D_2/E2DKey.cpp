//
//  E2DKey.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 12/05/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DKey.h"

#include "E2DFontManager.h"
#include "E2DTextureManager.h"
#include "E2DController.h"

#pragma mark -
#pragma mark E2DKeyDrawable
#pragma mark -
E2DKeyDrawable::E2DKeyDrawable(E2Dvertex *v_, E2Duint texture_)
{
    this->setClassName("E2DKeyDrawable");
    this->setClassType(E2D_CLASS_KEY_DRAWABLE);
    
    for (char i = 0; i < 4; i++)
    {
        v[i] = &v_[i];
        //v[i]->color = E2Dcolor(0);
        //e2dVertexPrint(v_[i]);
    }
    ss = E2DController::SharedController()->ScreenSize();
    ss_h = ss * 0.5f;
    
    tID = texture_;
    
    pos(0.0f, 0.0f);
    size(0.0f, 0.0f);
}

E2DKeyDrawable::~E2DKeyDrawable()
{
    
}

inline void E2DKeyDrawable::print(const char *log_) const
{
    E2D_PRINT("\npos:(%f, %f)", pos.x, pos.y);
    E2D_PRINT("\nsize:(%f, %f)", size.width, size.height);
    E2D_PRINT("\nvertices:")
    E2D_PRINT("\n(%f, %f) (%f, %f)", v[0]->position.x, v[0]->position.y, v[1]->position.x, v[1]->position.y);
    E2D_PRINT("\n(%f, %f) (%f, %f)", v[0]->position.x, v[0]->position.y, v[1]->position.x, v[1]->position.y);
    
    E2D_PRINT("\ntexID:%u", tID);
    E2D_PRINT("\ntexCoord:");
    E2D_PRINT("\n(%f, %f) (%f,%f)", v[0]->texCoord.s, v[0]->texCoord.t, v[1]->texCoord.s, v[1]->texCoord.t);
    E2D_PRINT("\n(%f, %f) (%f,%f)", v[2]->texCoord.s, v[2]->texCoord.t, v[3]->texCoord.s, v[3]->texCoord.t);
    
}

void E2DKeyDrawable::setSize(E2Dsize s_)
{
    size = s_;
    printf("\nsetting size:");
    ss.Print("ss");
    ss_h.Print("ss_h");
    e2dVertexPrint((*v)[0]);
    e2dVertexPrint((*v)[1]);
    e2dVertexPrint((*v)[2]);
    e2dVertexPrint((*v)[3]);
    
    v[0]->position(-ss_h.width + pos.x - size.width * 0.5f, -ss_h.height + pos.y + size.height * 0.5f);
    v[1]->position(-ss_h.width + pos.x + size.width * 0.5f, -ss_h.height + pos.y + size.height * 0.5f);
    v[2]->position(-ss_h.width + pos.x - size.width * 0.5f, -ss_h.height + pos.y - size.height * 0.5f);
    v[3]->position(-ss_h.width + pos.x + size.width * 0.5f, -ss_h.height + pos.y - size.height * 0.5f);
    e2dVertexPrint((*v)[0]);
    
    e2dVertexPrint((*v)[0]);
    e2dVertexPrint((*v)[1]);
    e2dVertexPrint((*v)[2]);
    e2dVertexPrint((*v)[3]);
    E2D_BL;
}

void E2DKeyDrawable::setPos(E2Dpoint p_)
{
    pos = p_;
    
    printf("\nsetting pos:");
    ss.Print("ss");
    ss_h.Print("ss_h");
    e2dVertexPrint((*v)[0]);
    e2dVertexPrint((*v)[1]);
    e2dVertexPrint((*v)[2]);
    e2dVertexPrint((*v)[3]);
    
    v[0]->position(-ss_h.width + pos.x - size.width * 0.5f, -ss_h.height + pos.y + size.height * 0.5f);
    v[1]->position(-ss_h.width + pos.x + size.width * 0.5f, -ss_h.height + pos.y + size.height * 0.5f);
    v[2]->position(-ss_h.width + pos.x - size.width * 0.5f, -ss_h.height + pos.y - size.height * 0.5f);
    v[3]->position(-ss_h.width + pos.x + size.width * 0.5f, -ss_h.height + pos.y - size.height * 0.5f);
    
    e2dVertexPrint((*v)[0]);
    e2dVertexPrint((*v)[1]);
    e2dVertexPrint((*v)[2]);
    e2dVertexPrint((*v)[3]);
    
    E2D_BL;

}

void E2DKeyDrawable::setTexCoord(E2DtexCoord *tc_)
{
    for (char i = 0; i < 4; i++)
    {
        v[i]->texCoord = tc_[i];
    }
}


#pragma mark -
#pragma mark E2Dkey
#pragma mark -
E2DKey::E2DKey(unsigned char char_, const char *bgFile_, const char *fontFile_, E2Dvertex v_[])
{
    this->E2D_Obj_::setClassName("E2DKey");
    this->setClassType(E2D_CLASS_KEY);
    
    fontManager = E2DFontManager::SharedFontManager();
    textureManager = E2DTextureManager::SharedTextureManager();
    
    character = char_;
    
    
    //set background
    E2DTexture *t = textureManager->TextureForName(bgFile_);
    bg_size = t->Size();
    bg = new E2DKeyDrawable(&v_[0], t->ID());
    bg->setSize(t->Size());
    //*
    E2D_PRINT("\nprtbgtc");
    e2dTexCoordPrint(&t->Coord()[0]);
    e2dTexCoordPrint(&t->Coord()[1]);
    e2dTexCoordPrint(&t->Coord()[2]);
    e2dTexCoordPrint(&t->Coord()[3]);
     E2D_BL;
    // */
    bg->setTexCoord(t->Coord());
    
    //set letter font
    const E2DFont *f = fontManager->FontForCharacter(char_, fontFile_);
    letter = new E2DKeyDrawable(&v_[4], f->TextureID());
    letter_size = f->Size();
    letter->setSize(letter_size);
    const E2Dvertex *v = f->Vertices();
    E2DtexCoord tc[4];
    for (char i = 0; i < 4; i++)
    {
        tc[i] = v[i].texCoord;
    }
    //*
    E2D_PRINT("\nprtlettertc");
    e2dTexCoordPrint(&tc[0]);
    e2dTexCoordPrint(&tc[1]);
    e2dTexCoordPrint(&tc[2]);
    e2dTexCoordPrint(&tc[3]);
    E2D_BL;
    //*/
    letter->setTexCoord(tc);
    
    this->SetSize(letter_size);
    
}

E2DKey::~E2DKey()
{
    E2D_RELEASE(letter);
    E2D_RELEASE(bg);
}

E2Duint E2DKey::letterTexID()
{
    return letter->tID;
}

E2Duint E2DKey::bgTexID()
{
    return bg->tID;
}

void E2DKey::SetPosition(E2Dpoint pos_)
{
    pos = pos_;
    
    bg->setPos(pos);
    letter->setPos(pos);
}

E2Dpoint E2DKey::Position()
{
    return pos;
}

void E2DKey::SetSize(E2Dsize s_)
{
    size = s_;
    
    bg->setSize(size);
    letter->setSize(size);
}

E2Dsize E2DKey::Size()
{
    return size;
}

E2Dsize E2DKey::LetterSize()
{
    return letter_size;
}

E2Dsize E2DKey::BgSize()
{
    return bg_size;
}