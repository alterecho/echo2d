attribute vec4 position;
attribute vec4 color;

varying vec4 f_color;

uniform mat4 projection;
uniform mat4 modelView;

attribute vec2 TexCoordIn;
varying vec2 TexCoordOut;

void main(void)
{
    f_color = color;
    gl_Position = projection * modelView * position;
    TexCoordOut = TexCoordIn;
}