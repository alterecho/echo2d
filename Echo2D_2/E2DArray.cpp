//
//  E2DArray.cpp
//  E2DDerivedStructs
//
//  Created by Vijay Chandran J on 04/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DArray.h"

E2DArray::E2DArray() : E2DEnumerable(_E2D_OBJ_COUNT + 1)
{
    this->setClassName("E2DArray");
    this->setClassType(E2D_CLASS_ARRAY);
    
    _first = 0;
    _last = 0;
    _objects = (E2D_Obj_ **)malloc(sizeof(E2D_Obj_ *) * (size_t)_size);
    this->initializeArray(&_objects, 0, _size);
}

E2DArray::E2DArray(E2Dull_t size_) : E2DEnumerable(size_ + 1) /* size + 1 for array size prediction, when resizing */
{
    this->setClassName("E2DArray");
    this->setClassType(E2D_CLASS_ARRAY);
    
    _first = 0;
    _last = 0;
    _objects = (E2D_Obj_**)malloc(sizeof(E2D_Obj_ *) * (size_t)_size);
    this->initializeArray(&_objects, 0, _size);
}

E2DArray::~E2DArray()
{
    for (E2Dull_t i = 0; i < _size; i++)
    {
        E2D_RELEASE(_objects[i]);
    }
    
    E2D_FREE(_objects);
    
    _size = 0;
    _first = 0;
    _last = 0;
    
    _enum_pointer = 0;
}

#pragma mark - print -
inline
void E2DArray::print(const char *tag_) const
{
    E2D_SYS_PRINT("\ncount:%llu, size:%llu\n", _count, _size);
    for (E2Dull_t i = 0; i < _size; i++)
    {
        E2D_PRINT("\nobject[%llu]:", i);
        if (_objects[i])
        {
            _objects[i]->Print();
        }
        else
        {
            E2D_PRINT("\n[EMPTY]");
        }
        E2D_BL;
    }
}

#pragma mark - getters -
E2Dull_t E2DArray::Size() const
{
    return _size;
}

E2Dull_t E2DArray::Count() const
{
    return _count;
}


E2D_Obj_* E2DArray::ObjectAtIndex(E2Dull_t index_) const
{
    if (index_ >= _size)
    {
        return nullptr;
    }
    
    return _objects[index_];
}

E2Dull_t E2DArray::IndexOfObject(E2D_Obj_ *obj_) const
{
    for (E2Dull_t i = 0; i < _size; i++)
    {
        if (_objects[i] == obj_)
            return i;
    }
    return 0;
}

bool E2DArray::ContainsObject(E2D_Obj_ *obj_) const
{
    for (E2Dull_t i = 0; i < _size; i++)
    {
        if (_objects[i] == obj_)
            return true;
    }
    return false;
}

#pragma mark - modifiers -
void E2DArray::AddObject(E2D_Obj_ *obj_)
{
    E2D_CLASS_PRINT("count:%llu _last:%llu adding '%s'" , _count, _last, obj_->ClassName());
    
    _objects[_index] = obj_->Retain();
    _last = _index;
    
    if (++_index >= _size)
    {
        this->resize(+1);
    }
    
    _count++;
}

void E2DArray::SwapObjectAtIndex(E2Dull_t index_, E2D_Obj_* obj_)
{
    if (_objects[index_])
    {
        E2D_RELEASE(_objects[index_]);
    }
    
    _objects[index_] = obj_->Retain();
}

void E2DArray::SwapObjects(E2Dull_t index1_, E2Dull_t index2_)
{
    E2D_Obj_ *temp = _objects[index1_];
    _objects[index1_] = _objects[index2_];
    _objects[index2_] = temp;
}

void E2DArray::RemoveObject(E2D_Obj_ *obj_)
{
    for (E2Dull_t i = 0; i < _last; i++)
    {
        if (_objects[i] == obj_)
        {
            this->removeObject(i);
        }
    }
    
}

void E2DArray::RemoveObjectAtIndex(E2Dull_t index_)
{
    if (index_ >= _last)
    {
        return;
    }
    
    this->removeObject(index_);
    
    if (index_ == _last)
        _last--;
}

void E2DArray::RemoveLastObject()
{
    this->removeObject(_last);
    _last--;
    _index--;
}

void E2DArray::RemoveAllObjects()
{
    for (E2Dull_t i = 0; i < _last; i++)
    {
        this->removeObject(i);
    }
}

void E2DArray::Consolidate()
{
    for (E2Dull_t i = 0; i < _size; i++)
    {
        if (!_objects[i])
        {
//            printf("\ni:%llu", i);
            E2Dull_t j = i;
            while (++j < _size)
            {
//                printf("\n\t\tj:%llu", j);
                if (_objects[j])
                {
                    _objects[i] = _objects[j];
                    _objects[j] = nullptr;
                    break;
                }
            }
            
        }
    }
    
}

#pragma mark - enumeration -
E2D_Obj_* E2DArray::NextObject()
{
    while (_enum_pointer < _size)
    {
        if (_objects[_enum_pointer])
        {
            return _objects[_enum_pointer++];
        }
        _enum_pointer++;
    }
    
    _enum_pointer = 0;
    return nullptr;
}

bool E2DArray::NextObject(E2D_Obj_ **stor_)
{
    if (_enum_pointer < _size)
    {
        *stor_ = _objects[_enum_pointer++];
        return true;
    }
    
    _enum_pointer = 0;
    return false;
}
