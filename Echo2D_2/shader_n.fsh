varying lowp vec4 f_color;

varying lowp vec2 TexCoordOut;
uniform sampler2D Texture;

void main(void)
{
    gl_FragColor = f_color * texture2D(Texture, TexCoordOut);
}