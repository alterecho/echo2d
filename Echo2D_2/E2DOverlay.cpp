//
//  E2DOverlay.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DOverlay.h"
#include "E2DRenderer.h"
#include "E2DController.h"
#include "E2DKeyboard.h"

E2DOverlay::E2DOverlay()
{
    this->setClassName("E2DOverlay");
    this->setClassType(E2D_CLASS_OVERLAY);
    
    controller = E2DController::SharedController();
    screenSize = controller->ScreenSize();
    screenCenter(screenSize.width * 0.5f, screenSize.height * 0.5f);
    
    /*
    spr = new E2DSprite("box_w.png", 0);
    spr->SetColor(E2Dcolor(255, 255, 255, 255));
    spr->SetPosition(screenCenter);
    */
    keyboard = E2DKeyboard::SharedKeyboard();
}

E2DOverlay::~E2DOverlay()
{
}

void E2DOverlay::Render()
{
    //E2D_PRINT("overlay rendering");
    if (keyboard->nativeKeyboard)
        E2DRenderer::SharedRenderer()->renderKB(keyboard);
    //E2DRenderer::SharedRenderer()->Render();
}