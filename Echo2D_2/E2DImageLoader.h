//
//  E2DImageLoader.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 25/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_IMAGE_LOADER_H
#define E2D_IMAGE_LOADER_H

#include <iostream>
#include "E2DObject.hpp"

class E2DFileManager;
class E2DImageLoader : public E2DObject {
protected:
    E2DFileManager      *fileManager;
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_IMAGE_LOADER; }
    static E2DImageLoader*  SharedImageLoader();
    E2DImageLoader();
    virtual ~E2DImageLoader();
    virtual E2Dsize LoadImage(const char *imageName_, GLubyte **out_, E2Dsize& aSize_) = 0;
};

#endif /* defined(__Echo2D_2__E2DImageLoader__) */
