precision lowp float;

attribute vec4 position;

attribute vec4 color;
varying vec4 f_color;

uniform mat4 projection, modelView;
attribute float pointSize;

void main(void)
{
    gl_Position = projection * modelView * position;
    f_color = color;
    gl_PointSize = pointSize;
}