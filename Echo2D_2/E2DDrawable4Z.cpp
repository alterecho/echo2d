//
//  E2DDrawable4Z.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DDrawable4Z.h"


E2DDrawable4Z::E2DDrawable4Z(E2Ddepth depth_) : E2DDrawable4(depth_)
{
    E2Dsize s(0.0f, 0.0f);
    __v[0] = e2dVertexMake(
                           E2Dvec2(-s.width * 0.5f, +s.height * 0.5f),
                           E2D_COLOR_WHITE(255),
                           0.0f, 1.0f
                           );
    
    __v[1] = e2dVertexMake(
                           E2Dvec2(+s.width * 0.5f, +s.height * 0.5f),
                           E2D_COLOR_WHITE(255),
                           1.0f, 1.0f
                           );
    
    __v[2] = e2dVertexMake(
                           E2Dvec2(-s.width * 0.5f, -s.height * 0.5f),
                           E2D_COLOR_WHITE(255),
                           0.0f, 0.0f
                           );
    
    __v[3] = e2dVertexMake(
                           E2Dvec2(+s.width * 0.5f, -s.height * 0.5f),
                           E2D_COLOR_WHITE(255),
                           1.0f, 0.0f
                           );
    
    
    for (char i = 0; i < 4; i++)
    {
        __v_orig[i] = __v[i];
    }

}

E2DDrawable4Z::~E2DDrawable4Z()
{
    
}

void E2DDrawable4Z::setSize(float width, float height)
{
    __size.width = width,
    __size.height = height;
    
    __v[0].position.x = -width * 0.5f;
    __v[0].position.y = height * 0.5f;
    
    __v[1].position.x = width * 0.5f;
    __v[1].position.y = height * 0.5f;
    
    __v[2].position.x = -width * 0.5f;
    __v[2].position.y = -height * 0.5f;
    
    __v[3].position.x = width * 0.5f;
    __v[3].position.y = -height * 0.5f;
}

