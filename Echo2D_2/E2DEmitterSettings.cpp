//
//  E2DEmitterSettings.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 15/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DEmitterSettings.h"

void E2DEmitterSettings::SetActiveParticles(unsigned int c_)
{
    activeParticles_MAX = c_;
}

unsigned int E2DEmitterSettings::ActiveParticles() const
{
    return activeParticles_MAX;
}

void E2DEmitterSettings::SetMinLifeTime(E2Dseconds s_) {
    lim.min.lifeTime = s_;
}
E2Dseconds E2DEmitterSettings::MinLifeTime() const
{
    return lim.min.lifeTime;
}

void E2DEmitterSettings::SetMaxLifeTime(E2Dseconds s_) {
    lim.max.lifeTime = s_;
}

E2Dseconds E2DEmitterSettings::MaxLifeTime() const {
    return lim.max.lifeTime;
}

#pragma mark - color -
void E2DEmitterSettings::SetMinColorStart(E2Dcolor c_)
{
    lim.min.color_start = c_;
}

E2Dcolor E2DEmitterSettings::MinColorStart(void) const
{
    return lim.min.color_start;
}

void E2DEmitterSettings::SetMaxColorStart(E2Dcolor c_) {
    lim.max.color_start = c_;
}

E2Dcolor E2DEmitterSettings::MaxColorStart(void) const {
    return lim.max.color_start;
}

void E2DEmitterSettings::SetMorphColor(bool bool_)
{
    morphColor = bool_;
}

bool E2DEmitterSettings::MorphColor() const
{
    return morphColor;
}

void E2DEmitterSettings::SetMinColorEnd(E2Dcolor c_)
{
    lim.min.color_end = c_;
}

E2Dcolor E2DEmitterSettings::MinColorEnd() const
{
    return lim.min.color_end;
}

void E2DEmitterSettings::SetMaxColorEnd(E2Dcolor c_)
{
    lim.max.color_end = c_;
}

E2Dcolor E2DEmitterSettings::MaxColorEnd() const
{
    return lim.max.color_end;
}

#pragma mark - opacity -
void E2DEmitterSettings::SetMinOpacityStart(E2Dclampf o_)
{
    lim.min.color_start.a = o_;
}

E2Dclampf E2DEmitterSettings::MinOpacityStart() const
{
    return lim.min.color_start.a;
}


void E2DEmitterSettings::SetMaxOpacityStart(E2Dclampf o_)
{
    lim.max.color_start.a = o_;
}

E2Dclampf E2DEmitterSettings::MaxOpacityStart() const
{
    return lim.max.color_start.a;
}

void E2DEmitterSettings::SetFade(bool bool_)
{
    fade = bool_;
}

bool E2DEmitterSettings::Fade() const
{
    return fade;
}

void E2DEmitterSettings::SetMinOpacityEnd(E2Dclampf o_)
{
    lim.min.color_end.a = o_;
}

E2Dclampf E2DEmitterSettings::MinOpacityEnd() const
{
    return lim.min.color_end.a;
}

void E2DEmitterSettings::SetMaxOpacityEnd(E2Dclampf o_)
{
    lim.max.color_end.a = o_;
}

E2Dclampf E2DEmitterSettings::MaxOpacityEnd() const
{
    return lim.max.color_end.a;
}

#pragma mark - size -
void E2DEmitterSettings::SetMinScaleStart(E2Dfloat f_)
{
    lim.min.scale_start= f_;
}
E2Dfloat E2DEmitterSettings::MinScaleStart(void) const
{
    return lim.min.scale_start;
}

void E2DEmitterSettings::SetMaxScaleStart(E2Dfloat f_)
{
    lim.max.scale_start = f_;
}
E2Dfloat E2DEmitterSettings::MaxScaleStart(void) const
{
    return lim.max.scale_start;
}

void E2DEmitterSettings::SetScale(bool bool_)
{
    scale = bool_;
}
bool E2DEmitterSettings::Scale() const
{
    return scale;
}

void E2DEmitterSettings::SetMinScaleEnd(E2Dfloat s_)
{
    lim.min.scale_end = s_;
}
E2Dfloat E2DEmitterSettings::MinScaleEnd() const
{
    return lim.min.scale_end;
}

void E2DEmitterSettings::SetMaxScaleEnd(E2Dfloat s_)
{
    lim.max.scale_end = s_;
}

E2Dfloat E2DEmitterSettings::MaxScaleEnd() const
{
    return lim.max.scale_end;
}

#pragma mark - translational motion -

void E2DEmitterSettings::SetTranslationalMotion(bool bool_)
{
    transMotion = bool_;
}

bool E2DEmitterSettings::TranslationalMotion() const
{
    return transMotion;
}

void E2DEmitterSettings::SetMinVelocity(E2Dvec2 v_)
{
    lim.min.vel = v_;
}
E2Dvec2 E2DEmitterSettings::MinVelocity() const
{
    return lim.min.vel;
}

void E2DEmitterSettings::SetMaxVelocity(E2Dvec2 v_)
{
    lim.max.vel = v_;
}
E2Dvec2 E2DEmitterSettings::MaxVelocity() const
{
    return lim.max.vel;
}

void E2DEmitterSettings::SetMinVelocityDamping(E2Dvec2 v_)
{
    lim.min.vel_damping = v_;
}
E2Dvec2 E2DEmitterSettings::MinVelocityDamping(void) const
{
    return lim.min.vel_damping;
}

void E2DEmitterSettings::SetMaxVelocityDamping(E2Dvec2 v_)
{
    lim.max.vel_damping = v_;
}
E2Dvec2 E2DEmitterSettings::MaxVelocityDamping(void) const
{
    return lim.max.vel_damping;
}

void E2DEmitterSettings::SetMinSpreadVelocity(E2Dvec2 v_)
{
    lim.min.vel_spread = v_;
}
E2Dvec2 E2DEmitterSettings::MinSpreadVelocity(void) const
{
    return lim.min.vel_spread;
}

void E2DEmitterSettings::SetMaxSpreadVelocity(E2Dvec2 v_)
{
    lim.max.vel_spread = v_;
}
E2Dvec2 E2DEmitterSettings::MaxSpreadVelocity(void) const
{
    return lim.max.vel_spread;
}

#pragma mark - rotational motion -

void E2DEmitterSettings::SetSpin(bool bool_)
{
    rotMotion = bool_;
}
bool E2DEmitterSettings::Spin() const
{
    return rotMotion;
}

void E2DEmitterSettings::SetMinAngle(E2Ddegrees a_)
{
    lim.min.angle = E2D_DEGREES_TO_RADIANS(a_);
}
E2Ddegrees E2DEmitterSettings::MinAngle() const
{
    return E2D_RADIANS_TO_DEGREES(lim.min.angle);
}

void E2DEmitterSettings::SetMaxAngle(E2Ddegrees a_)
{
    lim.max.angle = E2D_DEGREES_TO_RADIANS(a_);
}
E2Ddegrees E2DEmitterSettings::MaxAngle() const
{
    return E2D_RADIANS_TO_DEGREES(lim.max.angle);
}

#pragma mark - radial motion -
void E2DEmitterSettings::SetRadialMotion(bool bool_)
{
    radMotion = bool_;
}
bool E2DEmitterSettings::RadialMotion() const
{
    return radMotion;
}

void E2DEmitterSettings::SetMinRadialDistance(E2Dfloat f_)
{
    lim.min.radDist = f_;
}
E2Dfloat E2DEmitterSettings::MinRadialDistance() const
{
    return lim.min.radDist;
}

void E2DEmitterSettings::SetMaxRadialDistance(E2Dfloat f_)
{
    lim.max.radDist = f_;
}
E2Dfloat E2DEmitterSettings::MaxRadialDistance(void) const
{
    return lim.max.radDist;
}

void E2DEmitterSettings::SetMinRadialDistanceDelta(E2Dfloat f_)
{
    lim.min.radDist_d = f_;
}
E2Dfloat E2DEmitterSettings::MinRadialDistanceDelta() const
{
    return lim.min.radDist_d;
}
void E2DEmitterSettings::SetMaxRadialDistanceDelta(E2Dfloat f_)
{
    lim.max.radDist_d = f_;
}
E2Dfloat E2DEmitterSettings::MaxRadialDistanceDelta(void) const
{
    return lim.max.radDist_d;
}

void E2DEmitterSettings::SetMinRadialDistanceLimit(E2Dfloat f_)
{
    lim.min.radDist_limit = f_;
}
E2Dfloat E2DEmitterSettings::MinRadialDistanceLimit(void) const
{
    return lim.min.radDist_limit;
}

void E2DEmitterSettings::SetMaxRadialDistanceLimit(E2Dfloat f_)
{
    lim.max.radDist_limit = f_;
}
E2Dfloat E2DEmitterSettings::MaxRadialDistanceLimit(void) const
{
    return lim.max.radDist_limit;
}

void E2DEmitterSettings::SetMinRadialAngle(E2Ddegrees a_)
{
    lim.min.radAngle = E2D_DEGREES_TO_RADIANS(a_);
}
E2Ddegrees E2DEmitterSettings::MinRadialAngle(void) const
{
    return E2D_RADIANS_TO_DEGREES(lim.min.radAngle);
}

void E2DEmitterSettings::SetMaxRadialAngle(E2Ddegrees a_)
{
    lim.max.radAngle = E2D_DEGREES_TO_RADIANS(a_);
}
E2Ddegrees E2DEmitterSettings::MaxRadialAngle(void) const
{
    return E2D_RADIANS_TO_DEGREES(lim.max.radAngle);
}

void E2DEmitterSettings::SetMinRadialAngleDelta(E2Ddegrees a_)
{
    lim.min.radAngle_d = E2D_DEGREES_TO_RADIANS(a_);
}
E2Ddegrees E2DEmitterSettings::MinRadialAngleDelta(void) const
{
    return E2D_RADIANS_TO_DEGREES(lim.min.radAngle_d);
}

void E2DEmitterSettings::SetMaxRadialAngleDelta(E2Ddegrees a_)
{
    lim.max.radAngle_d = E2D_DEGREES_TO_RADIANS(a_);
}
E2Ddegrees E2DEmitterSettings::MaxRadialAngleDelta(void) const
{
    return E2D_RADIANS_TO_DEGREES(lim.max.radAngle_d);
}

void E2DEmitterSettings::SetGravity(E2Dvec2 v_)
{
    gravity = v_;
}
E2Dvec2 E2DEmitterSettings::Gravity() const
{
    return gravity;
}

void E2DEmitterSettings::SetOutOfViewAllowed(bool bool_)
{
    oov = bool_;
}

bool E2DEmitterSettings::OutOfViewAllowed() const
{
    return oov;
}

void E2DEmitterSettings::print() const
{
    E2D_PRINT("\nEmitterSettings:");
    E2D_PRINT("\nlifetime:[%f, %f]", lim.min.lifeTime, lim.max.lifeTime);
    E2D_PRINT("\nmorph color:%s", morphColor ? "true" : "false");
    E2D_PRINT("\ncolor_start:[(%u, %u, %u, %u), (%u, %u, %u, %u)]",
              lim.min.color_start.r, lim.min.color_start.g, lim.min.color_start.b, lim.min.color_start.a,
              lim.max.color_start.r, lim.max.color_start.g, lim.max.color_start.b, lim.max.color_start.a);
    //lim.min.color_start.Print("color_start min");
    //lim.max.color_start.Print("color_start max");
    E2D_PRINT("\ncolor_end:[(%u, %u, %u, %u), (%u, %u, %u, %u)]",
              lim.min.color_end.r, lim.min.color_end.g, lim.min.color_end.b, lim.min.color_end.a,
              lim.max.color_end.r, lim.max.color_end.g, lim.max.color_end.b, lim.max.color_end.a);
    //lim.min.color_end.Print("color_start min");
    //lim.max.color_end.Print("color_start max");
    
    E2D_PRINT("\nscale:%s", scale ? "true" : "false");
    E2D_PRINT("\nscale_start:[%f, %f]", lim.min.scale_start, lim.max.scale_start);
    E2D_PRINT("\nscale_end:[%f, %f]", lim.min.scale_end, lim.max.scale_end);
    
    /* mod limits */
    E2D_PRINT("\ntrans motion:%s", transMotion ? "true" : "false");
    E2D_PRINT("\nvel:[(%f, %f), (%f, %f)]", lim.min.vel.x, lim.min.vel.y, lim.max.vel.x, lim.max.vel.y);
    E2D_PRINT("\nvel_damping:[(%f, %f), (%f, %f)]", lim.min.vel_damping.x, lim.min.vel_damping.y, lim.max.vel_damping.x, lim.max.vel_damping.y);
    E2D_PRINT("\nvel_spread:[(%f, %f), (%f, %f)]", lim.min.vel_spread.x, lim.min.vel_spread.y, lim.max.vel_spread.x, lim.max.vel_spread.y);
    
    E2D_PRINT("\nrotational motion:%s", rotMotion ? "true" : "false");
    E2D_PRINT("\nangle:[%f, %f]", lim.min.angle, lim.max.angle);
    E2D_PRINT("\nangular vel:[%f, %f]", lim.min.aVel, lim.max.aVel);

    E2D_PRINT("\nradial motion:%s", radMotion ? "true" : "false");
    E2D_PRINT("\nradDist[%f, %f]", lim.min.radDist, lim.max.radDist);
    E2D_PRINT("\nradDist_limit:[%f, %f]", lim.min.radDist_limit, lim.max.radDist_limit);
    E2D_PRINT("\radDist_d:[%f, %f]", lim.min.radDist_d, lim.max.radDist_d);
    E2D_PRINT("\radDist_d_damping:[%f, %f]", lim.min.radDist_d_damping, lim.max.radDist_d_damping);
    E2D_PRINT("\nradAngle:[%f, %f]", lim.min.radAngle, lim.max.radAngle);
    E2D_PRINT("\nradAngle_d:[%f, %f]", lim.min.radAngle_d, lim.max.radAngle_d);
    E2D_PRINT("\nradAngle_d_damping:[%f, %f]", lim.min.radAngle_d_damping, lim.max.radAngle_d_damping);
    
    E2D_PRINT("\noov:%s", oov ? "true" : "false");
    E2D_PRINT("\nrbounds:(%s, %s, %s, %s)", rebound.bottom ? "true" : "false", rebound.right ? "true" : "false", rebound.top ? "true" : "false", rebound.left ? "true" : "false");
    
    E2D_PRINT("\ngravity:(%f, %f)", gravity.x, gravity.y);
}