//
//  E2DEnumerable.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 01/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_ENUMERABLE_H
#define E2D_ENUMERABLE_H

#include <iostream>
#include "E2D_Obj_.hpp"

class E2DEnumerable : public E2D_Obj_ {
    
protected:
    E2Dull_t        _count, _size, _index, _enum_pointer;           /* count of objects in collection, max capacity of collection, current index of pointer, enumeration pointer */
    
    E2DEnumerable(E2Dull_t size) : _count(0), _size(size), _index(0), _enum_pointer(0) { };
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_ENUMERABLE;}
    virtual ~E2DEnumerable() { };
    virtual void Consolidate(void) = 0;                             /* organize the array by removing null/invalid objects between elements */
    virtual void ResetEnumerator(void) { _enum_pointer = 0; };      /* reset the enumeration pointer to first index */
    virtual E2D_Obj_* NextObject(void) = 0;                         /* returns the next valid object */
    virtual bool NextObject(E2D_Obj_ **stor_) = 0;                  /* faster enumeration, but can retrieve invalid object. returns false, on end of array */
    E2Dull_t Count(void) const { return _count; };
};

#endif /* defined(__Echo2D_2__E2DEnumerable__) */
