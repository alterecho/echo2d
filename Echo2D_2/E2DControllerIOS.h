//
//  E2DControllerIOS.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_CONTROLLER_IOS_H
#define E2D_CONTROLLER_IOS_H

#import <UIKit/UIKit.h>             //EAGLContext
#import <QuartzCore/QuartzCore.h>   //CAEAGLLayer
#import "E2DGLView.h"

#include <iostream>
#include "E2DController.h"

class E2DControllerIOS : public E2DController {
    E2Dbool             retina;
    
    E2DGLView           *glView;
    EAGLContext         *context;
    CAEAGLLayer         *layer;
    //class E2DScene            *scene; //test
    
    
public:
    E2DControllerIOS();
    virtual ~E2DControllerIOS();
    
    E2DGLView *view();
    
    void PresentRenderBuffer() const;
    
    void update(E2Delapsed dt);
    
};

#endif /* defined(__Echo2D_2__E2DControllerIOS__) */
