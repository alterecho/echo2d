//
//  E2DArray.h
//  E2DDerivedStructs
//
//  Created by Vijay Chandran J on 04/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_ARRAY_H
#define E2D_ARRAY_H

#include <iostream>
#include "E2DEnumerable.h"

#define _E2D_OBJ_COUNT      100

class E2DArray : public E2DEnumerable {
    
    E2D_Obj_            **_objects;
    E2Dull_t            _first, _last;
    
    void resize(char op_);
    void removeObject(E2Dull_t  indx_);
    
    static inline void initializeArray(E2D_Obj_ ***arr_, E2Dull_t a, E2Dull_t sz_);
    
    void print(const char *) const;
    
    virtual ~E2DArray();
    
public:
    E2DArray(void);
    E2DArray(E2Dull_t);
    
    E2Dull_t Size(void) const;      /* current capacity of array */
    E2Dull_t Count(void) const;     /* number of objects in array */
    
    //getter
    E2D_Obj_* ObjectAtIndex(E2Dull_t) const;
    E2Dull_t IndexOfObject(E2D_Obj_ *) const;
    bool ContainsObject(E2D_Obj_ *) const;
    
    
    //add/insert objects
    void AddObject(E2D_Obj_ *);
    void SwapObjects(E2Dull_t, E2Dull_t);
    void SwapObjectAtIndex(E2Dull_t index1, E2D_Obj_ *index2);
    
    //remove objects
    void RemoveObject(E2D_Obj_ *);
    void RemoveObjectAtIndex(E2Dull_t);
    void RemoveLastObject(void);
    void RemoveAllObjects(void);
    
    //enumeration
    void Consolidate(void);                 /* organize the array by removing null/invalid objects between elements */
    E2D_Obj_* NextObject(void);             /* returns the next valid object. returns null on end of array */
    bool NextObject(E2D_Obj_ **stor_);      /* faster enumeration, but can retrieve invalid object. returns false, on end of array */
};




#pragma mark - private -

inline
void E2DArray::resize(char op_)
{
    E2D_SYS_PRINT("resizing...");
    _size *= 2 * op_;
    
    E2D_Obj_ **temp = (E2D_Obj_**)realloc(_objects, sizeof(E2D_Obj_ *) * (unsigned long)_size);
    if (!temp)
    {
        E2D_SYS_PRINT("error resizing array");
        free(_objects);
        exit(1);
    }
    this->initializeArray(&temp, _size / 2, _size);
    _objects = temp;
    
}

inline
void E2DArray::removeObject(E2Dull_t indx_)
{
    if (_objects[indx_])
    {
        _objects[indx_]->Release();
        _objects[indx_] = nullptr;
        _count--;
    }
}

inline
void E2DArray::initializeArray(E2D_Obj_ ***arr_, E2Dull_t a, E2Dull_t sz_)
{
    E2D_Obj_** obj_p = *arr_;
    
    for (E2Dull_t i = a; i < sz_; i++)
    {
        obj_p[i] = nullptr;
    }
}

#endif /* defined(__E2DDerivedStructs__E2DArray__) */
