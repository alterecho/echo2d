//
//  E2DParticle.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DParticle.h"
#include "E2DTextureManager.h"
#include "E2DEmitter.h"

E2DParticle::E2DParticle(const char* spriteFile_, E2DpRenderAttribs *pra_) : E2DDrawable(0)
{
    this->setClassName("E2DParticle");
    this->setClassType(E2D_CLASS_PARTICLE);

    pra = pra_;
    att.setPrAttribs(pra_);
    
    E2DTexture *t = E2DTextureManager::SharedTextureManager()->CacheTexture(spriteFile_);
    _textureID = t->ID();
    
    pSize = e2d::greaterValue(t->Size().width, t->Size().height);
    
    active = false;
    lifeTime = 0;
    emitter = nullptr;
    mod.Initialize();

}

E2DParticle::~E2DParticle()
{
    
}

void E2DParticle::update(E2Delapsed dt_)
{
    
    att.add(mod, emitter->scale, emitter->fade, emitter->morphColor, emitter->radMotion);
    mod.addGravity(emitter->gravity);
    
    //att.add(<#E2DparticleMod &m_#>, <#bool scale_#>, <#bool fade_#>, <#bool morph_#>, <#bool radMotion_#>)
    //att.pos.Print();
    //if (!emitter->radMotion)
    {
        //E2Dradians a = emitter->angle;
        //E2Dpoint p = pra->pos;
        //att.pra->pos(p.x * cosf(a) - p.y * sinf(a), p.x * sinf(a) + p.y * cosf(a));
    }
    //att.pos += E2Dpoint(1.0f, 1.0f)
    lifeTime += dt_;
    if (lifeTime >= mod.lifeTime)
    {
        lifeTime = 0.0f;
        this->reset();
    }
    
}

inline
void E2DParticle::reset()
{
    active = false;
    //emitter->reset(this);
    att.pra->color(0, 0, 0, 0);
    emitter->particleDeactivated();
}

void E2DParticle::setAttributes(const E2DparticleAttribs& att_, const E2DparticleMod& mod_)
{
    att = att_;
    att.pra = pra;
    mod = mod_;
    if (emitter->scale)
    {
        mod.calculatescaleMod(att.pra->size);
    }
    if (emitter->morphColor)
    {
        mod.calculateColorMod(att.pra->color);
    }
    if (emitter->fade)
    {
        //att.pra->color.a = mod.alpha_s;
        mod.calculateAlphaMod(att.pra->color.a);
    }
    
}

void E2DParticle::reflectEmitterStates()
{
    if (emitter->scale)
    {
        mod.calculatescaleMod(att.pra->size);
    }
    if (emitter->morphColor)
    {
        mod.calculateColorMod(att.pra->color);
    }
    if (emitter->fade)
    {
        //att.pra->color.a = mod.alpha_s;
        mod.calculateAlphaMod(att.pra->color.a);
    }
    
}

void E2DParticle::randomize(const E2DpLimits &lim_)
{
    att.randomize(lim_);
    mod.randomize(lim_);
}