//
//  E2D_OBJECT_.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 15/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D__OBJ__H
#define E2D__OBJ__H

#include "E2DConfig.h"
#include "E2DMacros.h"
#include "E2DDataTypes.h"
#include "E2DFunctions.h"
#include "E2DProtocols.h"

//#define E2D_CLASSNAME   typeid(*this).name()
#define E2D_CLASSNAME   ClassName()

#define E2D_NO_PRINT \
virtual inline void print(const char *msg_) const { \
    E2D_CLASS_PRINT("NOT PRINTABLE"); \
} \

class E2D_Obj_ {
    
    friend class E2DMemoryManager;
    
    E2Dul_t         __ref_count;
    char            *__className;
    bool            __autoRelease;
    E2Dclass_t      __classType;
    
    
    inline void checkRef()
    {
        //E2D_SYS_PRINT("released. ref_count:%lu", _ref_count);
        if (__ref_count <= 0)
        {
            //E2D_SYS_PRINT("destroying...");
            delete this;
        }
    };
    
    
protected:
    
    void setClassName(const char *s_)
    {
        E2D_FREE(__className);
        __className = (char *)malloc(strlen(s_) + 1);
        strcpy(__className, s_);
    }
    
    void setClassType(E2Dclass_t t_)
    {
        __classType = t_;
    }
    
    virtual void print(const char *msg_) const = 0;
    
public:
    
    static inline E2Dclass_t Class(void) { return E2D_CLASS_OBJ; };
    
    static inline void initializeArray(E2D_Obj_ **obj_, E2Dul_t size_, E2D_Obj_ *val_)
    {
        for (E2Dul_t i = 0; i < size_; i++)
        {
            obj_[i] = val_;
        }
    }
    
    E2D_Obj_()
    {
        __className = nullptr;
        this->setClassName("E2D_Obj_");
        __ref_count = 0;
        __ref_count++;
        
        //E2D_BL;
        //E2D_SYS_PRINT("object created");
        __autoRelease = false;
        __classType = E2D_CLASS_OBJ;
    };
    
    virtual ~E2D_Obj_()
    {
        //E2D_SYS_PRINT("destroyed");
        E2D_FREE(__className);
        
    };
    
    
    virtual inline void Print(void) const {
        this->Print("printing...");
    };
    
    virtual inline void Print(const char *str_) const {
        E2D_OBJ_PRINT_START("%s", str_);
        this->print(str_);
        E2D_OBJ_PRINT_END;
        E2D_BL;
    };
    
    
    E2D_Obj_* Retain()
    {
        __ref_count++;
        return this;
    };
    
    void Release(void)
    {
        __ref_count--;
        checkRef();
    };
    
    E2Dul_t RetainCount() const
    {
        return __ref_count;
    };
    
    const char* ClassName(void) const
    {
        return __className;
    }
    
    E2Dclass_t ClassType(void)
    {
        return __classType;
    }
    
    void PrintClassName(void) const
    {
        E2D_PRINT("\nCLASS:%s", __className);
    }
    
};

//typedef E2D_Obj_ * E2DObj;


/*
void E2D_Obj_::setClassName(const char *s_)
{
    E2D_FREE(_className);
    
    const char *s = typeid(*this).name();
    _className = (char *)malloc(strlen(s) + 1);
    strcpy(_className, s);
}
//*/



#endif
