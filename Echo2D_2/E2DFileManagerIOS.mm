//
//  E2DFileManagerIOS.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DFileManagerIOS.h"

void E2DFileManagerIOS::PathForFile_alc(const char *file_, char **pathBuf_) const
{
    NSString *file__ = [NSString stringWithUTF8String:file_];
    NSString *ret = [[NSBundle mainBundle] pathForResource:[file__ stringByDeletingPathExtension] ofType:[file__ pathExtension]];
    
    E2D_SYS_ASSERT(ret, "unable to locate file '%s'", file_);
    size_t l = strlen([ret UTF8String]);
    E2D_FREE(*pathBuf_);
    *pathBuf_ = (char *)malloc(sizeof(char) * l + 1);
    strcpy(*pathBuf_, [ret UTF8String]);
}

void E2DFileManagerIOS::StringFromContentsOfFile(const char *file_, char **buf_) const
{
    char *pathBuf = nullptr;
    PathForFile_alc(file_, &pathBuf);
    std::ifstream f(pathBuf);
    E2D_FREE(pathBuf);
    
    E2D_ASSERT(f.is_open(), "unable to open file:%s", file_);
    
    char  ch, char_buf[2] = {'0', '\0'};
    while (f.get(ch))
    {
        char_buf[0] = ch;
        strcat(*buf_, char_buf);
    }
    
    f.close();
}