//
//  E2DLayer.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 01/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_LAYERS_H
#define E2D_LAYERS_H

#include <iostream>
#include "E2DGeometry.hpp"
#include "E2DArray.h"
#include "E2DDataStructs.hpp"

class E2DDrawable;
class E2DLayer : public E2DGeometry, public e2d::protocol::TouchEvents {
    friend class E2DScene;
    friend class E2DRendererES2;
    friend class E2DDrawable;
    
    E2Ddepth                __depth;
    
    E2DArray                *__drawables, *__emitters;
    class E2DRenderer       *__renderer;
    
    E2Dsize                 __screenSize;
        
    virtual void print(const char *) const;
    
    void sortDrawables();
    void render(E2Delapsed dt_);
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_LAYER; }
    E2DLayer(E2Ddepth depth_);
    virtual ~E2DLayer();
    
    E2Ddepth Depth() const;
    
    void SetScale(E2Dclampf);   //overriding base
    
    void Update(E2Delapsed dt_) { };        /* user updates */
    
    void AddDrawable(E2DDrawable *);
    void RemoveDrawable(E2DDrawable *);
    
    virtual bool touchBegan(E2Dpoint);
    virtual void touchMoved(E2Dpoint);
    virtual void touchEnded(E2Dpoint);
};

#endif /* defined(__Echo2D_2__E2DLayer__) */
