//
//  E2DRendererES2EXT.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 13/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_RENDERER_ES_2_EXT_H
#define E2D_RENDERER_ES_2_EXT_H

#include <iostream>
#include "E2D_Obj_.hpp"
#include "E2DFileManager.h"

#define __ATTRIB_POSITION     "position"
#define __ATTRIB_COLOR        "color"
#define __ATTRIB_TEX_COORD    "texCoord"
#define __ATTRIB_POINT_SIZE   "pointSize"

#define __UNIFORM_PROJECTION  "projection"
#define __UNIFORM_MODEL_VIEW  "modelView"
#define __UNIFORM_TEXTURE     "texture"

#include "E2DDataStructs.hpp"

typedef struct {
    GLint pos, col, texc, tex, psize;
} E2DvtxAttribs;

typedef struct {
    GLint projection, modelView;
} E2Duniforms;

class E2DRendererES2EXT : public E2D_Obj_ {
    friend class E2DRendererES2;
    
    E2DvtxAttribs   &__att;
    E2Duniforms     &__unif;
    
    E2Dsize         __screenSize;
    E2Duint         &__program;
    
    E2DRendererES2EXT(E2DvtxAttribs* a_, E2Duniforms* u_, GLuint *p_, E2Dsize sSize_) : __att(*a_), __unif(*u_), __program(*p_), __screenSize(sSize_)
    {
        __att.pos = -2;
        __att.col = -2;
        __att.tex = -2;
        __att.texc = -2;
        __att.psize = -2;
        
        __unif.projection = -2;
        __unif.modelView = -2;
    };
    
    ~E2DRendererES2EXT() { };
    E2D_NO_PRINT;
    
    GLuint newShader(const char *source_, GLenum type_);
    GLuint newProgram(GLuint vShader_, GLuint fShader_);
    void useProgram(GLuint);
    void setupProgram(GLuint);
    void setupProjection(GLfloat left_, GLfloat right_, GLfloat bottom_, GLfloat top_,  GLfloat near_, GLfloat far_, GLint program_);
    inline void printUniform(const GLchar *name_, GLuint program_, GLuint count_, GLuint break_, const char *msg_);
    
};

GLuint E2DRendererES2EXT::newShader(const char *source_, GLenum type_)
{
    GLuint ret;
    
    ret = glCreateShader(type_);
    
    char *buffer = (char *)malloc(sizeof(char) * 2048);
    memset(buffer, 0, 2048);
    E2DFileManager::SharedFileManager()->StringFromContentsOfFile(source_, &buffer);
    
    glShaderSource(ret, 1, &buffer, NULL);
    glCompileShader(ret);
    
    //E2D_PRINT("\nshader %d:\n%s", type_, buffer);
    
    E2D_FREE(buffer);
    
    GLint status;
    glGetShaderiv(ret, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE)
    {
        //memset(buffer, '\0', 256);
        char buf[256] = "";
        glGetShaderInfoLog(ret, sizeof(buf), NULL, buf);
        E2D_EXIT("Error compiling shader type:%X\nmsg:%s",  type_, buf);
    }
    
    return ret;
}

GLuint E2DRendererES2EXT::newProgram(GLuint vShader_, GLuint fShader_)
{
    GLuint ret;
    
    ret = glCreateProgram();
    glAttachShader(ret, vShader_);
    glAttachShader(ret, fShader_);
    glLinkProgram(ret);
    
    GLint status;
    glGetProgramiv(ret, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        char buf[256];
        glGetProgramInfoLog(ret, sizeof(buf), NULL, buf);
        E2D_EXIT("Error linking program\nmsg:%s", buf);
    }
    
    return ret;
}

inline
void E2DRendererES2EXT::setupProgram(GLuint p_)
{
    GLint prog;
    glGetIntegerv(GL_CURRENT_PROGRAM, &prog);
    
    glUseProgram(p_);
    setupProjection(-__screenSize.width * 0.5f, __screenSize.width * 0.5f,
                    -__screenSize.height * 0.5f, __screenSize.height * 0.5f,
                    -100.0f, 100.0f,
                    p_);
    
    glUseProgram(prog);
}

inline
void E2DRendererES2EXT::useProgram(GLuint p_)
{
    glUseProgram(p_);
    
    __unif.modelView = glGetUniformLocation(p_, __UNIFORM_MODEL_VIEW);
    __att.pos = glGetAttribLocation(p_, __ATTRIB_POSITION);
    __att.col = glGetAttribLocation(p_, __ATTRIB_COLOR);
    __att.texc = glGetAttribLocation(p_, __ATTRIB_TEX_COORD);
    //__atribs.tex = glGetAttribLocation(program, __UNIFORM_TEXTURE);
    __att.psize = glGetAttribLocation(p_, __ATTRIB_POINT_SIZE);
    
    __unif.modelView = glGetUniformLocation(p_, __UNIFORM_MODEL_VIEW);
    
    //program = p_;
    
}

void E2DRendererES2EXT::setupProjection(GLfloat left_, GLfloat right_, GLfloat bottom_, GLfloat top_,  GLfloat near_, GLfloat far_, GLint program_)
{
    E2D_PRINT("\nsetting up orhogonal projection:\n left:%f, right:%f, bottom:%f, top:%f, near:%f, far:%f", left_, right_, bottom_, top_, near_, far_);
    E2Dmat4 ortho;
    ortho.SetOrtho(left_, right_, bottom_, top_, near_, far_);
    
    GLint projection = glGetUniformLocation(program_, __UNIFORM_PROJECTION);
    glUniformMatrix4fv(projection, 1, 0, ortho.Pointer());
}

#endif /* defined(__Echo2D_2__E2DRendererES2EXT__) */
