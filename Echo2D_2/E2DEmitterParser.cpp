//
//  E2DEmitterParser.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 21/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DEmitter.h"
#include "E2DFileManager.h"
#include <fstream>

void E2DEmitter::seekProperty(const char *p_, std::ifstream& stream_)
{
    char
    buf_c[2] = {'\0'},
    buf[256] = {'\0'};
    stream_.seekg(std::ios::beg);
    while (stream_.good())
    {
        buf_c[0] = stream_.get();
        if (buf_c[0] == '>')
        {
            buf_c[0] = stream_.get();
            buf_c[0] = stream_.get();
            while (buf_c[0] != ' ' && buf_c[0] != '\n')
            {
                strcat(buf, buf_c);
                buf_c[0] = stream_.get();
            }
            if (strcmp(p_, buf) == 0)
            {
                return;
            }
            buf[0] = '\0';
        }
    }
    
    stream_.close();
    E2D_EXIT("property '%s' not found", p_);
}

void E2DEmitter::LoadConfig(const char *file_)
{
    E2D_PRINT("\nreading config '%s", file_);
    char *path = NULL;
    E2DFileManager::SharedFileManager()->PathForFile_alc(file_, &path);
    std::ifstream stream(path);
    E2D_FREE(path);
    
    this->seekProperty("particles_max", stream);
    stream >> activeParticles_MAX;
   
    
    this->seekProperty("lifetime_min", stream);
    stream >> lim.min.lifeTime;
    this->seekProperty("lifetime_max", stream);
    stream >> lim.max.lifeTime;
    
    
    this->seekProperty("color_morph", stream);
    stream >> morphColor;
    this->seekProperty("color_min", stream);
    lim.min.color_start.Read(stream);
    this->seekProperty("color_max", stream);
    lim.max.color_start.Read(stream);
    
    this->seekProperty("fade", stream);
    stream >> fade;
    this->seekProperty("opacity_min", stream);
    stream >> lim.min.color_start.a;
    this->seekProperty("opacity_max", stream);
    stream >> lim.max.color_start.a;
    this->seekProperty("opacity_end_min", stream);
    stream >> lim.min.color_end.a;
    this->seekProperty("opacity_end_max", stream);
    stream >> lim.max.color_end.a;
    
    
    this->seekProperty("scale", stream);
    stream >> scale;
    this->seekProperty("scale_start_min", stream);
    stream >> lim.min.scale_start;
    this->seekProperty("scale_start_max", stream);
    stream >> lim.max.scale_start;
    this->seekProperty("scale_end_min", stream);
    stream >> lim.min.scale_end;
    this->seekProperty("scale_end_max", stream);
    stream >>lim.max.scale_end;
    
    
    this->seekProperty("translational", stream);
    stream >> transMotion;
    this->seekProperty("velocity_min", stream);
    lim.min.vel.Read(stream);
    this->seekProperty("velocity_max", stream);
    lim.max.vel.Read(stream);
    this->seekProperty("velocity_damping_min", stream);
    lim.min.vel_damping.Read(stream);
    this->seekProperty("velocity_damping_max", stream);
    lim.max.vel_damping.Read(stream);
    this->seekProperty("velocity_spread_min", stream);
    lim.min.vel_spread.Read(stream);
    this->seekProperty("velocity_spread_max", stream);
    lim.max.vel_spread.Read(stream);
    
    
    this->seekProperty("spin", stream);
    stream >> rotMotion;
    this->seekProperty("angle_min", stream);
    stream >> lim.min.angle;
    this->seekProperty("angle_max", stream);
    stream >> lim.max.angle;
    this->seekProperty("angular_velocity_min", stream);
    stream >> lim.min.aVel;
    this->seekProperty("angular_velocity_max", stream);
    stream >> lim.max.aVel;
    
    this->seekProperty("radialmotion", stream);
    stream >> radMotion;
    this->seekProperty("radiusFC_min", stream);
    stream >> lim.min.radDist;
    this->seekProperty("radiusFC_max", stream);
    stream >> lim.max.radDist;
    this->seekProperty("radiusFC_limit_min", stream);
    stream >> lim.min.radDist_limit;
    this->seekProperty("radiusFC_limit_max", stream);
    stream >> lim.max.radDist_limit;
    this->seekProperty("radiusFC_delta_min", stream);
    stream >> lim.min.radDist_d;
    this->seekProperty("radiusFC_delta_max", stream);
    stream >> lim.max.radDist_d;
    this->seekProperty("radiusFC_delta_damping_min", stream);
    stream >> lim.min.radDist_d_damping;
    this->seekProperty("radiusFC_delta_damping_max", stream);
    stream >> lim.max.radDist_d_damping;
    this->seekProperty("angleFC_min", stream);
    stream >> lim.min.radAngle;
    this->seekProperty("angleFC_max", stream);
    stream >> lim.max.radAngle;
    this->seekProperty("angleFC_delta_min", stream);
    stream >> lim.min.radAngle_d;
    this->seekProperty("angleFC_delta_max", stream);
    stream >> lim.max.radAngle_d;
    this->seekProperty("angleFC_delta_damping_min", stream);
    stream >> lim.min.radAngle_d_damping;
    this->seekProperty("angleFC_delta_damping_max", stream);
    stream >> lim.max.radAngle_d_damping;
    
    
    this->seekProperty("gravity", stream);
    gravity.Read(stream);
    
    
    this->seekProperty("oov", stream);
    stream >> oov;
    this->seekProperty("rebound", stream);
    stream >> rebound.bottom;
    stream >> rebound.right;
    stream >> rebound.top;
    stream >> rebound.left;
    
    stream.close();
    
    this->PrintConfig();
    
}

void E2DEmitter::PrintConfig() const
{
    E2D_PRINT("\npCount:%u", pCount);
    
    E2D_PRINT("\nlifetime_min:%f", lim.min.lifeTime);
    E2D_PRINT("\nlifetime_max:%f", lim.max.lifeTime);
    
    E2D_PRINT("\ncolor_morph:%s", morphColor ? "true" : "false");
    lim.min.color_start.Print("color_min:");
    lim.max.color_start.Print("color_max:");
    
    E2D_PRINT("\nfade:%s", fade ? "true" : "false");
    E2D_PRINT("\nopacity_min:%u", lim.min.color_start.a);
    E2D_PRINT("\nopacity_max:%u", lim.max.color_start.a);
    E2D_PRINT("\nopacity_end_min:%u", lim.min.color_end.a);
    E2D_PRINT("\nopacity_end_max:%u", lim.max.color_end.a);
    
    E2D_PRINT("\nscale:%s", scale ? "true" : "false");
    E2D_PRINT("\nscale_start_min:%f", lim.min.scale_start);
    E2D_PRINT("\nscale_start_max:%f", lim.max.scale_start);
    E2D_PRINT("\nscale_end_min:%f", lim.min.scale_end);
    E2D_PRINT("\nscale_end_max:%f", lim.max.scale_end);
    
    E2D_PRINT("\ntranslational:%s", transMotion ? "true" : "false");
    lim.min.vel.Print("velocity_min");
    lim.max.vel.Print("velocity_max");
    lim.min.vel_damping.Print("velocity_damping_min");
    lim.max.vel_damping.Print("velocity_damping_max");
    lim.min.vel_spread.Print("velocity_spread_min");
    lim.max.vel_spread.Print("velocity_spread_max");
    
    E2D_PRINT("\nspin:%s", rotMotion ? "true" : "false");
    E2D_PRINT("\nangle_min:%f", lim.min.angle);
    E2D_PRINT("\nangle_max:%f", lim.max.angle);
    E2D_PRINT("\nangular_velocity_min:%f", lim.min.aVel);
    E2D_PRINT("\nangular_velocity_max:%f", lim.max.aVel);
    
    E2D_PRINT("\nradialmotion:%s", radMotion ? "true" : "false");
    E2D_PRINT("\nradiusFC_min:%f", lim.min.radDist);
    E2D_PRINT("\nradiusFC_max:%f", lim.max.radDist);
    E2D_PRINT("\nradiusFC_limit_min:%f", lim.min.radDist_limit);
    E2D_PRINT("\nradiusFC_limit_max:%f", lim.max.radDist_limit);
    E2D_PRINT("\nradiusFC_delta_min:%f", lim.min.radDist_d);
    E2D_PRINT("\nradiusFC_delta_max:%f", lim.max.radDist_d);
    E2D_PRINT("\nradiusFC_delta_damping_min:%f", lim.min.radDist_d_damping);
    E2D_PRINT("\nradiusFC_delta_damping_max:%f", lim.max.radDist_d_damping);
    E2D_PRINT("\nangleFC_min:%f", lim.min.radAngle);
    E2D_PRINT("\nangleFC_max:%f", lim.max.radAngle);
    E2D_PRINT("\nangleFC_delta_min:%f", lim.min.radAngle_d);
    E2D_PRINT("\nangleFC_delta_max:%f", lim.max.radAngle_d);
    E2D_PRINT("\nangleFC_delta_damping_min:%f", lim.min.radAngle_d_damping);
    E2D_PRINT("\nangleFC_delta_damping_max:%f", lim.max.radAngle_d_damping);
    
    gravity.Print("gravity:");
    
    E2D_PRINT("\noov:%s", oov ? "true" : "false");
    E2D_PRINT("\nrebound:%s, %s, %s, %s", E2D_BOOL_STRING(rebound.bottom), E2D_BOOL_STRING(rebound.right), E2D_BOOL_STRING(rebound.top), E2D_BOOL_STRING(rebound.left));
    
}