//
//  E2DDataStructs.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 24/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_DATA_STRUCTS_H
#define E2D_DATA_STRUCTS_H

#include "E2DDataTypes.h"

#include "E2DMacros.h"
#include "E2Dvec2.hpp"
#include "E2Dvec3.hpp"
#include "E2DSize.hpp"
#include "E2DMatrix.hpp"
//#include "E2DString.h"
//#include "E2DNumber.h"
//#include "E2DArray.h"
//#include "E2DDictionary.h"

template <typename T>
void e2dPrintArray(const T *v, size_t s, unsigned int break_, const char *msg_)
{
    char msg[256];
    strcpy(msg, msg_);
    if (!msg)
    {
        strcpy(msg, "");
    }
    E2D_PRINT("\nprinting buffer %s of size %lu...", msg, s);
    for (int i = 0; i < s; i++)
    {
        if (i % break_ == 0)
        {
            std::cout << std::endl;
        }
        std::cout << v[i] << "\t\t";
    }
}

#pragma mark -
#pragma mark E2Dvec2
#pragma mark -
typedef E2Dvec2   E2Dpoint;

#pragma mark -
#pragma mark E2DSize
#pragma mark -


#pragma mark -
#pragma mark E2Dcolor
#pragma mark -

#define E2D_COLOR_RED(a)       E2Dcolor(255, 0, 0, a)
#define E2D_COLOR_GREEN(a)     E2Dcolor(0, 255, 0, a)
#define E2D_COLOR_BLUE(a)      E2Dcolor(0, 0, 255, a)
#define E2D_COLOR_WHITE(a)     E2Dcolor(255, 255, 255, a)
#define E2D_COLOR_BLACK(a)     E2Dcolor(0, 0, 0, a)

struct E2Dcolor {
    static inline E2Dclass_t Class(void) { return E2D_CLASS_COLOR; }
    E2Dubyte    r, g ,b, a;
    
    E2Dcolor(void) : r(0), g(0), b(0), a(0) { };
    E2Dcolor(E2Dubyte c_) : r(c_), g(c_), b(c_), a(c_) { };
    E2Dcolor(E2Dubyte c_, E2Dubyte a_) : r(c_), g(c_), b(c_), a(a_) { };
    E2Dcolor(E2Dubyte r_, E2Dubyte g_, E2Dubyte b_, E2Dubyte a_) : r(r_), g(g_), b(b_), a(a_) { }
    E2Dcolor(const E2Dcolor& color_) : r(color_.r), g(color_.g), b(color_.b), a(color_.a) { }
    
    inline void Print(void) const
    {
        this->Print("color:");
    }
    
    inline void Print(const char *msg_) const
    {
        E2D_PRINT("\n%s (%u, %u %u, %u)", msg_, r, g, b, a);
    }
    
    inline void operator()(E2Dubyte r_, E2Dubyte g_, E2Dubyte b_, E2Dubyte a_)
    {
        r = r_,
        g = g_,
        b = b_,
        a = a_;
    }
    
    friend inline std::ostream& operator<<(std::ostream& stream_, const E2Dcolor& rhs_)
    {
        stream_ << "(" << rhs_.r << ", " << rhs_.g << ", " << rhs_.b << ", " << rhs_.a << ")";
        return stream_;
    }
    
    
    inline E2Dcolor operator+(const E2Dcolor& color_) const
    {
        E2Dcolor ret;
        
        ret.r = r + color_.r;
        ret.g = g + color_.g;
        ret.b = b + color_.b;
        ret.a = a + color_.a;
        
        return ret;
    }
    
    inline E2Dcolor operator-(const E2Dcolor& color_) const
    {
        E2Dcolor ret;
        
        ret.r = r - color_.r;
        ret.g = g -color_.g;
        ret.b = b - color_.b;
        ret.a = a - color_.a;
        
        return ret;
    }
    
    inline E2Dcolor operator*(const E2Dcolor& c_) const
    {
        E2Dcolor ret;
        
        ret.r = r * c_.r;
        ret.g = g * c_.g;
        ret.b = b * c_.b;
        ret.a = a * c_.a;
        
        return ret;
    }
    
    inline E2Dcolor operator*(const float& f_) const
    {
        E2Dcolor ret;
        
        ret.r = r * f_;
        ret.g = g * f_;
        ret.b = b * f_;
        ret.a = a * f_;
        
        return ret;
    }
    
    
    friend inline E2Dcolor operator*(const float& f_, const E2Dcolor& col)
    {
        E2Dcolor ret;
        
        ret.r = col.r * f_;
        ret.g = col.g * f_;
        ret.b = col.b * f_;
        ret.a = col.a * f_;
        
        return ret;
    }
    
    inline void operator+=(const E2Dcolor& c_)
    {
        r += c_.r;
        g += c_.g;
        b += c_.b;
        a += c_.a;
    }
    
    inline
    const E2Dubyte* Pointer() const
    {
        return &r;
    }
    
    inline
    E2Dubyte* MutablePointer()
    {
        return &r;
    }
    
    inline void Read(std::ifstream& stream_)    /* reads in color from file. file pointer not reset */
    {
        //[rem]
        float f;
        stream_ >> f;
        r = (E2Dubyte)f;
        stream_ >> f;
        g = (E2Dubyte)f;
        stream_ >> f;
        b = (E2Dubyte)f;
        stream_ >> f;
        a = (E2Dubyte)f;
        
        //printf("\nrgba:%u, %u, %u, %u", r, g, b, a);
    }
    
};

/*
 static inline E2Dsize e2dSizeMake(float width, float height)
 {
 E2Dsize ret;
 
 ret.width = width;
 ret.height = height;
 
 return ret;
 }
 //*/

#pragma mark -
#pragma mark E2DRange
#pragma mark -
template <typename T>
class E2DRange {
    T min, max;
};


#pragma mark -
#pragma mark E2Dmat4
#pragma mark -
typedef E2Dmatrix<4>    E2Dmat4;

#pragma mark -
#pragma mark E2Dvertex
#pragma mark -
/*
struct E2Dvertex {
    E2Dpoint    position;
    E2Dcolor    color;
    E2Dvec2   texCoord;
    
public:
    E2Dvertex() { };
    E2Dvertex(E2Dpoint pos_, E2Dcolor col_, E2Dvec2 texCoord_) : position(pos_), color(col_), texCoord(texCoord_) { };
};
//*/

extern "C" {
    
#pragma mark -
#pragma mark E2DtexCoord
#pragma mark -
    typedef struct {
        E2Dfloat    s, t;
    } E2DtexCoord;
    
    static inline E2DtexCoord e2dTexCoordMake(const float s, const float t)
    {
        E2DtexCoord ret;
        ret.s = s;
        ret.t = t;
        
        return ret;
    }
    
    static inline void e2dTexCoordPrint(const E2DtexCoord *tc_)
    {
        E2D_PRINT("\ntex coord:(%f, %f)", tc_->s, tc_->t);
    }
    
    static inline void e2dTexCoordPrintS(const E2DtexCoord *tc_, const char *tag_)
    {
        E2D_PRINT("\n%s:(%f, %f)", tag_, tc_->s, tc_->t);
    }
    
#pragma mark -
#pragma mark E2Dvertex
#pragma mark -
    typedef struct {
        E2Dpoint        position;
        E2Dcolor        color;
        E2DtexCoord     texCoord;
    } E2Dvertex;
    
   
    
    static inline E2Dvertex e2dVertexMake(E2Dpoint p, E2Dcolor c, E2Dfloat s, E2Dfloat t)
    {
        E2Dvertex ret;
        
        ret.position = p;
        ret.color = c;
        ret.texCoord.s = s,
        ret.texCoord.t = t;
        
        return ret;
    }
    
    static inline void e2dVertexReset(E2Dvertex *v)
    {
        v->position(0.0f, 0.0f);
        v->color(255, 255, 255, 255);
        v->texCoord.s = 0.0f,
        v->texCoord.t = 0.0f;
    }
    
    static inline void e2dVertexPrint(E2Dvertex v_)
    {
        E2D_PRINT("\nvertex - pos:(%f, %f), col:(%u, %u, %u, %u), texCoord:(%f, %f)", v_.position.x, v_.position.y, v_.color.r, v_.color.g, v_.color.b, v_.color.a, v_.texCoord.s, v_.texCoord.t);
    }
    
    static inline void e2dVertexPrint(E2Dvertex v_, const char *tag_)
    {
        E2D_PRINT("\n%s - pos:(%f, %f), col:(%u, %u, %u, %u), texCoord:(%f, %f)", tag_, v_.position.x, v_.position.y, v_.color.r, v_.color.g, v_.color.b, v_.color.a, v_.texCoord.s, v_.texCoord.t);
    }
    
    /*
    static inline void e2dVertexPrint(E2Dvertex v_, const char *tag_, ...)
    {
        E2D_PRINT("\n%s - pos:(%f, %f), col:(%f, %f, %f, %f), texCoord:(%f, %f)", tag_, v_.position.x, v_.position.y, v_.color.r, v_.color.g, v_.color.b, v_.color.a, v_.texCoord.s, v_.texCoord.t, ##__VA_ARGS__);
    }
     */
    
#pragma mark -
#pragma mark E2Dpolygon
#pragma mark -
    typedef struct {
        E2Dvertex v[4];
    } E2Dpolygon;
    
    static inline void e2dPolygonSetSize(float width, float height, E2Dpolygon *p)
    {
        p->v[0].position(-width * 0.5f, height * 0.5f);
        p->v[1].position(width * 0.5f, height * 0.5f);
        p->v[2].position(-width * 0.5f, -height * 0.5f);
        p->v[3].position(width * 0.5f, -height * 0.5f);
    }
    
    static inline void e2dPolygonPrint(E2Dpolygon *p)
    {
        E2D_PRINT("\n\nprinting polygon...")
        for(char i = 0; i < 4; i++)
        {
             E2D_PRINT("\n[%d]:\npos:(%f, %f), (%p, %p)", i, p->v[i].position.x, p->v[i].position.y, &p->v[i].position.x, &p->v[i].position.y );
        }
    }
    
    static inline void e2dPolygonPrint(E2Dpolygon *p, const char *msg_)
    {
        E2D_PRINT("\n\n%s", msg_);
        for(char i = 0; i < 4; i++)
        {
            E2D_PRINT("\n[%d]:\npos:(%f, %f), (%p, %p)", i, p->v[i].position.x, p->v[i].position.y, &p->v[i].position.x, &p->v[i].position.y );
        }
    }
    
}
#endif
