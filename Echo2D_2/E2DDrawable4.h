//
//  E2DDrawable4.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_DRAWABLE_4_H
#define E2D_DRAWABLE_4_H

#include <iostream>
#include "E2DDrawable.h"

class E2DDrawable4 : public E2DDrawable {
    friend class E2DTextureManager;
    friend class E2DRendererES2;
    
protected:
    E2Dvertex           __v[4], __v_orig[4];
    
    E2DDrawable4(E2Ddepth depth_);
    virtual void setTexCoord(const E2DtexCoord[]);
    virtual void render();
    
    virtual void print(const char *) const;
    void copyVertices(const E2DDrawable4 *);
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_DRAWABLE4; }
    virtual ~E2DDrawable4();
    
    virtual void setSize(float width_, float height_) = 0;
    
    const E2Dvertex* Vertices() const;
    void SetColor(const E2Dcolor color_);
    
    
    
};

#endif /* defined(__Echo2D_2__E2DDrawable4__) */
