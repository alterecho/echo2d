//
//  E2DRendererES2EXT.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 13/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_RENDERER_ES_2_EXT_H
#define E2D_RENDERER_ES_2_EXT_H

#include <iostream>
#include "E2D_Obj_.hpp"

class E2DRendererES2EXT : public E2D_Obj_ {
    
protected:
    E2D_NO_PRINT;
};

#endif /* defined(__Echo2D_2__E2DRendererES2EXT__) */
