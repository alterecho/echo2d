//
//  E2DDictionary.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 19/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DDictionary.h"
#include "E2DString.h"

E2DDictionary::E2DDictionary() : E2DEnumerable(_E2D_DEF_DC_SZ)
{
    this->setClassName("E2DDictionary");
    this->setClassType(E2D_CLASS_DICTIONARY);
    __pair = (_KVPair *)malloc(sizeof(_KVPair) * (E2Dul_t)_size);
    this->initializeKVPair(__pair, 0, _size);
}

E2DDictionary::E2DDictionary(unsigned long n_) : E2DEnumerable(n_)
{
    this->setClassName("E2DDictionary");
    this->setClassType(E2D_CLASS_DICTIONARY);
    
    __pair = (_KVPair *)malloc(sizeof(_KVPair) * (E2Dul_t)_size);
    this->initializeKVPair(__pair, 0, _size);
}

E2DDictionary::~E2DDictionary()
{
    for (E2Dull_t i = 0; i < _size; i++)
    {
        E2D_FREE(__pair[i].key);
        E2D_RELEASE(__pair[i].object);
    }
    
    E2D_FREE(__pair);
    _size = 0;
    _index = 0;
}


#pragma mark - print -
inline
void E2DDictionary::print(const char *tag_) const
{
    for (unsigned long int i = 0; i < _size; i++)
    {
        if (__pair[i].object == nullptr)
            continue;
        
        E2D_PRINT("\n[KEY]:'%s'", __pair[i].key);
        E2D_PRINT("\n[VALUE]:");
        __pair[i].object->Print();
    }
}

void E2DDictionary::SetObject(E2D_Obj_ *obj_, long val_)
{
    unsigned char digits = 0;
    long val__ = val_;
    while (val__ >= 1)
    {
        val__ /= 10;
        digits++;
    }
    
    char buf[21] = {0, '\0'};
    snprintf(buf, 21, "%ld", val_);
    this->SetObject(obj_, buf);
}

void E2DDictionary::SetObject(E2D_Obj_ *obj_, const char *key_)
{
    E2D_RELEASE(__pair[_index].object);
    __pair[_index].object = obj_->Retain();
    //_pair[_index].key = (char *)malloc(sizeof(char) * strlen(key_));
    //E2D_FREE(_pair[_index].key);
    E2D_FREE(__pair[_index].key);
    __pair[_index].key = strdup(key_);
    _count++;
    
    if (++_index >= _size)
    {
        E2D_SYS_PRINT("resizing");
        this->resize(+1);
    }
}

void E2DDictionary::RemoveObjectForKey(const char *key_)
{
    for (E2Dull_t i = 0; i < _size; i++)
    {
        if (this->stringEqualsString(__pair[i].key, key_))
        {
            E2D_FREE(__pair[i].key);
            E2D_RELEASE(__pair[i].object);
            _count--;
            return;
        }
    }
}

void E2DDictionary::RemoveObject(E2D_Obj_ *obj_)
{
    if (!obj_)
        return;
    
    for (E2Dull_t i = 0; i < _size; i++)
    {
        if (__pair[i].object == obj_)
        {
            E2D_FREE(__pair[i].key);
            E2D_RELEASE(__pair[i].object);
            _count--;
            return;
        }
    }
}

E2D_Obj_* E2DDictionary::ObjectForKey(long k_)
{
    char buf[21];
    E2D_SYS_PRINT("object for key(char): %c", (char)k_);
    snprintf(buf, 21, "%ld", k_);
    return this->ObjectForKey(buf);
}

E2D_Obj_* E2DDictionary::ObjectForKey(const char *key_)
{
    E2D_SYS_PRINT("object for key: %s", key_);
    for (E2Dull_t i = 0; i < _size; i++)
    {
        if (this->stringEqualsString(__pair[i].key, key_))
            return __pair[i].object;
    }
    
    E2D_SYS_PRINT("NOT FOUND");
    return nullptr;
}

const char* E2DDictionary::KeyForObject(E2D_Obj_ *obj_)
{
    for (E2Dull_t i = 0; i < _size; i++)
    {
        if (__pair[i].object == obj_)
        {
            return __pair[i].key;
        }
    }
    return nullptr;
}

void E2DDictionary::Consolidate()
{
    
}

E2D_Obj_* E2DDictionary::NextObject()
{
    while (_enum_pointer < _size)
    {
        E2D_Obj_ *ret = __pair[_enum_pointer++].object;
        if (ret)
            return ret;
    }
    _enum_pointer = 0;
    return nullptr;
}

bool E2DDictionary::NextObject(E2D_Obj_ **stor_)
{
    if (_enum_pointer < _size)
    {
        *stor_ = __pair[_enum_pointer++].object;
        return true;
    }
    _enum_pointer = 0;
    return false;
}
