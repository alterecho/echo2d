//
//  Temporary.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 21/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef Echo2D_2_Temporary_h
#define Echo2D_2_Temporary_h

template <unsigned int n>
class E2Dmatrix {
    unsigned int size = n * n;
    unsigned int order = n;
    //float m[n * n];
    float m[n][n];
    
public:
    E2Dmatrix()
    {
        
    }
    
    static inline E2Dmatrix<n> Identity()
    {
        E2Dmatrix<n> ret;
        for (int i = 0; i < ret.order; i++)
        {
            for (int j = 0; j < ret.order; j++)
            {
                if (i == j)
                {
                    ret.m[i][j] = 1.0f;
                }
                else
                {
                    ret.m[i][j] = 0.0f;
                }
            }
        }
        
        return ret;
    }
    
    void Print()
    {
        E2D_PRINT("\nprinting matrix...\n ")
        for (int i = 0; i < order; i++)
        {
            for (int j = 0; j < order; j++)
            {
                E2D_PRINT("%f\t", m[i][j]);
            }
            E2D_PRINT("\n");
        }
    }
};


#endif
