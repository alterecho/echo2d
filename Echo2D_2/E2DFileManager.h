//
//  E2DFileManager.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_FILE_MANAGER_H
#define E2D_FILE_MANAGER_H

#include <iostream>
#include "E2D_Obj_.hpp"
#include <fstream>

class E2DFileManager : public E2D_Obj_ {
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_FILE_MANAGER;};
    static E2DFileManager *SharedFileManager();
    
    E2DFileManager();
    virtual ~E2DFileManager();
    
    E2D_NO_PRINT;
    
    virtual void PathForFile_alc(const char *file_, char **pathBuf_) const = 0;
    virtual void StringFromContentsOfFile(const char *file_, char **buf) const = 0;
};



#endif /* defined(__Echo2D_2__E2DFileManager__) */
