//
//  E2DMemoryManager.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 29/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_MEMORY_MANAGER_H
#define E2D_MEMORY_MANAGER_H

#include <iostream>
#include "E2D_Obj_.hpp"

class E2DMemoryManager : public E2D_Obj_ {
    //E2DArray _objects;
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_MEMORY_MANAGER; }
    static inline E2DMemoryManager* SharedMemoryManager(void);
    
    E2DMemoryManager(void);
    virtual ~E2DMemoryManager();
    
    E2D_NO_PRINT;
    
    void Drain(void);
};

#endif /* defined(__Echo2D_2__E2DMemoryManager__) */
