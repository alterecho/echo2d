//
//  E2DDrawable.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 25/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_DRAWABLE_H
#define E2D_DRAWABLE_H

#include <iostream>
#include "E2DGeometry.hpp"

class E2DLayer;
class E2DDrawable : public E2DGeometry {
    friend class E2DRendererES2;
    friend class E2DLayer;
    
private:
    class E2DController     *__controller;
    const E2DLayer          *__layer;
    
    
protected:
    E2Dcolor            __color;
    
    E2Dsize             __screenSize;
    E2Ddepth            __depth;

    
    GLuint              _textureID;
    class E2DRenderer   *_renderer;
    
    
    E2DDrawable(E2Ddepth depth_);
    
    void setSize(E2Dsize size_) { this->setSize(size_.width, size_.height); };
    virtual void setSize(E2Dfloat width, E2Dfloat height) = 0;
    virtual void render() = 0;
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_DRAWABLE; }
    virtual ~E2DDrawable();
    
    
    //virtual void Print() const;
    //virtual void Print(const char *) const;
    
    E2Dsize ScreenSize(void);
    
    GLuint TextureID(void) const;
    virtual const E2Dvertex* Vertices(void) const = 0;  //override
    
    virtual void SetColor(const E2Dcolor) = 0;  //override
    E2Dcolor Color(void);
    
    const E2DLayer* layer(void);

};

#endif /* defined(__Echo2D_2__E2DDrawable__) */
