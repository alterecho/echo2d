//
//  E2DObject.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 10/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//


/* ** E2DObject includes E2D_Obj_ and other derived objects. User classes should derive from this class. ** */

#ifndef E2D_OBJECT_H
#define E2D_OBJECT_H

#include <iostream>

#include "E2D_Obj_.hpp"         //#includes E2DConfig, E2DMacros
#include "E2DDataTypes.h"
#include "E2DDataStructs.hpp"

//#include "Test.h"
#include "E2DString.h"
#include "E2DDictionary.h"
#include "E2DFileManagerIOS.h"
#include "E2DFunctions.h"

//#include "E2DDrawable.hpp"

class E2DObject : public E2D_Obj_ {
    
public:
     static inline E2Dclass_t Class(void) { return E2D_CLASS_OBJECT; }
    
    E2DObject()
    {
        this->setClassName("E2DObject");
        this->setClassType(E2D_CLASS_OBJECT);
    }
    
    void Print() const
    {
        E2D_SYS_PRINT("E2D_Obj_")
    };
    
    void Print(const char *tag_) const
    {
        E2D_PRINT("\n%s", tag_);
        this->Print();
    }
    void print(const char *) const { };
    
};

#endif /* defined(__Echo2D_2__E2DObject__) */
