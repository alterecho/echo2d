precision mediump float;

attribute vec4  position, color;
uniform mat4    projection, modelView;

varying vec4 f_color;

void main(void)
{
    gl_Position = projection * modelView * position;
    f_color = color;
}