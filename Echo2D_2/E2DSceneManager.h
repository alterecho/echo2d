//
//  E2DSceneManager.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 02/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_SCENE_MANAGER_H
#define E2D_SCENE_MANAGER_H

#include <iostream>
#include "E2D_Obj_.hpp"

class E2DScene;
class E2DSceneManager : E2D_Obj_ {
    friend class E2DController;
    
    E2DScene            *currentScene, *deprecatedScene;
    
    void removeDeprecated(void);
    
public:
     static inline E2Dclass_t Class(void) { return E2D_CLASS_SCENE_MANAGER; }
    E2DSceneManager* SharedSceneManager(void);
    
    E2DSceneManager(void);
    virtual ~E2DSceneManager();
    
    E2D_NO_PRINT;
    
    void ReplaceScene(E2DScene *);
    E2DScene* CurrentScene(void);
};

#endif /* defined(__Echo2D_2__E2DSceneManager__) */
