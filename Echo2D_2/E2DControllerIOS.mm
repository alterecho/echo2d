//
//  E2DControllerIOS.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DControllerIOS.h"

#include "E2DRendererES2.h"
#include "E2DFileManagerIOS.h"
#include "E2DImageLoaderIOS.h"
#include "E2DTextureManager.h"
#include "E2DScene.h"
#include "E2DKeyboardIOS.h"
#include "E2DMusicPlayerIOS.h"

//*
E2DControllerIOS::E2DControllerIOS() : E2DController()
{
    this->setClassName("E2DControllerIOS");
    
    fileManager = new E2DFileManagerIOS();
    imageLoader = new E2DImageLoaderIOS();
    
    glView = [[E2DGLView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    glView.layer.opaque = YES;
    E2D_PRINT("\nopaque:%s", glView.layer.opaque ? "YES" : "NO");
    _screenSize = E2Dsize(glView.frame.size.width, glView.frame.size.height);
    _screenScale = [[UIScreen mainScreen] scale];
    //screenSize *= screenScale;
    glView->screenSize = _screenSize;
    glView->controller = this;
    
    retina = (_screenScale == 2.0f);
    
    layer = (CAEAGLLayer *)glView.layer;
    
    context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    //*
    if (!context || ![EAGLContext setCurrentContext:context])
    {
        printf("\nunable to set context");
        exit(EXIT_FAILURE);
    }
    // */
    //E2D_SYS_ASSERT((context || [EAGLContext setCurrentContext:context]), "unable to set context");
    this->initManagers();
    keyboard = new E2DKeyboardIOS();
    this->initModules();
    musicPlayer = new E2DMusicPlayerIOS();
    
    [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:layer];
}
// */

E2DControllerIOS::~E2DControllerIOS()
{
    /*
    E2D_RELEASE(musicPlayer);
    E2D_RELEASE(keyboard);
    E2D_RELEASE(imageLoader);
    E2D_RELEASE(fileManager);
    //*/
    E2D_RELEASE(musicPlayer);
    this->releaseModules();
    E2D_RELEASE(keyboard);
    this->releaseManagers();
    [context release];
    [glView release];
    E2D_RELEASE(imageLoader);
    E2D_RELEASE(fileManager);
}

E2DGLView* E2DControllerIOS::view()
{
    return glView;
}

void E2DControllerIOS::PresentRenderBuffer() const
{
    [context presentRenderbuffer:GL_RENDERBUFFER];
}

void E2DControllerIOS::update(E2Delapsed dt_)
{
    this->E2DController::update(dt_);
    this->PresentRenderBuffer();
}

