//
//  TestLabel.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 06/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef __Echo2D_2__TestLabel__
#define __Echo2D_2__TestLabel__

#include <iostream>
#include "E2DScene.h"

#include "E2DLayer.h"
#include "E2DLabel.h"


class TestLabel : public E2DScene {
    E2DLayer        *layer1;
    E2DLabel        *label1;
    
public:
    TestLabel();
    virtual ~TestLabel();
    
    void Update(E2Delapsed dt_);
};

#endif /* defined(__Echo2D_2__TestLabel__) */
