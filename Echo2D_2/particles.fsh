precision lowp float;

varying vec4 f_color;
uniform sampler2D texture;

void main(void)
{
    gl_FragColor = f_color * texture2D(texture, gl_PointCoord);
}