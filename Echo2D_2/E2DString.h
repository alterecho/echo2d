//
//  E2DString.h
//  E2DDerivedStructs
//
//  Created by Vijay Chandran J on 02/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_STRING_H
#define E2D_STRING_H

#include <iostream>
#include "E2D_Obj_.hpp"

class E2DString : public E2D_Obj_ {
    char        *_string = nullptr;
    size_t      _length = 0;
    
    _E2D_DISABLE_COPY(E2DString);
    void copyString(const char *string_);
    void addString(const char *string_);
    
    virtual ~E2DString(void);
    
public:
     static inline E2Dclass_t Class(void) { return E2D_CLASS_STRING; }
    E2DString(void);
    E2DString(const E2DString *string_);
    E2DString(const char *string_);
    
    
    
    void Print(void) const;
    void Print(const char *) const;
    void print(const char *) const { };
    
    void Set(const char *);
    void Set(E2DString *);
    
    const char* String(void);
    size_t Length(void);
    
    void Append(const char *);
    void Append(const E2DString *);
    
    bool IsEqualTo(const char *);
    bool IsEqualTo(const E2DString *);
    
    void Empty(void); /* clears string */
    
    void deleteExtension(void);
    size_t Extension(char **out_);
};

inline
void E2DString::copyString(const char *string_)
{
    E2D_FREE(_string);
    _length = strlen(string_);
    _string = (char *)malloc(_length + 1);
    strcpy(_string, string_);
}

inline
void E2DString::addString(const char *string_)
{
    _length += strlen(string_);
    _string = (char *)realloc(_string, (_length + 1));
    
    strcat(_string, string_);
}

#endif /* defined(__E2DDerivedStructs__E2DString__) */