//
//  Temp.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 15/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef Echo2D_2_Temp_h
#define Echo2D_2_Temp_h
/
- (void)loadEmitterConfig:(NSString *)file_
{
    //[rem] use filemanager
    emitterConfig = [file_ retain];
    
    NSString *fileName = [file_ stringByDeletingPathExtension];
    NSString *fileEXT = [file_ pathExtension];
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:fileEXT];
    
    if (!path)
    {
        E2DExit(@"Unable to locate file '%@'", file_);
    }
    
    FILE *file = fopen([path cStringUsingEncoding:NSUTF8StringEncoding], "r");
    if (!file)
    {
        E2DExit(@"Unable to open file '%@'", file_);
    }
    
    int int_;
    float float_;
    
    seekPropertyInFile("particles_max", file);
    fscanf(file, "%d", &int_);
    activeParticles_MAX = int_;
    
    seekPropertyInFile("lifetime_min", file);
    fscanf(file, "%f", &float_);
    lifetime_MIN = float_;
    
    seekPropertyInFile("lifetime_max", file);
    fscanf(file, "%f", &float_);
    lifetime_MAX = float_;
    
    seekPropertyInFile("color_morph", file);
    fscanf(file, "%d", &int_);
    morphColor = int_ == 1 ? YES : NO;
    
    seekPropertyInFile("color_min", file);
    fscanf(file, "%f", &float_);
    color_MIN.r = float_ / 255.0f;
    fscanf(file, "%f", &float_);
    color_MIN.g = float_ / 255.0f;
    fscanf(file, "%f", &float_);
    color_MIN.b = float_ / 255.0f;
    fscanf(file, "%f", &float_);
    color_MIN.a = float_ / 255.0f;
    
    //e2dglLogColor(&color_MIN);
    
    seekPropertyInFile("color_max", file);
    fscanf(file, "%f", &float_);
    color_MAX.r = float_ / 255.0f;
    fscanf(file, "%f", &float_);
    color_MAX.g = float_ / 255.0f;
    fscanf(file, "%f", &float_);
    color_MAX.b = float_ / 255.0f;
    fscanf(file, "%f", &float_);
    color_MAX.a = float_ / 255.0f;
    
    //e2dglLogColor(&color_MAX);
    
    seekPropertyInFile("fade", file);
    fscanf(file, "%d", &int_);
    fade = int_ == 1 ? YES : NO;
    
    seekPropertyInFile("opacity_min", file);
    fscanf(file, "%f", &float_);
    opacity_MIN = float_ / 255.0f;
    
    seekPropertyInFile("opacity_max", file);
    fscanf(file, "%f", &float_);
    opacity_MAX = float_ / 255.0f;
    
    seekPropertyInFile("scale", file);
    fscanf(file, "%d", &int_);
    scale = int_ == 1 ? YES : NO;
    
    seekPropertyInFile("scale_min", file);
    fscanf(file, "%f", &float_);
    scale_MIN = float_;
    
    seekPropertyInFile("scale_max", file);
    fscanf(file, "%f", &float_);
    scale_MAX = float_;
    
    seekPropertyInFile("scale_start_max", file);
    fscanf(file, "%f", &float_);
    scale_START = float_;
    
    seekPropertyInFile("scale_end_max", file);
    fscanf(file, "%f", &float_);
    scale_END = float_;
    
    seekPropertyInFile("translational", file);
    fscanf(file, "%d", &int_);
    translationalMotion = int_ == 1 ? YES : NO;
    
    seekPropertyInFile("velocity_min", file);
    fscanf(file, "%f", &float_);
    velocity_MIN.x = float_ * screenScale;
    fscanf(file, "%f", &float_);
    velocity_MIN.y = float_ * screenScale;
    
    seekPropertyInFile("velocity_max", file);
    fscanf(file, "%f", &float_);
    velocity_MAX.x = float_ * screenScale;
    fscanf(file, "%f", &float_);
    velocity_MAX.y = float_ * screenScale;
    
    seekPropertyInFile("velocity_damping_min", file);
    fscanf(file, "%f", &float_);
    velocity_Damping_MIN.x = float_ * screenScale;
    fscanf(file, "%f", &float_);
    velocity_Damping_MIN.y = float_ * screenScale;
    
    seekPropertyInFile("velocity_damping_max", file);
    fscanf(file, "%f", &float_);
    velocity_Damping_MAX.x = float_ * screenScale;
    fscanf(file, "%f", &float_);
    velocity_Damping_MAX.y = float_ * screenScale;
    
    seekPropertyInFile("velocity_spread_min", file);
    fscanf(file, "%f", &float_);
    velocity_SPREAD_MIN.x = float_ * screenScale;
    fscanf(file, "%f", &float_);
    velocity_SPREAD_MIN.y = float_ * screenScale;
    
    seekPropertyInFile("velocity_spread_max", file);
    fscanf(file, "%f", &float_);
    velocity_SPREAD_MAX.x = float_ * screenScale;
    fscanf(file, "%f", &float_);
    velocity_SPREAD_MAX.y = float_ * screenScale;
    
    seekPropertyInFile("spin", file);
    fscanf(file, "%d", &int_);
    spin = int_ == 1 ? YES : NO;
    
    seekPropertyInFile("angle_min", file);
    fscanf(file, "%f", &float_);
    angle_MIN = float_;
    
    seekPropertyInFile("angle_max", file);
    fscanf(file, "%f", &float_);
    angle_MAX = float_;
    
    seekPropertyInFile("angular_velocity_min", file);
    fscanf(file, "%f", &float_);
    w_MIN = float_;
    
    seekPropertyInFile("angular_velocity_max", file);
    fscanf(file, "%f", &float_);
    w_MAX = float_;
    
    seekPropertyInFile("radialmotion", file);
    fscanf(file, "%d", &int_);
    radialMotion = int_ == 1 ? YES : NO;
    
    seekPropertyInFile("radiusFC_min", file);
    fscanf(file, "%f", &float_);
    radius_MIN_START = float_;
    
    seekPropertyInFile("radiusFC_max", file);
    fscanf(file, "%f", &float_);
    radius_MAX_START = float_;
    
    seekPropertyInFile("radiusFC_limit_min", file);
    fscanf(file, "%f", &float_);
    radius_LIMIT_MIN = float_;
    
    seekPropertyInFile("radiusFC_limit_max", file);
    fscanf(file, "%f", &float_);
    radius_LIMIT_MAX = float_;
    
    seekPropertyInFile("radiusFC_delta_min", file);
    fscanf(file, "%f", &float_);
    radius_DELTA_MIN = float_ * screenScale;
    
    seekPropertyInFile("radiusFC_delta_max", file);
    fscanf(file, "%f", &float_);
    radius_DELTA_MAX = float_ * screenScale;
    
    seekPropertyInFile("angleFC_min", file);
    fscanf(file, "%f", &float_);
    angleFC_MIN = float_;
    
    seekPropertyInFile("angleFC_min", file);
    fscanf(file, "%f", &float_);
    angleFC_MAX = float_;
    
    seekPropertyInFile("angleFC_delta_min", file);
    fscanf(file, "%f", &float_);
    angleFC_DELTA_MIN = E2D_DEGREES_TO_RADIANS(float_);
    
    seekPropertyInFile("angleFC_delta_max", file);
    fscanf(file, "%f", &float_);
    angleFC_DELTA_MAX = E2D_DEGREES_TO_RADIANS(float_);
    
    seekPropertyInFile("gravity", file);
    fscanf(file, "%f", &float_);
    gravity.x = float_ * (1.0f / E2D_FPS) * screenScale;
    fscanf(file, "%f", &float_);
    gravity.y = float_ * (1.0f / E2D_FPS) * screenScale;
    
    seekPropertyInFile("oov", file);
    fscanf(file, "%d", &int_);
    outOfViewAllowed = int_ == 1 ? YES : NO;
    
    /*
     seekPropertyInFile("rebound", file);
     fscanf(file, "%d", &int_);
     self.rebound = int_ == 1 ? YES : NO;
     fscanf(file, "%d", &int_);
     self.reboundBottom = int_ == 1 ? YES : NO;
     fscanf(file, "%d", &int_);
     self.reboundRight = int_ == 1 ? YES : NO;
     fscanf(file, "%d", &int_);
     self.reboundTop = int_ == 1 ? YES : NO;
     fscanf(file, "%d", &int_);
     self.reboundLeft = int_ == 1 ? YES : NO;
     //*/
    fclose(file);
}

#endif
