//
//  TestReplace2.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 15/06/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef __Echo2D_2__TestReplace2__
#define __Echo2D_2__TestReplace2__

#include <iostream>
#include "E2DScene.h"

class TestReplace2 : public E2DScene {
  
protected:
    void update(E2Delapsed dt_);
    
public:
    TestReplace2();
    ~TestReplace2();
};

#endif /* defined(__Echo2D_2__TestReplace2__) */
