//
//  E2DparticleLimits.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 16/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_PARTICLE_LIMITS_H
#define E2D_PARTICLE_LIMITS_H

#include "E2DDataStructs.hpp"

struct E2DparticleLimits {
    /* attributes */
    E2Dseconds      lifeTime;
    E2Dcolor        color_start, color_end;
    E2Dfloat        scale_start, scale_end;
    E2Dradians      angle;
    E2Dfloat        radDist, radDist_limit;
    E2Dradians      radAngle;
    
    /* mod limits */
    E2Dvec2       vel, vel_damping, vel_spread;
    E2Dradians      aVel;
    E2Dfloat        radDist_d, radDist_d_damping;
    E2Dradians      radAngle_d, radAngle_d_damping;
    
    
    E2DparticleLimits(void) :
    lifeTime(0.0f),
    color_start(E2Dcolor(255)),
    color_end(E2Dcolor(255)),
    scale_start(1.0f), scale_end(1.0f),
    vel(E2Dvec2()), vel_damping(E2Dvec2(1.0f, 1.0f)), vel_spread(E2Dvec2()),
    angle(0.0f), aVel(0.0f),
    radDist(0.0f), radDist_limit(0.0f), radDist_d(0.0f),
    radAngle(0.0f), radAngle_d(0.0f) { };
};

typedef struct  {
    E2DparticleLimits min, max;
} E2DpLimits;




#endif
