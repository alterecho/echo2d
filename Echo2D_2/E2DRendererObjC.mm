//
//  E2DRenderer.m
//  Echo2D_2
//
//  Created by Vijay Chandran J on 10/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#import "E2DRendererObjC.h"
#include "E2DIncludes.h"

#define ATTRIB_POSITION     "position"
#define ATTRIB_COLOR        "color"
#define ATTRIB_TEX_COORD    "texCoord"

#define UNIFORM_PROJECTION  "projection"
#define UNIFORM_MODEL_VIEW  "modelView"
#define UNIFORM_TEXTURE     "texture"

#define SHADER_VERT         @"shader.vsh"
#define SHADER_FRAG         @"shader.fsh"

static inline void readFile(const char *filePath_, char *out_, size_t size)
{
    char buffer_[256] = {'\0'};
    
    FILE *file = fopen(filePath_, "r");
    
    while (fscanf(file, "%s", buffer_) != EOF) {
        //printf("\n%s", buffer_);
        strcat(out_, buffer_);
        strcat(out_, " ");
    }
    
    //E2D_SYS_PRINT("\nshader:%s", buffer);
    
}

@interface E2DRendererObjC ()
- (void)setupBuffers;
- (void)setupViewPort;
- (void)setupShaders;
- (void)setupShader:(GLuint *)shader_ type:(GLenum)type_ file:(NSString *)file_;
@end

@implementation E2DRendererObjC

-(id)initWithScreenSize:(E2Dsize)screenSize_
{
    self = [super init];
    if (self) {

        screenSize = screenSize_;
        
        [self setupBuffers];
        [self setupViewPort];
        [self setupShaders];
        
        //*
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        //*/
        
        E2Dmat4 ortho;
        ortho.SetOrtho(-screenSize.width * 0.5f,
                       screenSize.width * 0.5f,
                       -screenSize.width * 0.5f / (screenSize.width / screenSize.height),
                       screenSize.width * 0.5f / (screenSize.width /screenSize.height),
                       -1.0f,
                       1.0f);
        ortho.Print();
        GLint projection = glGetUniformLocation(program, UNIFORM_PROJECTION);
        glUniformMatrix4fv(projection, 1, 0, ortho.Pointer());
        
        float identity[] = {
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f
        };
        e2dPrintArray(identity, 16, 4, "ident");
        
        GLint modelView = glGetUniformLocation(program, UNIFORM_MODEL_VIEW);
        glUniformMatrix4fv(modelView, 1, 0, identity);
        
        GLfloat f[16] = {
            0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f
        };
        
        GLint prj = glGetUniformLocation(program, UNIFORM_PROJECTION);
        glGetUniformfv(program, prj, f);
        e2dPrintArray(f, 16, 4, "projection");
        
        GLint mv = glGetUniformLocation(program, UNIFORM_MODEL_VIEW);
        glGetUniformfv(program, mv, f);
        e2dPrintArray(f, 16, 4, "model view");
        printf("\n");
        
        [self createTexture:@"boxpow2.png"];
        
        [self render];
        
    }
    return self;
}

- (void)dealloc
{
    //[context release];
    //[layer release];
    
    glDeleteShader(fragmentShader);
    glDeleteShader(vertexShader);
    glDeleteProgram(program);
    
    [super dealloc];
}

+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

- (void)setupBuffers
{
    glGenFramebuffers(1, &frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    
    glGenRenderbuffers(1, &colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    
    /*
    if (!layer)
    {
        E2D_SYS_EXIT_OC(@"CAEAGLLayer not initialized");
    }
    [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:layer];
    //*/
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
}

- (void)setupViewPort
{
    //glViewport(screenSize.width * 0.5f - screenSize.width * 0.5f, screenSize.height * 0.5f - screenSize.width * 0.5f, screenSize.width, screenSize.width);
    glViewport(0.0f, 0.0f, screenSize.width, screenSize.height);
}

- (void)setupShaders
{
    [self setupShader:&vertexShader type:GL_VERTEX_SHADER file:SHADER_VERT];
    [self setupShader:&fragmentShader type:GL_FRAGMENT_SHADER file:SHADER_FRAG];
    
    program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    
    //glDeleteShader(vertexShader);
    //glDeleteShader(fragmentShader);
    
    glLinkProgram(program);
    
    
    GLint linkStatus;
    glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
    if (linkStatus == GL_FALSE)
    {
        GLchar msgLog[256];
        glGetProgramInfoLog(program, sizeof(msgLog), 0, msgLog);
        printf("\n[ERROR LINKING PROGRAM] %s", msgLog);
        exit(1);
    }
    
    glUseProgram(program);

}

- (void)setupShader:(GLuint *)shader_ type:(GLenum)type_ file:(NSString *)file_;
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[file_ stringByDeletingPathExtension] ofType:[file_ pathExtension]];
    
    *shader_ = glCreateShader(type_);
    
    char *buffer = (char *)malloc(sizeof(char) * 1000);
    memset(buffer, 0, 1000);
    readFile([filePath UTF8String], buffer, sizeof(buffer));
    printf("\n%s\n", buffer);
    
    glShaderSource(*shader_, 1, &buffer, NULL);
    E2D_FREE(buffer);
    
    glCompileShader(*shader_);
    GLint status;
    glGetShaderiv(*shader_, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE)
    {
        char infoLog[256];
        glGetShaderInfoLog(*shader_, sizeof(infoLog), NULL, infoLog);
        printf("\n[ERROR COMPILING SHADER]:%s", infoLog);
        exit(1);
    }
}

- (GLuint)createTexture:(NSString *)file_
{
    
    NSString *fileName = [file_ stringByDeletingPathExtension];
    NSString *fileExt = [file_ pathExtension];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:fileExt];
    NSData *texData = [[NSData alloc] initWithContentsOfFile:path];
    UIImage *image = [[UIImage alloc] initWithData:texData];
    if (image == nil)
        NSLog(@"Do real error checking here");
    
    GLuint64 width = CGImageGetWidth(image.CGImage);
    GLuint64 height = CGImageGetHeight(image.CGImage);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    void *imageData = malloc( (size_t)height * (size_t)width * 4 );
    CGContextRef context = CGBitmapContextCreate( imageData, (size_t)width, (size_t)height, 8, 4 * (size_t)width, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big );
    CGColorSpaceRelease( colorSpace );
    CGContextClearRect( context, CGRectMake( 0, 0, width, height ) );
    CGContextTranslateCTM( context, 0, height - height );
    CGContextDrawImage( context, CGRectMake( 0, 0, width, height ), image.CGImage );
    
    GLuint ret[1];
    glGenTextures(1, &ret[0]);
    glBindTexture(GL_TEXTURE_2D, ret[0]);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)width, (GLsizei)height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
    
    CGContextRelease(context);
    
    E2D_FREE(imageData);
    [image release];
    [texData release];
    
    
    return ret[0];
}

//*
- (void)render
{
    
    glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    float magX = 100.0f;
    float magY = 100.0f;
    /*
    float vertices_triangles[] = {
        -magX, -magY,
        -magX, magY,
        magX, magY,
        
        magX, magY,
        magX, -magY,
        -magX, -magY
    };
    //*/
    
    float vertices_triangleStrip[] {
        -magX * 0.5f, magY * 0.5f,
        magX * 0.5f, magY * 0.5f,
        -magX * 0.5f, -magY * 0.5f,
        magX * 0.5f, -magY *0.5f
    };
    
    float texCoords[] = {
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f
    };
    
    float colors[] = {
        1.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 0.0f, 1.0f,
        
        0.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
    };
    
    GLuint pos = glGetAttribLocation(program, ATTRIB_POSITION);
    GLuint col = glGetAttribLocation(program, ATTRIB_COLOR);
    GLint coord = glGetAttribLocation(program, ATTRIB_TEX_COORD);
    
    glEnableVertexAttribArray(pos);
    glEnableVertexAttribArray(col);
    glEnableVertexAttribArray(coord);
    
    glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), vertices_triangleStrip);
    glVertexAttribPointer(col, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), colors);
    glVertexAttribPointer(coord, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), texCoords);
        
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glDisableVertexAttribArray(pos);
    glDisableVertexAttribArray(col);
    glDisableVertexAttribArray(coord);
}
//*/



@end
