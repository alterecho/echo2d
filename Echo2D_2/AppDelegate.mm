//
//  AppDelegate.m
//  Echo2D_2
//
//  Created by Vijay Chandran J on 10/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#import "AppDelegate.h"
#import "RootViewController.h"

#include "E2DConfig.h"
#include "E2DControllerIOS.h"

#import "E2DRendererObjC.h"
#import "E2DGLView.h"
#import "E2DString.h"

#include "TestScene1.h"
#include "TestLabel.h"
#include "TestEmitter.h"
#include "TestSprite.h"
#include "E2DRendererES2.h"
#include "TestReplace1.h"
#include "TestReplace2.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    /*
    E2DString *string = new E2DString("abcd.efg");
    string->Print();
    //E2DString *ext = new E2DString();
    char *ext = NULL;
    string->Extension(&ext);
    std::cout <<std::endl << "ext:"<< ext << std::endl;
    string->deleteExtension();
    string->Print();
    //*/
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    rootViewController = [[RootViewController alloc] init];
    
    [self.window makeKeyAndVisible];
    
    controller = new E2DControllerIOS;
    //TestScene1 *ts1 = new TestScene1();
    //TestLabel *lt1 = new TestLabel();
    TestEmitter *et1 = new TestEmitter();
    //TestSprite *tspr = new TestSprite();
    //TestReplace1 *tr1 = new TestReplace1();
    controller->ReplaceScene(et1);
    //E2D_RELEASE(ts1);
    
    //[self.window addSubview:controller->view()];
    
    //[controller->view() setMultipleTouchEnabled:YES];
    rootViewController.view = controller->view();
    self.window.rootViewController = rootViewController;
    //[self initObjC];
    
    /*
    int v = 10;
    int *v1 = &v;
    int v2 = *v1;
    v2 = 12;
    printf("\nv:%d, v1:%d, v2:%d", v, *v1, v2);
    //*/
    
    /*
    displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(update)];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    //*/
    
    timer = [NSTimer scheduledTimerWithTimeInterval:(1.0f / (float)_E2D_FRAME_RATE) target:self selector:@selector(update:) userInfo:nil repeats:YES];
    
    
    
    return YES;
}

- (void)initObjC
{
    glView = [[E2DGLView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window addSubview:glView];
    layer = (CAEAGLLayer *)glView.layer;
    
    context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    if (!context || ![EAGLContext setCurrentContext:context])
    {
        printf("unable to set context");
        exit(1);
    }
    
    renderer_objC = [[E2DRendererObjC alloc] initWithScreenSize:E2Dsize(glView.frame.size.width, glView.frame.size.height)];
    
    [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:layer];
    
    [renderer_objC render];
    [context presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)dealloc
{
    E2D_RELEASE(controller);
    if (displayLink)
    {
        [displayLink invalidate];
        [displayLink release];
        displayLink = nil;
    }
    
    [super dealloc];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)update:(CADisplayLink *)displayLink_
{
    static CFTimeInterval  currentTime = CACurrentMediaTime(), lastTime = CACurrentMediaTime();
    static CFTimeInterval avg = 0;
    
    currentTime = CACurrentMediaTime();
    
    //E2D_PRINT("\n%f", 1.0f / (currentTime - lastTime));
    //E2D_PRINT("\n%f avg:%f", 1.0f / (currentTime - lastTime), avg);
    controller->update(currentTime - lastTime);
    avg += (currentTime - lastTime) / 2.0f;

    lastTime = currentTime;
    
    
}

- (void)updateObjC
{
    [renderer_objC render];
    [context presentRenderbuffer:GL_RENDERBUFFER];
}

@end
