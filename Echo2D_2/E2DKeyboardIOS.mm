//
//  E2DKeyboardIOS.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 12/06/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DKeyboardIOS.h"
#import "E2DGLView.h"

E2DKeyboardIOS::E2DKeyboardIOS() {
    this->setClassName("E2DKeyboardIOS");
    nativeKeyboard = false;
}

E2DKeyboardIOS::~E2DKeyboardIOS()
{

}

/*
void E2DKeyboardIOS::DeleteText()
{
    E2D_PRINT("\nios:deleteBackward");
}

bool E2DKeyboardIOS::HasText()
{
    E2D_PRINT("ios:has text");
    return false;
}

void E2DKeyboardIOS::InsertText(const char *t_)
{
    E2D_PRINT("\nios:insert text:%s", t_);
}
//*/

void E2DKeyboardIOS::Activate()
{
    printf("\nios activate");
    [[E2DGLView sharedGLView] becomeFirstResponder];
}

void E2DKeyboardIOS::Dismiss()
{
    printf("\nios dismiss");
    [[E2DGLView sharedGLView] resignFirstResponder];
}