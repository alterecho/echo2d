//
//  E2DOverlay.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_OVERLAY_H
#define E2D_OVERLAY_H

#include <iostream>
#include "E2D_Obj_.hpp"
#include "E2DSprite.h"

class E2DOverlay : public E2D_Obj_ {
    
    class E2DController *controller;
    class E2DKeyboard   *keyboard;
    
    E2Dsize     screenSize;
    E2Dpoint    screenCenter;
    //E2DSprite   *spr;
    
public:
    E2DOverlay();
    ~E2DOverlay();
    
    E2D_NO_PRINT;
    
    void Render();
};

#endif /* defined(__Echo2D_2__E2DOverlay__) */
