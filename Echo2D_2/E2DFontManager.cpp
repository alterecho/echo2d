//
//  E2DFontManager.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 07/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DFontManager.h"
#include "E2DFileManager.h"
#include "E2DFont.h"
#include "E2DRenderer.h"

static E2DFontManager *instanceofFontManager(nullptr);

E2DFontManager* E2DFontManager::SharedFontManager()
{
    E2D_ASSERT(instanceofFontManager, "FontManager not initialized");
    return instanceofFontManager;
}


E2DFontManager::E2DFontManager()
{
    this->setClassName("E2DFontManager");
    this->setClassType(E2D_CLASS_FONT_MANAGER);
    instanceofFontManager = this;
    
    __fileManager = E2DFileManager::SharedFileManager();
    __renderer = E2DRenderer::SharedRenderer();
    __fontFiles = new E2DDictionary();
}

E2DFontManager::~E2DFontManager()
{
    E2D_RELEASE(__fontFiles);
}

void E2DFontManager::print(const char *msg_) const
{
    __fontFiles->Print();
}

void E2DFontManager:: CacheFontFile(const char *file_)
{
    E2DDictionary *fonts = (E2DDictionary *)__fontFiles->ObjectForKey(file_);
    if (fonts)
        return;
    
    E2DString *imageFile = new E2DString(file_);
    imageFile->deleteExtension();
    imageFile->Append(".png");
    char *path = NULL;
    __fileManager->PathForFile_alc(file_, &path);
    E2D_PRINT("\npath:%s", path);
    std::ifstream stream;
    stream.open(path);
    E2D_FREE(path);
    E2D_ASSERT(stream.is_open(), "unable to open file '%s'", file_);
    
    GLuint t;
    __renderer->CreateTexture(imageFile->String(), &t);
    
    char buf[256] = {'0'};
    fonts = new E2DDictionary();
    while (e2d::readStringFromFile(stream, buf))
    {
        if(strcmp(buf, ">") == 0)
        {
           // E2DFont *font = nullptr;
            E2Dsize size;
            
            char c[2] = { 0, '\0' };
            c[0] = e2d::readElementFromFile<char>(stream);

            size.width = e2d::readElementFromFile<float>(stream);
            size.height = e2d::readElementFromFile<float>(stream);
            
            E2DtexCoord coord[4];
            coord[0].s = e2d::readElementFromFile<float>(stream);
            coord[0].t = e2d::readElementFromFile<float>(stream);
            
            coord[1].s = e2d::readElementFromFile<float>(stream);
            coord[1].t = e2d::readElementFromFile<float>(stream);
            
            coord[2].s = e2d::readElementFromFile<float>(stream);
            coord[2].t = e2d::readElementFromFile<float>(stream);
            
            coord[3].s = e2d::readElementFromFile<float>(stream);
            coord[3].t = e2d::readElementFromFile<float>(stream);
            
            E2DFont *f = new E2DFont(c[0], size, coord, t);
            fonts->SetObject(f, c);
            E2D_RELEASE(f);
        }
        
    }
    
    __fontFiles->SetObject(fonts, file_);
    E2D_RELEASE(fonts);
    
    stream.close();
}

E2Duint E2DFontManager::FontFileTextureID(const char *file_)
{
    E2Duint ret;
    
    E2DDictionary *fonts = (E2DDictionary *)__fontFiles->ObjectForKey(file_);
    E2DFont *f = (E2DFont *)fonts->ObjectForKey("a");
    ret = f->TextureID();
    
    return ret;
}

const E2DFont* E2DFontManager::FontForCharacter(const char c_, const char *fontFile_)
{
    const char char_str[2] = {c_, '\0'};
    E2DDictionary *fonts = (E2DDictionary *)__fontFiles->ObjectForKey(fontFile_);
    E2D_SYS_ASSERT(fonts, "font file '%s' not cached", fontFile_);
    
    return (E2DFont *)fonts->ObjectForKey(char_str);
}

