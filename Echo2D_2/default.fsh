varying lowp vec4 f_color;

varying lowp vec2 f_texCoord;
uniform sampler2D texture;

void main(void)
{
    gl_FragColor = f_color * texture2D(texture, f_texCoord);
}