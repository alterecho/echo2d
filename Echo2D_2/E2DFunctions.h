//
//  E2DFunctions.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 06/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_FUNCTIONS_H
#define E2D_FUNCTIONS_H

#include <fstream>
#include "E2DDataTypes.h"

namespace e2d {
    
    template <typename T>
    unsigned long nearestPow2(T val_)
    {
        unsigned long ret = 2;
        while (ret < val_)
        {
            ret *= 2;
        }
        
        return ret;
    }
    
    template <typename T = float>
    T greaterValue(T v1, T v2)
    {
        return v1 > v2 ? v1 : v2;
    }
    
    static inline float random_0_1(void)
    {
        return (arc4random() % 100) / 100.0f;
    }
    static inline char random_MINUS_1_1_ABS(void)
    {
        return (arc4random() % 100) > 50 ? 1 : -1;
    }
    static inline E2Dubyte random_255(void)
    {
        return arc4random() % 255;
    }
    
    static inline float convert_255_1(float val)
    {
        return  val / 255.0f;
    }
    
    static inline float convert_1_255(float val)
    {
        return 255.0f / val;
    }
    
    static inline bool readStringFromFile(std::ifstream& stream_, char *buf_)
    {
        buf_[0] = '\0';
        char c_buf[2] = { 0, '\0' };
        while (stream_.good())
        {
            c_buf[0] = stream_.get();
            if (c_buf[0] == ' ' || c_buf[0] == '\n') return true;
            strcat(buf_, c_buf);
        }
        
        return false;
    }
    
    template <typename T>
    static inline T readElementFromFile(std::ifstream& stream_)
    {
        T ret;
        stream_ >> ret;
        return ret;
    }
    
    template <typename T>
    unsigned char digits(T number)
    {
        unsigned char ret;
        T buf = number;
        while (buf >= 1)
        {
            buf /= 10;
            ret++;
        }
        
        return ret;
    }
    
    static inline size_t copyCString_alc(char **t, const char *source)  //manual dealloc
    {
        size_t l = strlen(source), i = 0;
        E2D_FREE(*t);
        *t = (char *)malloc(sizeof(char) * l + 1);
        (*t)[l] = '\0';
        while (i < l)
        {
            (*t)[i] = source[i];
            printf("\n%c, %c", (*t)[i], source[i]);
            i++;
        }
        
        return l;
    }
    
    
    static inline unsigned char extension(const char *string, char **buf)   //[rem] adds extra char/array? (fixed)
    {
        unsigned char ret = 0;
        
        size_t l = strlen(string);
        size_t i = l;
        
        while (i > 0) {
            if (string[i] == '.')
            {
                ret = l - 1 - i;
                i++;
                E2D_FREE(*buf);
                *buf = (char *)malloc(sizeof(unsigned char) * ret + 1);
                size_t i1 = 0;
                while (i < l)
                {
                    (*buf)[i1++] = string[i++];
                }
                (*buf)[i1] = '\0';
                
                break;
            }
            i--;
        }
        return ret;
    }
    
    static inline bool strcmp(const char *s1, const char*s2)
    {
        size_t
        l = strlen(s1),
        i = strlen(s2);
        if (l != i)
            return false;
        
        i = 0;
        
        while (i < l) {
            if (s1[i] != s2[i])
                return false;
            i++;
        }
        
        return true;
    }
    
    typedef enum {
        endian_big,
        endian_little
    }E2Dendian;
    
    static inline void printStringFromInt(unsigned int n, E2Dendian endian = endian_big, const char *msg = NULL)
    {
        if (!msg)
            msg = "string:";
        
        if (endian == endian_big)
            printf("\n%s:%c%c%c%c", msg, n & 0xFF, (n >> 8) & 0xFF, (n >> 16) & 0xFF, (n >> 24) & 0xFF);
        else
            printf("\n%s:%c%c%c%c", msg, (n >> 24) & 0xff, (n >> 16) & 0xff, (n >> 8) & 0xff, n & 0xff);
    }
    
    static inline void extractString(int n, char *buf, size_t size, E2Dendian endian = endian_big)
    {
        if (!buf)
            return;
        for (unsigned int i = 0; i < 4 && i < size ; i++)
        {
            buf[i] = (n >> 8 * i) & 0xff;
        }
        buf[size] = '\0';
    }
    
}


#endif
