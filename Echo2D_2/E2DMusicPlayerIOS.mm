//
//  E2DMusicPlayer.cpp
//  Echo2D_2_Audio
//
//  Created by Vijay Chandran J on 13/06/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#include "E2DMusicPlayerIOS.h"

#pragma mark -
#pragma mark E2DPlayer
#pragma mark -
E2DPlayerIOS::E2DPlayerIOS(const char *f_)
{
    fileName = new E2DString(f_);
    NSString *file = [[NSString alloc] initWithCString:f_ encoding:NSUTF8StringEncoding];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:[[NSBundle mainBundle] URLForResource:[file stringByDeletingPathExtension] withExtension:[file pathExtension]] error:nil];
}

E2DPlayerIOS::~E2DPlayerIOS()
{
    [player stop];
    [player release];
    fileName->Release();
}

#pragma mark -
#pragma mark E2DMusicPlayerIOSDelegate
#pragma mark -
@implementation E2DMusicPlayerIOSDelegate
- (id)initWithPlayer:(struct e2d::protocol::MusicPlayerDelegate *)player_
{
    if ((self = [super init]))
    {
        player = player_;
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player_ successfully:(BOOL)flag_
{
    player->musicPlaybackFinished();
}

- (void)beginInterruption
{
    E2D_PRINT("\nbeginInterruption");
}

- (void)endInterruption
{
    E2D_PRINT("\nendInterruption");
}

@end

#pragma mark -
#pragma mark E2DMusicPlayerIOS
#pragma mark -
E2DMusicPlayerIOS::E2DMusicPlayerIOS() : currentPlayer(nullptr)
{
    this->setClassName("E2DMusicPlayerIOS");
    
    players = new E2DDictionary();
    delegate = [[E2DMusicPlayerIOSDelegate alloc] initWithPlayer:this];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategorySoloAmbient error:nil];
    
    /*
    this->PreloadMusic("music1.mp3");
    this->PreloadMusic("music2.mp3");
    this->PreloadMusic("c.wav");
    this->Play("music1.mp3", false);
    printf("\ncurrent music:%s\n", this->CurrentMusic());
    //*/
}

E2DMusicPlayerIOS::~E2DMusicPlayerIOS()
{
    players->Release();
}

void E2DMusicPlayerIOS::musicPlaybackFinished()
{
    E2D_SYS_PRINT("playback finished");
}

void E2DMusicPlayerIOS::PreloadMusic(const char *file_)
{
    E2DPlayerIOS *p = new E2DPlayerIOS(file_);
    p->player.delegate = delegate;
    players->SetObject(p, file_);
    currentPlayer = p;
    p->Release();
}

void E2DMusicPlayerIOS::Play(const char *fileName_, bool loop_)
{
    E2DPlayerIOS *p = (E2DPlayerIOS *)players->ObjectForKey(fileName_);
    if (!p)
        E2D_EXIT("music file '%s' not loaded", fileName_);
    
    printf("\nkey:%s\n", players->KeyForObject(p));
    [p->player setNumberOfLoops:(loop_ ? -1 : 0)];
    [p->player play];
    currentPlayer = p;
    
}

void E2DMusicPlayerIOS::Play(bool loop_)
{
    if (currentPlayer)
    {
        [currentPlayer->player setNumberOfLoops:loop_ ? -1 : 0];
        [currentPlayer->player play];
    }
}

void E2DMusicPlayerIOS::Pause() const
{
    if (currentPlayer)
    {
        [currentPlayer->player pause];
    }
}

void E2DMusicPlayerIOS::Stop() const
{
    if (currentPlayer)
    {
        [currentPlayer->player stop];
    }
}

const char * E2DMusicPlayerIOS::CurrentMusic() const
{
    if (currentPlayer)
        return  currentPlayer->fileName->String();
    
    return nullptr;
}

bool E2DMusicPlayerIOS::IsPlaying(void) const
{
    if (currentPlayer)
        return [currentPlayer->player isPlaying];
    
    return false;
}

bool E2DMusicPlayerIOS::IsPlayingMusic(const char *fileName_) const
{
    if (currentPlayer)
        return currentPlayer->fileName->IsEqualTo(fileName_);
    
    return false;
};

