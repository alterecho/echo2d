//
//  E2DScene.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 01/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_SCENE_H
#define E2D_SCENE_H

#include <iostream>
#include "E2D_Obj_.hpp"
#include "E2DArray.h"
#include "E2DDataStructs.hpp"

class E2DLayer;
class E2DScene : public E2D_Obj_, public e2d::protocol::TouchEvents {
    friend class E2DController;
    
    E2DArray                *layers;
    class E2DRenderer       *renderer;
    
    bool            touchClaimed;
    E2Dull_t        touchIndex;
    
    void sortLayers();      /* organize layers with ascending order of depth */
    void refresh(E2Delapsed dt_);
    
protected:
    class E2DController     *_controller;
    E2Dsize                 __screenSize;
    E2Dpoint                __screenCenter;
    
    virtual void update(E2Delapsed dt_) { };
    virtual void print(const char *) const;
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_SCENE; }
    E2DScene(void);
    virtual ~E2DScene();
    
    void AddLayer(E2DLayer *layer_);
    void RemoveLayer(E2DLayer *layer_);
    
    bool touchBegan(E2Dpoint p_);
    void touchMoved(E2Dpoint p_);
    void touchEnded(E2Dpoint p_);
    
};

#endif /* defined(__Echo2D_2__E2DScene__) */
