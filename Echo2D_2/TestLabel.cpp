//
//  TestLabel.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 06/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "TestLabel.h"
#include "E2DFontManager.h"
TestLabel::TestLabel()
{
    E2DFontManager::SharedFontManager()->CacheFontFile("ocr_20.e2dfnt");
    E2DFontManager *fm = E2DFontManager::SharedFontManager();
    fm->Print();
    layer1 = new E2DLayer(0);
    label1 = new E2DLabel("Alabelint", 0);
    this->AddLayer(layer1);
    layer1->AddDrawable(label1);
    
    label1->SetPosition(__screenCenter);
    label1->SetAngle(45.0f);
    label1->SetPosition(E2Dpoint(__screenSize.width * 0.5f, __screenSize.height * 0.5f));
    
    E2D_PRINT("\nstring:%s", label1->String());
    E2D_PRINT("\nstring:%s", label1->String());
    
}

TestLabel::~TestLabel()
{
    
}

void TestLabel::Update(E2Delapsed dt_)
{
}
