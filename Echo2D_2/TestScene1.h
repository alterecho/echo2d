//
//  TestScene1.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 02/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef __Echo2D_2__TestScene1__
#define __Echo2D_2__TestScene1__

#include <iostream>
#include "E2DScene.h"

#include "E2DLayer.h"
#include "E2DSprite.h"
#include "E2DLabel.h"

class TestScene1 : public E2DScene {
    E2DLayer            *layer1, *layer2;
    E2DSprite           *spr1, *spr2, *spr3, *spr4;
    
public:
    TestScene1();
    virtual ~TestScene1();
    
    void Update(E2Delapsed dt_);
};

#endif /* defined(__Echo2D_2__TestScene1__) */
