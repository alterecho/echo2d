//
//  E2DDrawable1.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_DRAWABLE_1_H
#define E2D_DRAWABLE_1_H

#include <iostream>
#include "E2DDrawable.h"

class E2DDrawable1 : public E2DDrawable {
    E2Dvertex           v;
    
protected:
    E2DDrawable1(E2Ddepth d_);
    virtual void render();
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_DRAWABLE1; }
    virtual ~E2DDrawable1();
    
    virtual void setSize(float width_, float height_) { __size(width_, height_); };
    
    const E2Dvertex* Vertices(void) const;
    void SetColor(const E2Dcolor color_);
};

#endif /* defined(__Echo2D_2__E2DDrawable1__) */
