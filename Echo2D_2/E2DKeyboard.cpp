//
//  E2DKeyboard.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DKeyboard.h"
#include "E2DFontManager.h"
#include "E2DTextureManager.h"
#include "E2DController.h"

static E2DKeyboard *instanceOfKeyboard(nullptr);

E2DKeyboard * E2DKeyboard::SharedKeyboard(void)
{
    E2D_ASSERT(instanceOfKeyboard, "Keyboard not instantiated");
    return instanceOfKeyboard;
}

E2DKeyboard::E2DKeyboard() : nativeKeyboard(true)
{
    if (instanceOfKeyboard)
    {
        E2D_EXIT("keyboard already instantiated");
    }
    instanceOfKeyboard = this;
    
    this->setClassName("E2DKeyboard");
    this->setClassType(E2D_CLASS_KEYBOARD);
    
    screenSize = E2DController::SharedController()->ScreenSize();
    screenCenter(screenSize.width * 0.5f, screenSize.height * 0.5f);
    
    tID_text = 0;
    tID_bg = 0;
    
    layout_a = new E2DDictionary(26);
    layout_A = new E2DDictionary(26);
    layout_$ = new E2DDictionary(10);
    
    //btn_bg_def = new E2DSprite("box_w.png", 0);
    printf("\n->");
    E2DFontManager::SharedFontManager()->CacheFontFile(_E2D_KEYBOARD_FONT);
    printf("\n->>");
    E2DTextureManager::SharedTextureManager()->CacheTexture(_E2D_KEYBOARD_BG);
    
    tID_text = E2DFontManager::SharedFontManager()->FontFileTextureID(_E2D_KEYBOARD_FONT);
    E2DTexture *t = E2DTextureManager::SharedTextureManager()->TextureForName(_E2D_KEYBOARD_BG);
    tID_bg = t->ID();
    
    vertices_count = E2D_KEY_COUNT * 8; // 4 vertices for key bg, 4 vertices for font
    vertices = (E2Dvertex *)malloc(sizeof(E2Dvertex) * vertices_count);
    this->initializeVertices();
    
    v_index = 0;
    
    fontFile = new E2DString(_E2D_KEYBOARD_FONT);
    
    this->initLayout_a();
    this->setKeyBGColor(E2D_COLOR_GREEN(255));
    this->setKeyTextColor(E2D_COLOR_RED(125));
    
    /*
    float m = 100.0f;
    vertices[0].position = { -m, m };
    vertices[1].position = { m * 2.0f, m };
    vertices[2].position = { -m, -m };
    vertices[3].position = { m, -m };
    // */
}


E2DKeyboard::~E2DKeyboard()
{
    //E2D_RELEASE(btn_bg_def);

    E2D_RELEASE(fontFile);
    E2D_FREE(vertices);
    
    E2D_RELEASE(layout_$);
    E2D_RELEASE(layout_A);
    E2D_RELEASE(layout_a);
}

void E2DKeyboard::Activate()
{
    printf("\nactivate");
}

void E2DKeyboard::Dismiss()
{
    printf("\ndismiss");
}

void E2DKeyboard::initializeVertices()
{
    for (E2Duint i = 0; i < vertices_count; i++)
    {
        e2dVertexReset(&vertices[i]);
    }
}

void E2DKeyboard::setKeyTextColor(E2Dcolor c_)
{
    for (E2Duint i = 4; i < vertices_count; i += 8)
    {
        for (E2Duint j = i; j < i + 4; j++)
        {
            vertices[j].color = c_;
        }
        
    }
}

void E2DKeyboard::setKeyBGColor(E2Dcolor c_)
{
    for (E2Duint i = 0; i < vertices_count; i += 8)
    {
        for (E2Duint j = i; j < i + 4; j++)
        {
            vertices[j].color = c_;
        }
        
    }
}

void E2DKeyboard::initLayout_a()
{
    
    E2Dpoint pos(0.0f, screenCenter.y);
    
    char ch[100] = {
        'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
            'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
                'z', 'x', 'c', 'v', 'b', 'n', 'm',
                                ' '
    };
    
    
    E2D_PRINT("\ninit layout_a");
    for (unsigned char i = 0; i < 26; i++)
    {
        E2DKey *k = new E2DKey(ch[i], "480.png", _E2D_KEYBOARD_FONT, &vertices[v_index]);
        letterSize = k->LetterSize();
        bgSize = k->BgSize();
        v_index += 8;
        layout_a->SetObject(k, ch[i]);
        k->Release();
    }
    
    float newWidth = (screenSize.width - (E2D_KEYBOARD_KEY_PADDING_X * 11)) / 10; //qwertyuiop
    pos.x = newWidth * 0.5f + E2D_KEYBOARD_KEY_PADDING_X;
    
    for (unsigned char i = 0; i < 26; i++)
    {
        if (i == 10 || i == 19)
        {
            pos.x = newWidth * 0.5f + E2D_KEYBOARD_KEY_PADDING_X;
            pos.y -= newWidth;
        }
        
        E2DKey *k = (E2DKey *)layout_a->ObjectForKey(ch[i]);
        k->SetSize(E2Dsize(newWidth, newWidth));
        k->SetPosition(pos);
        pos.x += newWidth + E2D_KEYBOARD_KEY_PADDING_X;
    }
    
}

void E2DKeyboard::InsertText(const char *t_)
{
    E2D_SYS_PRINT("insert text");
}

void E2DKeyboard::DeleteText()
{
    E2D_SYS_PRINT("delete text");
}

bool E2DKeyboard::HasText()
{
    E2D_SYS_PRINT("has text");
    return false;
}