//
//  E2DGeometry.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 03/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_GEOMETRY_H
#define E2D_GEOMETRY_H

#include <iostream>
#include "E2D_Obj_.hpp"
#include "E2DDataStructs.hpp"

class E2DGeometry : public E2D_Obj_ {
    
protected:
    E2Dpoint            __pos, __ap, __ap_adj;
    E2Dsize             __size;
    E2Dclampf           __scale;
    E2Ddegrees          __angle;
    
    virtual void print(const char *msg_) const {
        __pos.Print("__pos");
        __ap.Print("__ap");
        __ap_adj.Print("__ap_adj");
        __size.Print("__size");
        E2D_PRINT("\n__scale:%f", __scale);
        E2D_PRINT("\n__angle:%f", __angle);
    }
    
protected:
    E2DGeometry(void) :
    __pos(E2Dpoint(0.0f, 0.0f)),
    __ap(E2Dpoint(0.5f, 0.5f)),
    __ap_adj(E2Dpoint(0.0f, 0.0f)),
    __size(E2Dsize(0.0f, 0.0f)),
    __scale(1.0f), __angle(0.0f) {
        this->setClassName("E2DGeometry");
        this->setClassType(E2D_CLASS_GEOMETRY);
        //Print();
    };
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_GEOMETRY; }
    
    virtual ~E2DGeometry(void) { };
    
    inline virtual E2Dsize Size() const
    {
        return __size;
    }
    
    inline virtual void SetScale(E2Dclampf s_) {
        __scale = s_;
    }
    
    inline virtual E2Dclampf Scale(void) const {
        return __scale;
    }
    
    inline virtual void SetPosition(E2Dpoint p_) {
        __pos = p_ + __ap_adj;
    }
    
    inline virtual void SetPosition(E2Dfloat x, E2Dfloat y) {
        __pos.x = x + __ap_adj.x;
        __pos.y = y + __ap_adj.y;
    }
    
    inline virtual void Move(E2Dvec2 p_) {
        __pos += p_;// + __ap_adj;
    }
    
    
    inline virtual void Move(E2Dfloat x, E2Dfloat y) {
        __pos.x += x;// + __ap_adj.x;
        __pos.y += y;// + __ap_adj.y;
    }
    
    inline virtual E2Dpoint Position(void) {
        return __pos;
    }
    
    
    inline virtual void SetAnchorPoint(E2Dpoint ap_) {
        __ap = ap_;
        __ap_adj.x = (0.5f - __ap.x) * __size.width,
        __ap_adj.y = (0.5f - __ap.y) * __size.height;
    }
    
    inline virtual void SetAnchorPoint(E2Dfloat x, E2Dfloat y) {
        __ap.x = x;
        __ap.y = y;
        __ap_adj.x = (0.5f - __ap.x) * __size.width,
        __ap_adj.y = (0.5f - __ap.y) * __size.height;
    }
    
    inline virtual E2Dpoint AnchorPoint(void) {
        return __ap;
    }
    
    inline virtual void SetAngle(E2Ddegrees angle_)
    {
        __angle = angle_;
    }
    
    inline virtual E2Ddegrees Angle(void)
    {
        return __angle;
    }
    
    bool visible;
    inline void SetVisible(bool v_) {
        visible = v_;
    }
    
    inline bool Visible(void) {
        return visible;
    }
    
    
};

/*
#pragma mark - scale -
void E2DGeometry::SetScale(E2Dclampf s_)
{
    __scale = s_;
}

E2Dclampf E2DGeometry::Scale()
{
    return __scale;
}

#pragma mark -
#pragma mark Position
#pragma mark -
void E2DGeometry::SetPosition(E2Dpoint p_)
{
    __pos.x = p_.x + __ap_adj.x;
    __pos.y = p_.y + __ap_adj.y;
}

void E2DGeometry::SetPosition(E2Dfloat x, E2Dfloat y)
{
    __pos.x = x + __ap_adj.x;
    __pos.y = y + __ap_adj.y;
}

void E2DGeometry::Move(E2Dvec2 p_)
{
    __pos.x += p_.x;// + __ap_adj.x;
    __pos.y += p_.y;// + __ap_adj.y;
}

void E2DGeometry::Move(E2Dfloat x, E2Dfloat y)
{
    __pos.x += x;// + __ap_adj.x;
    __pos.y += y;// + __ap_adj.y;
}

E2Dpoint E2DGeometry::Position()
{
    return __pos;
}

#pragma mark - anchor point -
void E2DGeometry::SetAnchorPoint(E2Dpoint ap_)
{
    __ap = ap_;
    __ap_adj.x = (0.5f - __ap.x) * __size.width,
    __ap_adj.y = (0.5f - __ap.y) * __size.height;
}

void E2DGeometry::SetAnchorPoint(E2Dfloat x, E2Dfloat y)
{
    __ap.x = x;
    __ap.y = y;
    __ap_adj.x = (0.5f - __ap.x) * __size.width,
    __ap_adj.y = (0.5f - __ap.y) * __size.height;
}

E2Dpoint E2DGeometry::AnchorPoint()
{
    return __ap;
}

#pragma mark - visible -
void E2DGeometry::SetVisible(bool v_)
{
    visible = v_;
}
 
bool E2DGeometry::Visible()
{
    return visible;
}
//*/
#endif /* defined(__Echo2D_2__E2DGeometry__) */
