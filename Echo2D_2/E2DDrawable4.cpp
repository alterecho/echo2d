//
//  E2DDrawable4.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DDrawable4.h"
#include "E2DRenderer.h"

E2DDrawable4::E2DDrawable4(E2Ddepth depth_) : E2DDrawable(depth_)
{
    this->SetColor(E2Dcolor(255, 255, 255, 255));
}

E2DDrawable4::~E2DDrawable4()
{
    
}

void E2DDrawable4::print(const char *tag_) const
{
    e2dVertexPrint(__v[0], "v[0]");
    e2dVertexPrint(__v[1], "v[1]");
    e2dVertexPrint(__v[2], "v[2]");
    e2dVertexPrint(__v[3], "v[3]");
    E2D_PRINT("\ntextureID:%d", _textureID);
}

void E2DDrawable4::copyVertices(const E2DDrawable4 *d_)
{
    __v[0] = d_->__v[0];
    __v[1] = d_->__v[1];
    __v[2] = d_->__v[2];
    __v[3] = d_->__v[3];
    
}


void E2DDrawable4::setTexCoord(const E2DtexCoord coord_[])
{
    for (char i = 0; i < 4; i++)
    {
        __v[i].texCoord = coord_[i];
    }
}

void E2DDrawable4::SetColor(const E2Dcolor c_)
{
    __color = c_;
    
    for (char i = 0; i < 4; i++)
    {
        __v[i].color = c_;
    }
}


const E2Dvertex* E2DDrawable4::Vertices() const
{
    return &__v[0];
}

void E2DDrawable4::render()
{
    E2DRenderer::SharedRenderer()->Render(this);
}