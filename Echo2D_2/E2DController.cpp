//
//  E2DController.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 22/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DController.h"
#include "E2DFileManager.h"
#include "E2DRendererES2.h"
#include "E2DTextureManager.h"
#include "E2DFontManager.h"
#include "E2DSceneManager.h"
#include "E2DScene.h"
#include "E2DOverlay.h"
#include "E2DKeyboard.h"
#include "E2DAudioLoader.h"
#include "E2DFXPlayer.h"
//#include "E2DMusicPlayer.h" /* Use platform specific version */

static E2DController *instanceOfController = NULL;

E2DController* E2DController::SharedController()
{
    E2D_ASSERT(instanceOfController, "E2DController not initialized");
    return instanceOfController;
}

E2DController::E2DController() :
    fileManager(nullptr), imageLoader(nullptr), renderer(nullptr), textureManager(nullptr), fontManager(nullptr),
    keyboard(nullptr), overlay(nullptr)
{
    this->setClassName("E2DController");
    this->setClassType(E2D_CLASS_CONTROLLER);
    instanceOfController = this;
}

/*
void E2DController::initialize()
{
    _renderer = new E2DRendererES2(_screenSize);
    _textureManager = new E2DTextureManager();
    _fontManager = new E2DFontManager();
    overlay = new E2DOverlay();
}
//*/

void E2DController::initManagers()
{
    sceneManager = new E2DSceneManager();
    renderer = new E2DRendererES2(_screenSize);
    textureManager = new E2DTextureManager();
    fontManager = new E2DFontManager();
}

void E2DController::initModules()
{
    audioLoader = new E2DAudioLoader();
    fxPlayer = new E2DFXPlayer();
    //musicPlayer = new E2DMusicPlayer();     /* platform specific version */
    overlay = new E2DOverlay();
    
}

E2DController::~E2DController()
{
    /*
    E2D_RELEASE(musicPlayer);
    E2D_RELEASE(keyboard);
    E2D_RELEASE(imageLoader);
    E2D_RELEASE(fileManager);
    //*/
    /*
    E2D_RELEASE(overlay);
    E2D_RELEASE(fxPlayer);
    E2D_RELEASE(audioLoader);
    E2D_RELEASE(keyboard);
    E2D_RELEASE(fontManager);
    E2D_RELEASE(sceneManager);
    E2D_RELEASE(renderer);
    E2D_RELEASE(textureManager);
    // */
    
    instanceOfController = NULL;
}

void E2DController::releaseManagers()
{
    E2D_RELEASE(fontManager);
    E2D_RELEASE(textureManager);
    E2D_RELEASE(renderer);
    E2D_RELEASE(sceneManager);
}

void E2DController::releaseModules()
{
    E2D_RELEASE(overlay);
    //E2D_RELEASE(musicPlayer);
    E2D_RELEASE(fxPlayer);
    E2D_RELEASE(audioLoader);
}

E2Dsize E2DController::ScreenSize() { return _screenSize; }
E2Dfloat E2DController::ScreenScale() { return _screenScale; }

//inline
E2DMusicPlayer* E2DController::MusicPlayer()
{
    return musicPlayer;
}

//inline
E2DFXPlayer* E2DController::FXPlayer()
{
    return fxPlayer;
}


inline
void E2DController::update(E2Delapsed dt_)  //called from subclass, E2DControllerIOS' update()
{
    /*
    static float avgFPS = 0;
    avgFPS += dt_;
    avgFPS /= 2.0f;
    E2D_PRINT("\n%f, avg:%f", 1.0f / avgFPS, 1.0f / dt_);
    //*/
    
    renderer->Clear();
    
    //overlay->Render();
    //if (__sceneManager->__currentScene)
    if (!sceneManager->deprecatedScene)
    {
        sceneManager->currentScene->update(dt_);
        sceneManager->currentScene->refresh(dt_);
    }
    else
    {
        E2D_SYS_PRINT("removing deprecated scene");
        sceneManager->removeDeprecated();
    }
    
    //overlay->Render();
}

void E2DController::ReplaceScene(class E2DScene *scene_)
{
    sceneManager->ReplaceScene(scene_);
}

bool E2DController::touchBegan(E2Dpoint p_)
{
    E2D_SYS_PRINT("touchBegan:(%f, %f)", p_.x, p_.y);
    sceneManager->CurrentScene()->touchBegan(p_);
    return false;
}

void E2DController::touchMoved(E2Dpoint p_)
{
    E2D_SYS_PRINT("touchMoved:(%f, %f)", p_.x, p_.y)
    sceneManager->CurrentScene()->touchMoved(p_);
}

void E2DController::touchEnded(E2Dpoint p_)
{
    E2D_SYS_PRINT("touchEnded:(%f, %f)", p_.x, p_.y);
    sceneManager->CurrentScene()->touchEnded(p_);
}

void E2DController::ActivateKeyboard()
{
    keyboard->Activate();
}

void E2DController::DismissKeyboard()
{
    keyboard->Dismiss();
}


