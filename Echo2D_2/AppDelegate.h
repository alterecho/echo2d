//
//  AppDelegate.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 10/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@class E2DRendererObjC, E2DGLView;
@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    RootViewController          *rootViewController;

    struct E2DControllerIOS     *controller;
    CADisplayLink               *displayLink;
    NSTimer                     *timer;
    
    E2DRendererObjC             *renderer_objC;
    E2DGLView                   *glView;
    EAGLContext                 *context;
    CAEAGLLayer                 *layer;
}

@property (strong, nonatomic) UIWindow *window;

- (void)update:(CADisplayLink *)displayLink_;
- (void)updateObjC;

- (void)initObjC;

@end
