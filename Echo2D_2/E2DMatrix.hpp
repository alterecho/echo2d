//
//  E2DMatrix.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 21/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_MATRIX_H
#define E2D_MATRIX_H

#include <iostream>
#include "E2DDataTypes.h"

template <unsigned int n>
class E2D_mat_ {
protected:
    unsigned int _size = n * n;
    unsigned int _order = n;
    
public:
    float m[n][n];
    
    static inline E2D_mat_<n> Identity();
    
    E2D_mat_();
    
    const float* Pointer();
    float* MutablePointer();
    unsigned int Size();
    unsigned int Order();
    void Print();
};

#pragma mark -
#pragma mark E2Dmatrix
#pragma mark -
template <unsigned int n>
class E2Dmatrix : public E2D_mat_<n>{
    
};

#pragma mark -
#pragma mark E2Dmatrix<4>
#pragma mark -
template <>
class E2Dmatrix<4> : public E2D_mat_<4>{

public:
    
    inline void operator=(E2D_mat_<4> mat);
    inline E2Dmatrix<4> operator*(E2Dmatrix<4>& m2);
    inline void operator*=(E2Dmatrix<4>& m2);
    
    static inline E2Dmatrix<4> Identity();
    
    inline void SetX(float x);
    inline void SetY(float y);
    inline void SetZ(float z);
    inline void SetW(float w);
    
    inline void SetOrtho(E2Dfloat left, E2Dfloat right, E2Dfloat bottom, E2Dfloat top, E2Dfloat near, E2Dfloat far);
    inline void SetScale(float x, float y, float z);
    
    inline void SetTranslate(float x, float y, float z);
    inline void SetRotateZ(float deg);
    inline void RotateZ(float deg);
    inline void SetTransform(float dx, float dy, float dz, float sx, float sy, float sz, float rad);
};

typedef E2Dmatrix<4>        E2Dmat4;


#pragma mark - definition -

#pragma mark -
#pragma mark E2D_mat_
#pragma mark -
template <unsigned int n>
E2D_mat_<n>::E2D_mat_()
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            m[i][j] = 0.0f;
        }
    }
}

template <unsigned int n>
const float* E2D_mat_<n>::Pointer()
{
    return &m[0][0];
}

template <unsigned int n>
float* E2D_mat_<n>::MutablePointer()
{
    return &m[0][0];
}

template <unsigned int n>
unsigned int E2D_mat_<n>::Size()
{
    return _size;
}

template <unsigned int n>
unsigned int  E2D_mat_<n>::Order()
{
    return _order;
}

template <unsigned int n>
inline E2D_mat_<n> E2D_mat_<n>::Identity()
{
    E2D_mat_<n> ret;
    for (int i = 0; i < ret._order; i++)
    {
        for (int j = 0; j < ret._order; j++)
        {
            if (i == j)
            {
                ret.m[i][j] = 1.0f;
            }
            else
            {
                ret.m[i][j] = 0.0f;
            }
        }
    }
    
    return ret;
}

template <unsigned int n>
void E2D_mat_<n>::Print()
{
    E2D_PRINT("\nprinting matrix...\n ")
    for (int i = 0; i < _order; i++)
    {
        for (int j = 0; j < _order; j++)
        {
            E2D_PRINT("%f\t", m[i][j]);
        }
        E2D_PRINT("\n");
    }
}




#pragma mark -
#pragma mark E2Dmatrix<4>
#pragma mark -
inline E2Dmatrix<4> E2Dmatrix<4>::Identity()
{
    E2Dmatrix<4> ret;
    for (int i = 0; i < ret._order; i++)
    {
        for (int j = 0; j < ret._order; j++)
        {
            if (i == j)
            {
                ret.m[i][j] = 1.0f;
            }
            else
            {
                ret.m[i][j] = 0.0f;
            }
        }
    }
    
    return ret;
}

inline void E2Dmatrix<4>::operator=(E2D_mat_<4> mat)
{
    //memcpy(mat.m, mat.m, sizeof(float));
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            m[i][j] = mat.m[i][j];
        }
    }
}

inline E2Dmatrix<4> E2Dmatrix<4>::operator*(E2Dmatrix<4>& m2)
{
    E2Dmatrix<4> ret;
    
    
    ret.m[0][0] =  m[0][0] * m2.m[0][0]  +  m[0][1] * m2.m[1][0]  +  m[0][2] * m2.m[2][0]  +  m[0][3] * m2.m[3][0];
    ret.m[0][1] =  m[0][0] * m2.m[0][1]  +  m[0][1] * m2.m[1][1]  +  m[0][2] * m2.m[2][1]  +  m[0][3] * m2.m[3][1];
    ret.m[0][2] =  m[0][0] * m2.m[0][2]  +  m[0][1] * m2.m[1][2]  +  m[0][2] * m2.m[2][2]  +  m[0][3] * m2.m[3][2];
    ret.m[0][3] =  m[0][0] * m2.m[0][3]  +  m[0][1] * m2.m[1][3]  +  m[0][2] * m2.m[2][3]  +  m[0][3] * m2.m[3][3];
    
    ret.m[1][0] =  m[1][0] * m2.m[0][0]  +  m[1][1] * m2.m[1][0]  +  m[1][2] * m2.m[2][0]  +  m[1][3] * m2.m[3][0];
    ret.m[1][1] =  m[1][0] * m2.m[0][1]  +  m[1][1] * m2.m[1][1]  +  m[1][2] * m2.m[2][1]  +  m[1][3] * m2.m[3][1];
    ret.m[1][2] =  m[1][0] * m2.m[0][2]  +  m[1][1] * m2.m[1][2]  +  m[1][2] * m2.m[2][2]  +  m[1][3] * m2.m[3][2];
    ret.m[1][3] =  m[1][0] * m2.m[0][3]  +  m[1][1] * m2.m[1][3]  +  m[1][2] * m2.m[2][3]  +  m[1][3] * m2.m[3][3];
    
    ret.m[2][0] =  m[2][0] * m2.m[0][0]  +  m[2][1] * m2.m[1][0]  +  m[2][2] * m2.m[2][0]  +  m[2][3] * m2.m[3][0];
    ret.m[2][1] =  m[2][0] * m2.m[0][1]  +  m[2][1] * m2.m[1][1]  +  m[2][2] * m2.m[2][1]  +  m[2][3] * m2.m[3][1];
    ret.m[2][2] =  m[2][0] * m2.m[0][2]  +  m[2][1] * m2.m[1][2]  +  m[2][2] * m2.m[2][2]  +  m[2][3] * m2.m[3][2];
    ret.m[2][3] =  m[2][0] * m2.m[0][3]  +  m[2][1] * m2.m[1][3]  +  m[2][2] * m2.m[2][3]  +  m[2][3] * m2.m[3][3];
    
    ret.m[3][0] =  m[3][0] * m2.m[0][0]  +  m[3][1] * m2.m[1][0]  +  m[3][2] * m2.m[2][0]  +  m[3][3] * m2.m[3][0];
    ret.m[3][1] =  m[3][0] * m2.m[0][1]  +  m[3][1] * m2.m[1][1]  +  m[3][2] * m2.m[2][1]  +  m[3][3] * m2.m[3][1];
    ret.m[3][2] =  m[3][0] * m2.m[0][2]  +  m[3][1] * m2.m[1][2]  +  m[3][2] * m2.m[2][2]  +  m[3][3] * m2.m[3][2];
    ret.m[3][3] =  m[3][0] * m2.m[0][3]  +  m[3][1] * m2.m[1][3]  +  m[3][2] * m2.m[2][3]  +  m[3][3] * m2.m[3][3];
    
    return ret;
}

inline void E2Dmatrix<4>::operator*=(E2Dmatrix<4> &m2)
{
    m[0][0] =  m[0][0] * m2.m[0][0]  +  m[0][1] * m2.m[1][0]  +  m[0][2] * m2.m[2][0]  +  m[0][3] * m2.m[3][0];
    m[0][1] =  m[0][0] * m2.m[0][1]  +  m[0][1] * m2.m[1][1]  +  m[0][2] * m2.m[2][1]  +  m[0][3] * m2.m[3][1];
    m[0][2] =  m[0][0] * m2.m[0][2]  +  m[0][1] * m2.m[1][2]  +  m[0][2] * m2.m[2][2]  +  m[0][3] * m2.m[3][2];
    m[0][3] =  m[0][0] * m2.m[0][3]  +  m[0][1] * m2.m[1][3]  +  m[0][2] * m2.m[2][3]  +  m[0][3] * m2.m[3][3];
    
    m[1][0] =  m[1][0] * m2.m[0][0]  +  m[1][1] * m2.m[1][0]  +  m[1][2] * m2.m[2][0]  +  m[1][3] * m2.m[3][0];
    m[1][1] =  m[1][0] * m2.m[0][1]  +  m[1][1] * m2.m[1][1]  +  m[1][2] * m2.m[2][1]  +  m[1][3] * m2.m[3][1];
    m[1][2] =  m[1][0] * m2.m[0][2]  +  m[1][1] * m2.m[1][2]  +  m[1][2] * m2.m[2][2]  +  m[1][3] * m2.m[3][2];
    m[1][3] =  m[1][0] * m2.m[0][3]  +  m[1][1] * m2.m[1][3]  +  m[1][2] * m2.m[2][3]  +  m[1][3] * m2.m[3][3];
    
    m[2][0] =  m[2][0] * m2.m[0][0]  +  m[2][1] * m2.m[1][0]  +  m[2][2] * m2.m[2][0]  +  m[2][3] * m2.m[3][0];
    m[2][1] =  m[2][0] * m2.m[0][1]  +  m[2][1] * m2.m[1][1]  +  m[2][2] * m2.m[2][1]  +  m[2][3] * m2.m[3][1];
    m[2][2] =  m[2][0] * m2.m[0][2]  +  m[2][1] * m2.m[1][2]  +  m[2][2] * m2.m[2][2]  +  m[2][3] * m2.m[3][2];
    m[2][3] =  m[2][0] * m2.m[0][3]  +  m[2][1] * m2.m[1][3]  +  m[2][2] * m2.m[2][3]  +  m[2][3] * m2.m[3][3];
    
    m[3][0] =  m[3][0] * m2.m[0][0]  +  m[3][1] * m2.m[1][0]  +  m[3][2] * m2.m[2][0]  +  m[3][3] * m2.m[3][0];
    m[3][1] =  m[3][0] * m2.m[0][1]  +  m[3][1] * m2.m[1][1]  +  m[3][2] * m2.m[2][1]  +  m[3][3] * m2.m[3][1];
    m[3][2] =  m[3][0] * m2.m[0][2]  +  m[3][1] * m2.m[1][2]  +  m[3][2] * m2.m[2][2]  +  m[3][3] * m2.m[3][2];
    m[3][3] =  m[3][0] * m2.m[0][3]  +  m[3][1] * m2.m[1][3]  +  m[3][2] * m2.m[2][3]  +  m[3][3] * m2.m[3][3];
}


inline void E2Dmatrix<4>::SetOrtho(E2Dfloat left, E2Dfloat right, E2Dfloat bottom, E2Dfloat top, E2Dfloat near, E2Dfloat far)
{
    float
    x = 2.0f / (right - left),
    y = 2.0f / (top - bottom),
    z = 2.0f / (far - near),
    dx = - ( (right + left) / (right - left) ),
    dy = - ( (top + bottom) / (top - bottom) ),
    dz = - ( (far + near) / (far - near) );
    
    m[0][1] = m[0][2] = m[0][3] =
    m[1][0] = m[1][2] = m[1][3] =
    m[2][0] = m[2][1] = m[2][3] = 0.0f;
    m[3][3] = 1.0f;
    
    m[0][0] = x;
    m[1][1] = y;
    m[2][2] = z;
    
    m[3][0] = dx;
    m[3][1] = dy;
    m[3][2] = dz;
}

inline void E2Dmatrix<4>::SetX(float x)
{
    
    m[0][0] = x;
}

inline void E2Dmatrix<4>::SetY(float y)
{
    m[1][1] = y;
}

inline void E2Dmatrix<4>::SetZ(float z)
{
    m[2][2] = z;
}

inline void E2Dmatrix<4>::SetW(float w)
{
    m[3][3] = w;
}

inline void E2Dmatrix<4>::SetScale(float x, float y, float z)
{
    m[0][0] = x;
    m[1][1] = y;
    m[2][2] = z;
}

inline void E2Dmatrix<4>::SetTranslate(float x, float y, float z)
{
    m[3][0] = x;
    m[3][1] = y;
    m[3][2] = z;
}

inline void E2Dmatrix<4>::SetRotateZ(float rad)
{
    float
    c = cosf(rad),
    s = sinf(rad);
    
    m[0][0] = c,        m[0][1] = s,        m[0][2] = 0.0f,     m[0][3] = 0.0f;
    
    m[1][0] = -s,       m[1][1] = c,        m[1][2] = 0.0f,     m[1][3] = 0.0f;
    
    m[2][0] = -0.0f,    m[2][1] = 0.0f,     m[2][2] = 1.0f,     m[2][3] = 0.0f;
    
    m[3][0] = 0.0f,     m[3][1] = 0.0f,     m[3][2] = 0.0f,     m[3][3] = 1.0f;
}

inline void E2Dmatrix<4>::RotateZ(float rad)
{
    float
    c = cosf(rad),
    s = sinf(rad);
    
    float sx = m[0][0], sy = m[1][1];
    
    m[0][0] = sx * c,   m[0][1] = sy * s,
    
    m[1][0] = sx * -s,  m[1][1] = sy * c;

}

inline void E2Dmatrix<4>::SetTransform(float dx, float dy, float dz, float sx, float sy, float sz, float rad)
{
    float
    c = cosf(rad),
    s = sinf(rad);
    
    m[0][0] = sx * c,   m[0][1] = sy * s,   m[0][2] = 0.0f,     m[0][3] = 0.0f;
    
    m[1][0] = sx * -s,  m[1][1] = sy * c,   m[1][2] = 0.0f,     m[1][3] = 0.0f;
    
    m[2][0] = 0.0f,     m[2][1] = 0.0f,     m[2][2] = sz,       m[2][3] = 0.0f;
    
    m[3][0] = dx,       m[3][1] = dy,       m[3][2] = dz,       m[3][3] = 1.0f;
}

//*/



#endif /* defined(__Echo2D_2__E2DMatrix__) */
