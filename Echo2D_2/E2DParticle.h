//
//  E2DParticle.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 11/04/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_PARTICLE_H
#define E2D_PARTICLE_H

#include <iostream>
#include "E2DDrawable.h"
#include "E2DparticleAttribs.hpp"
#include "E2DparticleMod.hpp"
class E2DEmitter;

class E2DParticle : public E2DDrawable {
    friend class E2DEmitter;
    
    E2DpRenderAttribs   *pra;
    bool                active;
    
    E2DEmitter          *emitter;
    
    E2DparticleAttribs  att;
    E2DparticleMod      mod;
    E2Dseconds          lifeTime;
    E2Dfloat            pSize;
    
    void setSize(float width_, float height_) { };
    void reflectEmitterStates(void);
    void setAttributes(const E2DparticleAttribs&, const E2DparticleMod&);
    void randomize(const E2DpLimits& lim_);
protected:
    void render(void) { };
    void update(E2Delapsed dt_);
    void reset(void);
    
    
public:
    static inline E2Dclass_t Class(void) { return E2D_CLASS_PARTICLE; }
    E2DParticle(const char* spriteFile_, E2DpRenderAttribs *pra_);
    ~E2DParticle();
    
    const E2Dvertex* Vertices(void) const { return nullptr;};
    void SetColor(E2Dcolor color_) { };
    
    
};

#endif /* defined(__Echo2D_2__E2DParticle__) */
