//
//  RootViewController.m
//  Echo2D_2
//
//  Created by Vijay Chandran J on 12/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //[self.view setMultipleTouchEnabled:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event_
{
    printf("\n touches began");
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event_
{
    printf("\n touches moved");
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event_
{
    printf("\n touches ended");
}

@end
