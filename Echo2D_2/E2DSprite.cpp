//
//  E2DSprite.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 25/03/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DSprite.h"
#include "E2DRenderer.h"
#include "E2DTextureManager.h"
#include "E2DLayer.h"

E2DSprite::E2DSprite(const char *fileName_, E2Ddepth depth_) : E2DDrawable4Z(depth_)
{
    this->setClassName("E2DSprite");
    this->setClassType(E2D_CLASS_SPRITE);
    
    E2DTextureManager::SharedTextureManager()->AssignTexture(fileName_, this);
}
