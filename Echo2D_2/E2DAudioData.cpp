//
//  E2DAudioData.cpp
//  Echo2D_2_Audio
//
//  Created by Vijay Chandran J on 02/06/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#include "E2DAudioData.h"


E2DAudioData::E2DAudioData(long sampleRate_, long dataSize_, short bps_, short channels_, unsigned char *buffer_)
{
    sampleRate = sampleRate_;
    dataSize = dataSize_;
    bitsPerSample = bps_;
    channels = channels_;
    
    if (bitsPerSample == 8)
    {
        if (channels == 1)
            format = AL_FORMAT_MONO8;
        else
            format = AL_FORMAT_STEREO8;
            
    }
    else
    {
        if (channels == 1)
            format = AL_FORMAT_MONO16;
        else
            format = AL_FORMAT_STEREO16;
    }
    
    buffer = buffer_;
}

E2DAudioData::~E2DAudioData()
{
    E2D_FREE(buffer);
}


