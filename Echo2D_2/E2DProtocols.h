//
//  E2DProtocols.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 13/06/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef E2D_PROTOCOLS_H
#define E2D_PROTOCOLS_H

#include <iostream>
#include "E2DDataStructs.hpp"

namespace e2d {
    
    namespace protocol {
        
        struct TouchEvents {
            virtual bool touchBegan(E2Dpoint) = 0;
            virtual void touchMoved(E2Dpoint) = 0;
            virtual void touchEnded(E2Dpoint) = 0;
            
            virtual void touchCancelled(E2Dpoint) { };
        };
        
        struct KeyboardEvents {
            virtual void DeleteText() = 0;
            virtual bool HasText() = 0;
            virtual void InsertText(const char *t_) = 0;
        };
        
        struct MusicPlayerDelegate {
            virtual void musicPlaybackFinished(void) = 0;
        };
        
    }
    
}

namespace E2Dprt = e2d::protocol;

#endif
