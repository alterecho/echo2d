//
//  E2DFileManager.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 19/02/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "E2DFileManager.h"

static E2DFileManager *instanceOfFileManager = NULL;

E2DFileManager* E2DFileManager::SharedFileManager()
{
    E2D_ASSERT(instanceOfFileManager, "E2DFileManager not initialized");
    return instanceOfFileManager;
}

E2DFileManager::E2DFileManager()
{
    this->setClassName("E2DFileManager");
    this->setClassType(E2D_CLASS_FILE_MANAGER);
    E2D_PRINT("\nfm constructor");
    instanceOfFileManager = this;
}

E2DFileManager::~E2DFileManager()
{
    instanceOfFileManager = NULL;
}