//
//  TestLayer2.h
//  Echo2D_2
//
//  Created by Vijay Chandran J on 13/06/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#ifndef __Echo2D_2__TestLayer2__
#define __Echo2D_2__TestLayer2__

#include <iostream>
#include "E2DLayer.h"

class TestLayer2 : public E2DLayer {
    
public:
    TestLayer2(E2Ddepth d_);
    
    bool touchBegan(E2Dpoint);
    void touchMoved(E2Dpoint);
    void touchEnded(E2Dpoint);
};

#endif /* defined(__Echo2D_2__TestLayer2__) */
