//
//  TestLayer1.cpp
//  Echo2D_2
//
//  Created by Vijay Chandran J on 13/06/14.
//  Copyright (c) 2014 Vijay Chandran J. All rights reserved.
//

#include "TestLayer1.h"

TestLayer1::TestLayer1(E2Ddepth d_) : E2DLayer(d_)
{
    this->setClassName("TestLayer1");
}

bool TestLayer1::touchBegan(E2Dpoint p_)
{
    E2D_SYS_PRINT("t began")
    return false;
}

void TestLayer1::touchMoved(E2Dpoint p_)
{
    E2D_SYS_PRINT("t moved");
}

void TestLayer1::touchEnded(E2Dpoint p_)
{
    E2D_SYS_PRINT("t ended");
}
